
// ------------------------------------------------------------------
//
// XWisp2Cfg.c - Wisp648 Programmer support
//
//             -  Build table of target PICS
//             -  Functions to search a specific PIC
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023, R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ------------------------------------------------------------------

// #define __DEBUG__

#include "xwisp2.h"                                     // all includes


//  Symbolic names of keyword parameters
enum _keyword { K_AAA,                                  // comment
                K_ALGORITHM,
                K_ALIAS,
                K_CORE,
                K_DATASHEET,
                K_DATASIZE,
                K_DATASTART,
                K_DELAY,
                K_DEVID,
                K_DEVIDSIZE,
                K_DEVIDSTART,
                K_WRITEBURST,
                K_FAMILY,
                K_FUSESSIZE,
                K_FUSESSTART,
                K_FIXEDONE,
                K_FIXEDZERO,
                K_IDSIZE,
                K_IDSTART,
                K_INCLUDE,                              // include file
                K_NAME,
                K_PRESERVE,
                K_PGMSPEC,
                K_PROGSIZE,
                K_PROGSTART,
                K_PROTECTMASK,
                K_REVISION,
                K_STATUS,
                K_VPPFIRST,
                K_WRITEBUFFER,
                K_ZZZ};                                 // must be last!

static ULONG       ulCore          = 0;                 // default Core (global)
static ULONG       ulLineNumber    = 0;                 // line numbering
static char        szCfgCurr[260]  = "";                // current cfg file
static char        szPathPref[260] = "";                // include prefix
static const char  *szCfgExt       = ".cfg";            // extension of cfg
static const char  *szUnknown      = "        ";        // default datasheet
static TARGETINFO  TargetInfo = {NULL, NULL, NULL, "?", "?",
                                 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                 {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                 {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                 0,0,0,0,FALSE,NULL
                                 };

typedef struct _familyinfo {
                           ULONG   Core;                // bits
                           ULONG   Algorithm;           // default prog. alg.
                           ULONG   ProgLoc;             // program mem loc.
                           ULONG   DataLoc;             // Data mem loc.
                           ULONG   IDLoc;               // (conf) id loc.
                           ULONG   DevIDLoc;            // device ID loc.
                           ULONG   FusesLoc;            // fuses location
                           USHORT  ProtectMask;         // memory protection
                           USHORT  BlankMask;           // active bits
                           char    FuseFixedZero[14];   // fixed zero bits
                           } FAMILYINFO, *PFAMILYINFO;

static FAMILYINFO  FamilyInfo[] = {
//core algorithm  progloc  dataloc    IDloc devIDloc fusesloc   prot  blank
//
  {12,ALG_PIC12,2*0x0000,2*0x0FFF,2*0x0100,2*0x0106,2*0x0107,0x0080,0x0FFF,
   {0xFF,0x0F,0xFF,0x0F,0xFF,0x0F,0xFF,0x0F,0xFF,0x0F,0xFF,0x0F,0xFF,0x0F}},

  {14,ALG_PIC16,2*0x0000,2*0x2100,2*0x2000,2*0x2006,2*0x2007,0x3130,0x3FFF,
   {0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F}},

  {15,ALG_PIC16G,2*0x0000,2*0x8100,2*0x8000,2*0x8006,2*0x8007,0x3130,0x3FFF,
   {0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F,0xFF,0x3F}},

  {16,ALG_PIC18,0x000000,0xF00000,0x200000,0x3FFFFE,0x300000,0xC00F,0xFFFF,
   {0x00,0x27,0x0F,0x0F,0x00,0x01,0x85,0x00,0x0F,0xC0,0x0F,0xE0,0x0F,0x40}}

          };


// prototypes of local functions

static  PTARGETINFO  addPIC(char *);
static  LONG         Hex2Long(char *);
static  ULONG        InvalidValue(char *);
static  ULONG        NewRegion(ULONG, char **);
static  ULONG        ParseKeyword(char *, char *, char **);
static  int          ReadKeywordFile(char *);


// ---------------------------------------------
// Add new PIC to chain
//
// Input   - pointer to name of PIC
//
// Output  - nothing
//
// Returns - pointer to TargetInfo record
//           NULL when failing to allocate memory or duplicate name
//
// ---------------------------------------------
static  PTARGETINFO  addPIC(char *v) {                  // PIC name

                                                        // default patterns
  PTARGETINFO  pC, pT;                                  // ptr(s) to target
  char        *pV;                                      // ptr to string

  pC = SearchPICName(v);                                // search name in chain
  if (pC != NULL) {                                     // found (duplicate!)
    if (!bQuiet)
      printf("ERR: Duplicate specification of '%s' on line %lu of '%s'\n",
              v, ulLineNumber, szCfgCurr);
    return NULL;                                        // signal error
    }

#ifdef __DEBUG__
  printf("Adding new PIC: %s\n", v);
  fflush(stdout);
#endif

  pT = (PTARGETINFO)malloc(sizeof(TARGETINFO));         // alloc new element
  if (pT == NULL) {                                     // available
    if (!bQuiet)
      printf("ERR: No memory for properties of %s on line %ld of '%s'\n",
              v, ulLineNumber, szCfgCurr);
    return NULL;
    }

#ifdef __DEBUG__
  printf("Pointer to memory obtained: %p\n", pT);
  fflush(stdout);
#endif

  pT->Next = NULL;                                      // end of chain
  pT->Name = ToUpperCase(strdup(v));                    // PIC name
  pT->Alias = pT->Name;                                 // default alias!
  pV = strchr(pT->Name, 'F');                           // flash-type
  if (pV == NULL) {                                     // not found
    pV = strchr(pT->Name, 'H');                         // HV type
    if (pV == NULL) {                                   // not found
      if (bVerbose  &&  !bQuiet) {
        printf("WNG: '%s' on line %lu of '%s'"
               " is probably not a flash PIC\n",
               v, ulLineNumber, szCfgCurr);
        }
      }
    }
  if (pV != NULL)                                       // 'F' found
    pT->Alias = pV;                                     // last part
  pT->DataSheet = (char *)szUnknown;                    // unknown datasheet
  pT->PgmSpec = (char *)szUnknown;                      // unknown pgm spec
  if (strncmp(pT->Name, "18", 2) == 0) {                // 18F series
    ulCore = CORE_16;                                   // to distinguish devid
    }
  pT->FamilyIndex = ulCore;                             // PIC family index
  pT->Core = FamilyInfo[ulCore].Core;                   // address bits
  pT->Algorithm = FamilyInfo[ulCore].Algorithm;         // base algorithm
  pT->DevID = 0;                                        // unknown device
  pT->RevMask = 0x001F;                                 // rev-bits in DevID
  pT->BlankMask = FamilyInfo[ulCore].BlankMask;         // implemented bits
  pT->ProgStart = FamilyInfo[ulCore].ProgLoc;           // start Prog mem
  pT->ProgSize = 0;                                     // no program memory
  pT->DataStart = FamilyInfo[ulCore].DataLoc;           // start data mem
  pT->DataSize = 0;                                     // no data memory
  pT->IDStart = FamilyInfo[ulCore].IDLoc;               // start ID mem
  pT->IDSize = 8;                                       // bytes ID memory
  pT->DevIDStart = FamilyInfo[ulCore].DevIDLoc;         // start DevID mem
  pT->DevIDSize = 2;                                    // DevID mem
  pT->FusesStart = FamilyInfo[ulCore].FusesLoc;         // start config
  pT->FusesSize = (pT->FamilyIndex == CORE_16) ? 14 : 2;     // bytes fuses memory
  memcpy(pT->FuseFixedZero, FamilyInfo[ulCore].FuseFixedZero,
                           sizeof(pT->FuseFixedZero));
  memset(pT->FuseFixedOne, 0x00, sizeof(pT->FuseFixedOne)); // no fixed-1 bits
  pT->ProtectMask = FamilyInfo[ulCore].ProtectMask;     // no memory protect.
  pT->WriteBurst = 8;                                   // default (F18)
  pT->VppFirst = FALSE;                                 // Vdd before Vpp
  pT->Delay = 0;                                        // Wisp648 firmware default
  pT->Status = WISP648_UNTESTED;                        // untested
  pT->pPreserve = (PPRESERVE)NULL;                      // nothing to preserve

  pC = SearchPICLast(NULL);                             // get last in chain
  pC->Next = pT;                                        // add this one

  return pT;                                            // ptr to new element
  }                                                     // (NULL = error)


// -------------------------------------------------------------------------
// Adjust programming algorithm
// ------------------------------------------------------------------
extern  void  AdjustAlgorithm(ULONG  ulFirmwareLevel) {

  PTARGETINFO  pT;                                      // pointer to target

  pT = TargetInfo.Next;                                 // to first target
  while (pT != NULL) {                                  // all PICs

    /* nothing for Xwisp2 2.0.0 and Wisp648 firmware >= 1.28 */

    pT = pT->Next;                                      // next PIC
    }

  return;
  }


// ---------------------------------------------------
//
//  Bulk erase memory region
//
//  input:   - size in bytes
//           - pointer to pointer to new region memory
//
//  output:  - region is erased
//
//  return:  - nothing
//
// -------------------------------------------------
extern  void   BulkErase(ULONG   ulRegionSize,          // size in bytes
                         char   *pucRegion,             // ptr to region
                         USHORT  usBlankMask) {         // mask word

  int     i;                                            // counter(s)

#ifdef __DEBUG__
  printf("Filling %lu words from %08lX with %04hX\n",
          ulRegionSize/2, pucRegion, usBlankMask);
  fflush(stdout);
#endif

  for (i = 0; i < ulRegionSize; i++, i++)               // whole region
    WORD2BYTES(usBlankMask, pucRegion + i);             // blank word
  }


// ---------------------------------------------------
//
//  Convert string to hex (all 4 characters must be hexadecimal digits)
//
//  input:   - pointer to pointer to new region memory
//
//  output:  - none
//
//  return:  - hex value (0 if any char not hex)
//
// -------------------------------------------------
static  LONG   Hex2Long(char   *szString) {

  int     i;                                            // counter(s)
  LONG    slValue;
  char    c;

  slValue = 0;                                          // init

  for (i = 0; i < 4; i++) {                             // 4 characters
    slValue <<= 4;                                      // space for next byte
    c = szString[i];
    if (c >= '0' && c <= '9')                           // in range 0..9
      slValue += c - '0';
    else if (c >= 'a'  &&  c <= 'f')                    // in range a..f
      slValue += 10 + c - 'a';
    else if (c >= 'A'  &&  c <= 'F')                    // in range A..F
      slValue += 10 + c - 'A';
    else
      return 0;                                         // invalid
    }

#ifdef __DEBUG__
  printf("String '%s', Bin = %ld\n", szString, slValue);
  fflush(stdout);
#endif
  if (szString[i] != '\0')                              // not end of string
    return 0;                                           // invalid

  return slValue;
  }


// ------------------------------------------------
// Issue error message for unacceptable keyword value
// ------------------------------------------------
static  ULONG  InvalidValue(char  *szValue) {
  if (!bQuiet)
    printf("ERR: Value '%s' on line %lu of '%s' unrecognised or out of bounds\n",
            szValue, ulLineNumber, szCfgCurr);
  return XW_INVAL;
  }


// --------------------------------------------------------------
//
//  Allocate new region region and 'erase' all bytes
//
//  input:   - size in bytes
//           - pointer to pointer to new region memory
//
//  output:  - pointer to new region
//
//  return:  - result code
//
//  Remarks  - Old regions are not freed, but pointer updated,
//             caller is responsible for freeing old regions
//           - Pointers to ID-, Fuses- and Data-memory are updated
//             when a new Program memory region is allocated
//
// ---------------------------------------------------------------
static  ULONG  NewRegion(ULONG   ulRegionSize,
                          char  **ppucRegion) {

  char   *Region;                                       // ptr to new region

  Region = malloc(ulRegionSize);                        // allocate new
  if (Region == NULL) {                                 // not obtained
    if (!bQuiet)
      printf("ERR: Not enough memory\n");
    return  XW_NOMEM;                                   // error
    }

  BulkErase(ulRegionSize, Region, (USHORT)MINUS1);      // erase memory

  *ppucRegion = Region;                                 // copy to caller
  return XW_NOERROR;                                    // OK, pointer set
  }


// ------------------------------------------------
// Find keyword parameter and its identifier (sequence number)
//
// Input   - pointer to input line
//         - pointer to pointer to keyword value string
//
// Output  - pointer to the keyword value
//         - NULL if no value found
//
// Returns - Keyword sequence number
//         - K_AAA for comment or ignored lines
//         - K_ZZZ when keyword not found
// ------------------------------------------------
static  ULONG  ParseKeyword(char    *cfgfile,
                            char    *line,
                            char   **value) {

  static const struct parse_config {            // Keyword Parameter Spec
       char     id;                             // parameter identifier
       char     len;                            // minimum keyword length
       char    *kwd;                            // keyword string
       } cfg[] = {                              // table of keyword parms
           {K_ALIAS,        4, "Abbreviation"},    // alphabetic
           {K_ALGORITHM,    3, "Algorithm"},
           {K_ALIAS,        5, "Alias"},
           {K_CORE,         4, "Core"},
           {K_PROGSIZE,     6, "CodeSize"},
           {K_PROGSTART,    6, "CodeStart"},
           {K_FUSESSTART,   8, "ConfigStart"},
           {K_FUSESSIZE,    8, "ConfigSize"},
           {K_DATASHEET,    6, "DataSheet"},
           {K_DATASIZE,     6, "DataSize"},
           {K_DATASTART,    6, "DataStart"},
           {K_DELAY,        3, "Delay"},
           {K_DEVID,        8, "DeviceID"},
           {K_DEVIDSIZE,    7, "DevIDSize"},
           {K_DEVIDSTART,   7, "DevIDStart"},
           {K_FAMILY,       3, "Family"},
           {K_FIXEDONE,     6, "FixedOne"},
           {K_FIXEDZERO,    6, "FixedZero"},
           {K_FIXEDONE,    10, "FuseFixedOne"},
           {K_FIXEDZERO,   10, "FuseFixedZero"},
           {K_FIXEDONE,    11, "FusesFixedOne"},
           {K_FIXEDZERO,   11, "FusesFixedZero"},
           {K_FUSESSIZE,    7, "FusesSize"},
           {K_FUSESSTART,   7, "FusesStart"},
           {K_IDSIZE,       4, "IDSize"},
           {K_IDSTART,      4, "IDStart"},
           {K_INCLUDE,      4, "Include"},
           {K_NAME,         4, "Name"},
           {K_NAME,         3, "PIC"},
           {K_PRESERVE,     3, "Preserve"},
           {K_PGMSPEC,      4, "PgmSpec"},
           {K_PGMSPEC,      6, "ProgSpecs"},
           {K_PROGSIZE,     6, "ProgSize"},
           {K_PROGSTART,    6, "ProgStart"},
           {K_PROTECTMASK,  4, "ProtectMask"},
           {K_REVISION,     3, "RevisionMask"},
           {K_DATASHEET,    5, "Sheet"},
           {K_ALIAS,        5, "Shorthand"},
           {K_PGMSPEC,      5, "Specs"},
           {K_STATUS,       4, "Status"},
           {K_DELAY,        3, "Tprog"},
           {K_VPPFIRST,     3, "VppFirst"},
           {K_WRITEBURST,   6, "WriteBurst"},
           {K_ZZZ,          0, ""},            // end of table: kwd not found
             };

  static  char *szKeywordSeparators = " =:\t\n\r";  // keyword separators

  ULONG   i, k;                                 // counter(s)
  ULONG   ulKwd;                                // keyword sequence number
  char   *pszToken;                             // pointer to token

  *value = NULL;                                // init to 'no value string'

  pszToken = strtok(line, szKeywordSeparators);  // isolate keyword
  if (pszToken == NULL)                         // blank line
    return K_AAA;                               // treat as comment line

  if (pszToken[0] == '%'   ||                   // )
      pszToken[0] == '#'   ||                   // ) comment characters
      pszToken[0] == ';')                       // )
    return K_AAA;                               // comment line

  k = strlen(pszToken);                         // length of keyword

  for (i=0; cfg[i].id < K_ZZZ   &&              // whole table
            strnicmp(pszToken, cfg[i].kwd, max(cfg[i].len, k)) != 0;
                   i++)
    i = i;                                      // count until keyword found

  ulKwd = cfg[i].id;                            // keyword (index)value
  if (ulKwd >= K_ZZZ) {                         // keyword not found
    if (!bQuiet)
      printf("ERR: Unknown keyword '%s' on line %lu of '%s'!\n",
              pszToken, ulLineNumber, cfgfile);
    return K_ZZZ;                               // error
    }

  pszToken = strtok(NULL, szKeywordSeparators); // isolate value
  if (pszToken == NULL) {                       // missing value
    if (!bQuiet)
      printf("ERR: No value specified for keyword '%s' on line %lu of '%s'\n",
              cfg[i].kwd, ulLineNumber, szCfgCurr);
    return K_ZZZ;                               // treat as comment and proceed
    }

  *value = pszToken;                            // give pointer to caller

#ifdef __DEBUG__
  printf("K_keyword = %lu, value = %s\n", ulKwd, pszToken);
  fflush(stdout);
#endif

  return ulKwd;                                 // return the keyword number
  }


// ----------------------------------------------
// Read contents of (included) configuration file
//
// Input:   filespec
//
// Output:  chain of PIC properties
//
// Returns: result code - 0: OK
//
// Remarks: whole file will be processed, regardless results of
//          of individual keyword parsing. So probably all errors
//          of a file will be signalled (in stead of terminating
//          immediately after the first error.
//
// ----------------------------------------------
static  int   ReadKeywordFile(char  *szKeywordFile) {

  FILE      *pKwdFile;                                  // file pointer
  PTARGETINFO pT, pTemp;                                // ptr to PIC info
  char      *szValue;                                   // ptr to parm string
  char      *szEnd;                                     // end of num conv.
  char      *pV;                                        // pointer in value
  int        i;                                         // counter(s)
  int        rx;                                        // intermediate rc
  ULONG      ulKeyword;                                 // enum keyword value
  LONG       slTemp,slTemp2;                            // work fields
  char       szBuffer[260];                             // read buffer
  char       szKwdName[260];                            // include pathspec
  char       szWork[10];                                // intermediate buffer
  char       szCfgPrev[260];                            // previous cfg file
  ULONG      ulLinePrev;                                // previous line number
  PPRESERVE  pPres, pPresNew;                           // ptr preserve data

  pKwdFile = fopen(szKeywordFile, "r");                 // open file
  if (pKwdFile == NULL) {                               // error
    if (!bQuiet)
      printf("ERR: Could not open PIC configuration file '%s'\n, errno=%d",
              szKeywordFile, errno);                      // file not found(?)
    return errno;
    }

#ifdef __DEBUG__
  printf("Started reading configuration file %s\n", szKeywordFile);
  fflush(stdout);
#endif

  ulLinePrev = ulLineNumber;                            // save previous
  strcpy(szCfgPrev, szCfgCurr);                         // save previous
  strcpy(szCfgCurr, szKeywordFile);                     // current file

  pT = SearchPICLast(NULL);                             // init target pointer
  ulLineNumber = 0;                                     // line of this file
  rx = XW_NOERROR;                                      // no errors
  while (fgets(szBuffer, sizeof(szBuffer) - 1, pKwdFile) != NULL) {

    ++ulLineNumber;                                     // another line read

#ifdef __DEBUG__
    printf("[%4lu] %s", ulLineNumber, szBuffer);
    fflush(stdout);
#endif

    ulKeyword = ParseKeyword(szKeywordFile, szBuffer, &szValue);  // preprocess

    switch(ulKeyword) {

      case K_AAA:                                       // nothing to do
        break;                                          // (comments, etc.)

      case K_ALGORITHM:                                 // pgmg algorithm
        if (szValue[0] >= '0' && szValue[0] <= '9') {   // numeric
          slTemp = strtol(szValue, NULL, 10);           // decimal
          if (slTemp >= ALG_PIC16  &&  slTemp < ALG_ZZZ)  // check bounds
            pT->Algorithm = slTemp;                     // accepted
          else
            rx = InvalidValue(szValue);                 // error
          }
        else {
          for (i = 0; i < ALG_ZZZ; i++) {
            if ( !stricmp(szValue, szAlgorithmName[i])) {  // matching name
              pT->Algorithm = i;
              break;
              }
            }
          if (i >= ALG_ZZZ)
            rx = InvalidValue(szValue);
          }
        break;

      case K_ALIAS:                                     // abbreviation
        pTemp = SearchPICName(szValue);                 // search name in chain
        if (pTemp == NULL)                              // new name/alias
          pT->Alias = ToUpperCase(strdup(szValue));     // accept
        else {                                          // possible duplicate
          pV = strrchr(pT->Name, 'F');                  // default alias
          if (stricmp(pV, szValue) != 0) {              // not default
            if (!bQuiet)
              printf("ERR: Duplicate specification of '%s' on line %lu of '%s'\n",
                      szValue, ulLineNumber, szCfgCurr);
            rx = XW_INVAL;
            }
          }
        break;

      case K_CORE:                                      // core type
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp == 12)                               // 12-bits core
          ulCore = CORE_12;                             // 10F group (+others)
        else if (slTemp == 14)                          // 14-bits core
          ulCore = CORE_14;                             // 12F/16F group
        else if (slTemp == 15)                          // enhanced 14-bits core
          ulCore = CORE_14H;                            // enhanced 12F/16F group
        else if (slTemp == 16)                          // 16-bits core
          ulCore = CORE_16;                             // 18Fxxx group
        else {
          sprintf(szWork, "%ld", slTemp);
          rx = InvalidValue(szWork);
          }
        break;

      case K_DATASHEET:                                 // datasheet
        pT->DataSheet = strdup(szValue);                // store string
        break;

      case K_DATASIZE:                                  // data memory size
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp >= 0) {                              // positive or zero
          pT->DataSize = slTemp;
          if (szEnd[0] == 'k' || szEnd[0] == 'K')       // expressed in KB
            pT->DataSize *= 1024;                       // make it bytes
          }
        else
          rx = InvalidValue(szValue);
        break;

      case K_DATASTART:                                 // EEPROM location
        slTemp = strtol(szValue, &szEnd, 16);           // hexadecimal
        if (slTemp >= 0)                                // positive or zero
          pT->DataStart = slTemp;
        else                                            // negative
          rx = InvalidValue(szValue);
        break;

      case K_DELAY:                                     // Tprog delay
        slTemp = strtol(szValue, NULL, 10);             // decimal
        if (slTemp >= 0  &&  slTemp <= 0xFF)            // must fit in char
          pT->Delay = (char)slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_DEVID:                                     // PIC device ID
        slTemp = Hex2Long(szValue);                     // hexadecimal
        if (slTemp >= 1 && slTemp <= 0xFFFF) {          // !0 and fit in USHORT
          if (slTemp & 0x000F) {                        // some rev bit(s) set
            slTemp &= ~0x000F;                          // modify DeviceID
            if (!bQuiet)
              printf("WNG: Revision bits of deviceID of %s not zero,"
                     " using %04lX!\n", pT->Name, slTemp);
            }
          for (pTemp = TargetInfo.Next; pTemp != NULL; pTemp = pTemp->Next) {
            if ( ((pTemp->DevID & ~pTemp->RevMask) == slTemp) &&
                 (pTemp->FamilyIndex == pT->FamilyIndex) )
              break;                                    // found
            }
          if (pTemp != NULL) {                          // found
            if (!bQuiet) {
              printf("ERR: Duplicate DeviceID '%s' for '%s'"
                     " on line %lu of '%s',\n",
                     szValue, pT->Name, ulLineNumber, szCfgCurr);
              printf("     DeviceID is the same as of %s\n", pTemp->Name);
              }
            rx = XW_INVAL;
            }
          else
            pT->DevID = (USHORT)slTemp;
          }
        else                                            // out of bounds
          rx = InvalidValue(szValue);
        break;

      case K_DEVIDSIZE:                                 // devID size
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp >= 0)                                // positive or zero
          pT->DevIDSize = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_DEVIDSTART:                                // devID location
        slTemp = strtol(szValue, &szEnd, 16);           // hexadecimal
        if (slTemp >= 0)                                // positive or zero
          pT->DevIDStart = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_FAMILY:
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if      (slTemp == 12)                          // 12-bits core
          pT->FamilyIndex = CORE_12;                    // 10F group (+others)
        else if (slTemp == 14)                          // 14-bits core
          pT->FamilyIndex = CORE_14;                    // 12F/16F group
        else if (slTemp == 15)                          // enhanced 14-bits core
          pT->FamilyIndex = CORE_14H;                   // enhanced 12F/16F group
        else if (slTemp == 16)                          // 16-bits core
          pT->FamilyIndex = CORE_16;                    // 18Fxxx group
        else {
          sprintf(szWork, "%ld", slTemp);
          rx = InvalidValue(szWork);                    // error
          break;                                        // skip rest
          }
        pT->Core = FamilyInfo[pT->FamilyIndex].Core;
        pT->Algorithm = FamilyInfo[pT->FamilyIndex].Algorithm;
        pT->BlankMask = FamilyInfo[pT->FamilyIndex].BlankMask;
        for (i=0; i<sizeof(pT->FuseFixedZero); i++)
          pT->FuseFixedZero[i] = FamilyInfo[pT->FamilyIndex].FuseFixedZero[i];
        memset(pT->FuseFixedOne, 0x00, sizeof(pT->FuseFixedOne));
        pT->ProtectMask = FamilyInfo[pT->FamilyIndex].ProtectMask;
        break;

      case K_FIXEDONE:                                  // fixed 1-bits
        if (pT->FamilyIndex == CORE_12) {               // low range
          if ((strlen(szValue) % 4) != 0)               // not a multiple of 4
            rx = InvalidValue(szValue);                 // error
          else {
            if (pT->FusesSize != 2) {                   // not default length
              if (pT->FusesSize != strlen(szValue) / 2) {  // not matching
                rx = InvalidValue(szValue);             // error
                break;
                }
              }
            else                                        // default length
              pT->FusesSize = strlen(szValue) / 2;      // new(?) length
            szWork[4] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++, i++) {    // all bytes
              strncpy(szWork, szValue + 2*i, 4);        // take next 4 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0') {                   // all digits accepted
                WORD2BYTES((USHORT)slTemp, pT->FuseFixedOne + i);  // store
                }
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        else if (pT->FamilyIndex == CORE_14) {          // mid range
          if ((strlen(szValue) % 4) != 0)               // not a multiple of 4
            rx = InvalidValue(szValue);                 // error
          else {
            if (pT->FusesSize != 2) {                   // not default length
              if (pT->FusesSize != strlen(szValue) / 2) {  // not matching
                rx = InvalidValue(szValue);             // error
                break;
                }
              }
            else                                        // default length
              pT->FusesSize = strlen(szValue) / 2;      // new(?) length
            szWork[4] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++, i++) {    // all bytes
              strncpy(szWork, szValue + 2*i, 4);        // take next 4 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0') {                   // all digits accepted
                WORD2BYTES((USHORT)slTemp, pT->FuseFixedOne + i);  // store
                }
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        else if (pT->FamilyIndex == CORE_16) {          // 18Fxxxx range
          if (strlen(szValue) != 28)                    // 28 hex digits reqd
            rx = InvalidValue(szValue);                 // error
          else {
            szWork[2] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++) {
              strncpy(szWork, szValue + 2*i, 2);        // take next 2 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0')                     // all digits accepted
                pT->FuseFixedOne[i] = (char )slTemp;    // store 'm
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        else {                                          // not (yet) supported
#ifdef __DEBUG__
          printf("PIC family %hu not supported (yet)\n", pT->FamilyIndex);
          fflush(stdout);
#endif
// temp   rx = InvalidValue(szWork);                    // error
          }
        break;

      case K_FIXEDZERO:                                 // Fixed 0-bits
        if (pT->FamilyIndex == CORE_12) {               // mid range (core 14)
          if ((strlen(szValue) % 4) != 0)               // not multiple of 4
            rx = InvalidValue(szValue);                 // error
          else {
            if (pT->FusesSize != 2) {                   // not default length
              if (pT->FusesSize != strlen(szValue) / 2) {  // not matching
                rx = InvalidValue(szValue);             // error
                break;
                }
              }
            else                                        // default length
              pT->FusesSize = strlen(szValue) / 2;      // new(?) length
            szWork[4] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++, i++) {    // all bytes
              strncpy(szWork, szValue + 2*i, 4);        // take next 4 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0') {                   // all digits accepted
                WORD2BYTES((USHORT)slTemp, pT->FuseFixedZero + i);  // store
                }
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        if (pT->FamilyIndex == CORE_14) {               // mid range (core 14)
          if ((strlen(szValue) % 4) != 0)               // not multiple of 4
            rx = InvalidValue(szValue);                 // error
          else {
            if (pT->FusesSize != 2) {                   // not default length
              if (pT->FusesSize != strlen(szValue) / 2) {  // not matching
                rx = InvalidValue(szValue);             // error
                break;
                }
              }
            else                                        // default length
              pT->FusesSize = strlen(szValue) / 2;      // new(?) length
            szWork[4] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++, i++) {    // all bytes
              strncpy(szWork, szValue + 2*i, 4);        // take next 4 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0') {                   // all digits accepted
                WORD2BYTES((USHORT)slTemp, pT->FuseFixedZero + i); // store
                }
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        else if (pT->FamilyIndex == CORE_16) {          // 18Fxxxx range
          if (strlen(szValue) != 28)                    // 28 hex digits reqd
            rx = InvalidValue(szValue);                 // error
          else {
            szWork[2] = '\0';                           // end of string
            for (i=0; i < pT->FusesSize; i++) {
              strncpy(szWork, szValue + 2*i, 2);        // take next 2 bytes
              slTemp = strtol(szWork, &szEnd, 16);      // hex digits only
              if (szEnd[0] == '\0')                     // all digits accepted
                pT->FuseFixedZero[i] = (char )slTemp;   // store 'm
              else
                rx = InvalidValue(szWork);              // error
              }
            }
          }
        else {                                          // not (yet) supported
#ifdef __DEBUG__
          printf("%hu bit core PIC family not supported (yet)\n",
                 pT->FamilyIndex);
          fflush(stdout);
#endif
// temp   rx = InvalidValue(szWork);                    // error
          }
        break;

      case K_FUSESSIZE:                                 // config size
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp >= 0)                                // positive or zero
          pT->FusesSize = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_FUSESSTART:                                // config location
        slTemp = strtol(szValue, &szEnd, 16);           // hexadecimal
        if (slTemp >= 0)                                // positive or zero
          pT->FusesStart = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_IDSIZE:                                    // ID memory size
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp >= 0)                                // positive or zero
          pT->IDSize = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_IDSTART:                                   // ID memory location
        slTemp = strtol(szValue, &szEnd, 16);           // hexadecimal
        if (slTemp >= 0)                                // positive or zero
          pT->IDStart = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_INCLUDE:                                   // include file
        *szKwdName = '\0';                              // empty string
        if (strchr(szValue, ':') == NULL) {             // drive not specified
          if (strchr(szValue, ucDIRECTORYSEPARATOR) == NULL) {  // no dir
            if (strlen(szPathPref) > 0)                 // prefix present
              strcpy(szKwdName, szPathPref);            // path of xwisp2.cfg
            }
          }
        strcat(szKwdName, szValue);                      // (add) filespec
#ifdef __DEBUG__
        printf("Suspended reading of configuration file %s\n", szCfgCurr);
        printf("Include file %s on line %lu of %s\n",
                         szKwdName, ulLineNumber, szCfgCurr);
        fflush(stdout);
#endif
        rx = ReadKeywordFile(szKwdName);                // recursive call!
        break;

      case K_NAME:                                      // PIC name
        pT = addPIC(szValue);                           // new chain element
        if (pT == NULL)                                 // duplicate (or error)
          rx = XW_INVAL;                                // error
        break;

      case K_PGMSPEC:                                   // programming specs
        pT->PgmSpec = strdup(szValue);                  // store string
        break;

      case K_PRESERVE:                                  // bit to preserve
        i = sscanf(szValue, "%04lx,%04lx", &slTemp, &slTemp2);
#ifdef __DEBUG__
        printf("K_PRESERVE: i=%d, temp=%08lX, temp2=%08lX\n",
                i, slTemp, slTemp2);
        fflush(stdout);
#endif
        if ( (i == 2)  &&                                       // 2 values
             (slTemp  >= 0x0000  &&  slTemp  <= 0xFFFF) &&      // USHORT
             (slTemp2 >= 0x0000  &&  slTemp2 <= 0xFFFF) ) {     // USHORT
          pPresNew = malloc(sizeof(PRESERVE));
          if (pPresNew != NULL) {
            pPresNew->next = (PPRESERVE)NULL;           // last in chain
            pPresNew->Address = (USHORT)slTemp;
            pPresNew->BitMask = (USHORT)slTemp2;
            pPres = pT->pPreserve;                      // first in chain
            if (pPres == (PPRESERVE)NULL)               // very first
              pT->pPreserve = pPresNew;
            else {                                      // not first
              while (pPres->next != NULL)               // search last
                pPres = pPres->next;
              pPres->next = pPresNew;                   // add to chain
              }
            }
          else {
            if (!bQuiet)
              printf("ERR: No memory for preservation data on line %lu of '%s'\n",
                      ulLineNumber, szCfgCurr);
            rx = XW_NOMEM;
            }
          }
        else
          rx = InvalidValue(szValue);                   // spec error
        break;

      case K_PROGSIZE:                                  // program memory size
        slTemp = strtol(szValue, &szEnd, 10);           // decimal conversion
        if (slTemp >= 0) {                              // must be pos or zero
          pT->ProgSize = slTemp;
          if (szEnd[0] == 'k' || szEnd[0] == 'K')       // expressed in KB
            pT->ProgSize *= 1024;                       // make it bytes
          }
        else
          rx = InvalidValue(szValue);                   // error
        break;

      case K_PROGSTART:                                 // prog location
        slTemp = strtol(szValue, &szEnd, 16);           // hexadecimal
        if (slTemp >= 0)                                // positive or zero
          pT->ProgStart = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_PROTECTMASK:                               // protection mask
        slTemp = strtol(szValue, NULL, 16);             // Hexadecimal
        if (slTemp >= 0  &&  slTemp <= 0xFFFF)          // must fit in USHORT
          pT->ProtectMask = (USHORT)slTemp;
        else
          rx = InvalidValue(szValue);                   // error
        break;

      case K_REVISION:                                  // revision mask
        slTemp = strtol(szValue, NULL, 16);             // Hexadecimal
        if (slTemp >= 0  &&  slTemp <= 0x3F)            // max 6 bits!!
          pT->RevMask = (USHORT)slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_STATUS:                                    // status info
        pT->Status = WISP648_UNTESTED;                  // undo any previous
        if (stricmp(szValue, "Tested") == 0)            // tested
          pT->Status = WISP648_TESTED;
        else if (stricmp(szValue, "UnTested") == 0)     // untested
          pT->Status = WISP648_UNTESTED;
        else if (stricmp(szValue, "Family") == 0)       // relative
          pT->Status = WISP648_FAMILY;
        else if (stricmp(szValue, "Failed") == 0)       // failed the test
          pT->Status = WISP648_FAILED;
        else
          rx = InvalidValue(szValue);
        break;

      case K_VPPFIRST:
        pT->VppFirst = FALSE;
        if (stricmp("TRUE", szValue) == 0)
           pT->VppFirst = TRUE;
        else if (stricmp("FALSE", szValue) == 0)
           pT->VppFirst = FALSE;
        else
          rx = InvalidValue(szValue);
        break;

      case K_WRITEBURST:
        slTemp = strtol(szValue, &szEnd, 10);           // decimal
        if (slTemp > 0  &&  slTemp <= 256)              // within bounds
          pT->WriteBurst = slTemp;
        else
          rx = InvalidValue(szValue);
        break;

      case K_ZZZ:                                       // Keyword not found
      default:                                          // unknown keyword id
        rx = XW_INVAL;                                  // error
        break;
      }

    if (pT == NULL) {                                   // check for errors like dups
#ifdef __DEBUG__
      printf("Terminated reading configuration file %s due to error\n", szKeywordFile);
      fflush(stdout);
#endif
      break;                                            // terminate reading this file
      }
    }

  fclose(pKwdFile);                                     // done with this file

#ifdef __DEBUG__
  printf("Finished reading configuration file %s\n", szKeywordFile);
  printf("szCfgCurr = '%s', szCfgPrev = '%s'\n", szCfgCurr, szCfgPrev);
  fflush(stdout);
#endif

  strcpy(szCfgCurr,szCfgPrev);                          // restore previous
  ulLineNumber = ulLinePrev;                            // restore previous

#ifdef __DEBUG__
  if (strcmp(szCfgCurr,"") != 0) {                      // not back at highest level
    printf("Resuming with file %s after line %lu\n", szCfgCurr, ulLineNumber);
    fflush(stdout);
    }
#endif

  return  rx;                                           // return max value
  }


// ----------------------------------------------------------------
// Read PIC configuration file(s)
//
// Input:  pointer to array of commandline arguments
//
// Output: chain of PIC properties
//
// Returns: result code - 0: OK
//
// Remarks: searches file "xwisp2.cfg" and its path for includes
//          opens and read "xwisp2.cfg" (and included files)
//
// ---------------------------------------------------------------
extern  int    ReadPICSpecs(char  *commandspec) {

  PTARGETINFO pT;                                       // ptr to PIC info
  int         rc;                                       // return code
  char       *pucSep;                                   // ptr to char
  char        szCfgPath[260];                           // path spec
  char        szKwdFile[260];                           // name of base cfg

  strcpy(szKwdFile, szProgName);                        // construct filespec
  strcat(szKwdFile, szCfgExt);                          // of PIC properties

  rc = OS_FileSearch(szKwdFile, szPathPref, commandspec);  // search file

  if (rc)                                               // .cfg file not found
    return rc;

  pucSep = strrchr(szPathPref, ucDIRECTORYSEPARATOR);
  if (pucSep != NULL)                                   // separator found
    *(pucSep + 1) = '\0';                               // truncate path

#ifdef __DEBUG__
  printf("Configuration file path prefix: '%s'\n", szPathPref);
  fflush(stdout);
#endif

  strcpy(szCfgPath, szPathPref);                        // path
  strcat(szCfgPath, szKwdFile);                         // file
  rc = ReadKeywordFile(szCfgPath);                      // read contents
#ifdef __DEBUG__
  printf("Configuration file(s) processed, rc=%d\n", rc);
  fflush(stdout);
#endif
  if (rc)                                               // problem with kwds
    return rc;

                                                        // post processing
  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
    if (ulProgSize < pT->ProgSize)                      // Prog size larger
      ulProgSize  = pT->ProgSize;                       // new max
    if (ulDataSize < pT->DataSize)                      // Data size larger
      ulDataSize  = pT->DataSize;                       // new max
    pT->DevID &= ~pT->RevMask;                          // reset rev bits
    }

#ifdef __DEBUG__
  printf("Maximum program memory %lu, max data memory %lu\n",
          ulProgSize, ulDataSize);
  fflush(stdout);
#endif
                                                        // post processing
  if (ulProgSize < 0x4400)                              // minimum for mapped
    ulProgSize = 0x4400;                                // datamem midrange
  rc = NewRegion(ulProgSize, &ProgRegion);              // prog memory
  if (rc)
    return rc;
  FusesRegionMidrange = ProgRegion + 0x400E;            // ) stored in
  IDRegionMidrange = ProgRegion + 0x4000;               // ) in Progmem
  DataRegionMidrange = ProgRegion + 0x4200;             // ) for midrange

  rc = NewRegion(ulDataSize, &DataRegion);              // data memory
  if (rc)
    return rc;

  rc = NewRegion(ulIDSize, &IDRegion);                  // ID memory
  if (rc)
    return rc;

  rc = NewRegion(ulFusesSize, &FusesRegion);            // Fuses memory

#ifdef __DEBUG__
  printf("Configuration data ready!\n");
  printf("ProgRegion %08lX DataRegion %08lX "
         "IDRegion %08lX FusesRegion %08lX\n",
          ProgRegion, DataRegion, IDRegion, FusesRegion);
  fflush(stdout);
#endif

  return  rc;
  }


// ---------------------------------------------
// Release PIC properties memory chain
//
// Input:  nothing
//
// Output: nothing
//
// Empties the PIC properties chain
//
// ---------------------------------------------
extern  void  ReleasePICSpecs(void) {

  PTARGETINFO  pTCurrent, pTNext;                       // ptr to targets
  PPRESERVE    pPCurrent, pPNext;                       // ptr to targets
  char        *pV;                                      // ptr to string

#ifdef __DEBUG__
  printf("Releasing PIC specs memory\n");
  fflush(stdout);
#endif
  pTCurrent = TargetInfo.Next;                          // ptr to dummy record
  while (pTCurrent != NULL) {                           // until last entry
    pV = strrchr(pTCurrent->Name, 'F');                 // ptr to def. alias
    if (pV != NULL  &&                                  // found
        pTCurrent->Alias != pV) {                       // not default alias
      free(pTCurrent->Alias);                           // release alias string
      }
    free(pTCurrent->Name);                              // release name string
    pPCurrent = pTCurrent->pPreserve;
    while(pPCurrent != NULL) {                          // preserve ptr
      pPNext = pPCurrent->next;                         // next in chain
      free(pPCurrent);                                  // release current
      pPCurrent = pPNext;
      }
    pTNext = pTCurrent->Next;                           // ptr to next block
    free(pTCurrent);                                    // release block
    pTCurrent = pTNext;                                 // shift
    }
  TargetInfo.Next = NULL;                               // chain is empty

  if (FusesRegion != NULL) {                            // release fuses memory
#ifdef __DEBUG__
    printf("Freeing FusesRegion -> %08lX\n", FusesRegion);
    fflush(stdout);
#endif
    free(FusesRegion);
    FusesRegion = NULL;
    }
  if (IDRegion != NULL) {                              // release ID memory
#ifdef __DEBUG__
    printf("Freeing IDRegion -> %08lX\n", IDRegion);
    fflush(stdout);
#endif
    free(IDRegion);
    IDRegion = NULL;
    }
  if (DataRegion != NULL) {                            // release data memory
#ifdef __DEBUG__
    printf("Freeing DataRegion -> %08lX\n", DataRegion);
    fflush(stdout);
#endif
    free(DataRegion);
    DataRegion = NULL;
    }
  if (ProgRegion != NULL) {                            // release prog memory
#ifdef __DEBUG__
    printf("Freeing ProgRegion -> %08lX\n", ProgRegion);
    fflush(stdout);
#endif
    free(ProgRegion);
    ProgRegion = NULL;
    }

  return;                                               // done
  }


// ---------------------------------------------
// Search PIC by Programming Algorithm
//
// Input:  Programming Algorithm (numeric)
//
// Output: Nothing
//
// Returns pointer to PIC properties (NULL if not found)
//         of first PIC with specified programming algorithm.
//
// ---------------------------------------------
extern  PTARGETINFO  SearchPICAlgorithm(ULONG ulAlgorithm) {

  PTARGETINFO  pT = NULL;

  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
    if (pT->Algorithm == ulAlgorithm)                   // matching algorithm
      break;                                            // found
    }
  return  pT;                                           // returnvalue
  }


// --------------------------------------------------------
// Search PIC by Device ID and Family Index
//
// Input:  - DeviceID of PIC
//         - Family-Index
//
// Output: Nothing
//
// Returns pointer to PIC properties (NULL if not found)
//
// Remarks: - The revision code bits of the DeviceID are not taken
//            into account for device detection.
//          - Second parameter is needed, since there are duplicate
//            DeviceIDs used, e.g. 16F628 and 18F1320.
//          - First search is with a 4-bits revision mask. If no chip
//            found a second search is done with the actual revision mask.
//            This is needed for the 18Fxx23 chips (changed with 1.9.2).
//
// ---------------------------------------------------------
extern  PTARGETINFO  SearchPICDevID(USHORT usDevID,
                                    USHORT ulFamilyIndex) {

  PTARGETINFO  pT;                                      // ptr to target info

#ifdef __DEBUG__
  printf("SearchPICDevID(%04hX,%04hX)\n", usDevID, ulFamilyIndex);
  fflush(stdout);
#endif

  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
#ifdef __DEBUG__
    printf("\tPIC %s, DeviceID=%04hX, RevisionMask=%04hX, Family=%hd\n",
            pT->Name, pT->DevID, pT->RevMask, pT->FamilyIndex);
    fflush(stdout);
#endif
    if ( ((pT->DevID & ~0x000F) == (usDevID & ~0x000F)) &&
         (pT->FamilyIndex == ulFamilyIndex) )
      return pT;                                        // found
    }

  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
    if ( ((pT->DevID & ~pT->RevMask) == (usDevID & ~pT->RevMask)) &&
         (pT->FamilyIndex == ulFamilyIndex) )
      return pT;                                        // found
    }
  return NULL;                                          // not found
  }


// ---------------------------------------------
// Search (first) PIC by Family ID
//
// Input:  Family Index
//
// Output: Nothing
//
// Returns pointer to PIC properties (NULL if not found)
//         of first PIC with specified programming algorithm.
//
// ---------------------------------------------
extern  PTARGETINFO  SearchPICFamily(USHORT usFamilyIndex) {

  PTARGETINFO  pT = NULL;

  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
    if (pT->FamilyIndex == usFamilyIndex)               // matching family
      break;                                            // found
    }
  return  pT;                                           // returnvalue
  }


// ---------------------------------------------
// Search last PIC in chain
//
// Input:  nothing
//
// Output: number of PICs in chain.
//
// Returns pointer to properties of last PIC in chain
//
// ---------------------------------------------
extern  PTARGETINFO SearchPICLast(PULONG pulPICs) {

  ULONG        ulCount;                                 // PIC counter
  PTARGETINFO  pT;                                      // ptr to curr target

  pT = &TargetInfo;                                     // ptr to dummy record
  for (ulCount = 0; pT->Next != NULL; ++ulCount)        // search last entry
    pT = pT->Next;
  if (pulPICs != NULL)                                  // not a null pointer
    *pulPICs = ulCount;                                 // # of PICs in chain
#ifdef __DEBUG__
  printf("Last: %s\n", pT->Name);
  fflush(stdout);
#endif
  return  pT;                                           // ptr to last PIC
  }


// --------------------------------------------------------
// Search PIC by name or alias
//
// Input:  Name or Abbreviation of PIC
//
// Output: Nothing
//
// Returns pointer to PIC properties (NULL if not found)
//
// --------------------------------------------------------
extern  PTARGETINFO  SearchPICName(char *szTargetName) {

  PTARGETINFO  pT;

  for (pT = TargetInfo.Next; pT != NULL; pT = pT->Next) {
    if ( !stricmp(pT->Name, szTargetName) ||
         !stricmp(pT->Alias, szTargetName))
      return pT;                                        // found
    }
  return NULL;                                          // not found
  }


// ------------------------------------------------------------------
// Search PIC by sequence number in chain
//
// Input:  sequence number in chain
//         ('1' returns pointer to first PIC)
//
// Output: Nothing
//
// Returns pointer to PIC properties (NULL if not found)
//
// ------------------------------------------------------------------
extern  PTARGETINFO  SearchPICNumber(ULONG ulNumber) {

  PTARGETINFO  pT;
  ULONG        i;                                       // counter(s)

  pT = TargetInfo.Next;                                 // to first real target
  for (i = 1;  i <= ulNumber  &&  pT != NULL; ++i)
    pT = pT->Next;                                      // next in chain

  return pT;                                            // return ptr (NULL?)
  }


// --------------------------------------
//  Convert string to upper case  ( equivalent of strupr() )
// --------------------------------------
extern  char  *ToUpperCase(char *pszString) {

  char  *pszS = pszString;

  while (*pszS) {
    *pszS = toupper(*pszS);
    ++pszS;
    }

  return pszString;
  }
