
# ----------------------------------------------------------
# Makefile to build xwisp2.exe and xwlist.exe for eComStation (OS/2)
#
# For use with GCC compiler (3.3.5) and GNU make for eComStation
#
# Author: Rob Hamerling
#
# Copyright (c) 2002..2023 R.Hamerling. All rights reserved.
#
# Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
# ----------------------------------------------------------

CC      = gcc
CFLAGS  = -Wall -O2 -funsigned-char -fpack-struct \
          -DVMAJOR=$(VMAJOR) -DVMINOR=$(VMINOR) -DVSUFFIX=$(VSUFFIX)
HEADERS = xwisp2.h compiler.h
OBJECTS = \
        xwisp2bus.o  \
        xwisp2cfg.o  \
        xwisp2cmd.o  \
        xwisp2com.o  \
        xwisp2dat.o  \
        xwisp2hex.o  \
        xwisp2mis.o  \
        xwisp2os.o   \
        xwisp2tgt.o
TARGETS = xwisp2.exe xwlist.exe
SOURCES = $(OBJECTS:.o=.c)

all : $(TARGETS)

.c.o :
        @echo  Compiling $<
        @$(CC) $(CFLAGS) -c -o $@  $<

xwisp2.exe : xwisp2.o $(OBJECTS)
        @echo  Linking $@
        $(CC) $^ -o $@

xwlist.exe : xwlist.o $(OBJECTS)
        @echo  Linking $@
        @$(CC) $^ -o $@

# -- dependents --
xwisp2.o :    xwisp2.c     $(HEADERS)
xwisp2bus.o : xwisp2bus.c  $(HEADERS)
xwisp2cfg.o : xwisp2cfg.c  $(HEADERS)
xwisp2cmd.o : xwisp2cmd.c  $(HEADERS)
xwisp2com.o : xwisp2com.c  $(HEADERS)
xwisp2dat.o : xwisp2dat.c  $(HEADERS)
xwisp2hex.o : xwisp2hex.c  $(HEADERS)
xwisp2mis.o : xwisp2mis.c  $(HEADERS)
xwisp2os.o :  xwisp2os.c   $(HEADERS)
xwisp2tgt.o:  xwisp2tgt.c  $(HEADERS)
xwlist.o :    xwlist.c     $(HEADERS)
