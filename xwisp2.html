<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
                      "http://www.w3.org/TR/html4/strict.dtd">

<!- -------------------------------------------------------- -->
<!-       Copyright (c) Rob Hamerling - 2006..2023.          -->
<!-                                                          -->
<!- Released under the FREEBSD license:                      -->
<!- (http://www.freebsd.org/copyright/freebsd-license.html)  -->
<!- -------------------------------------------------------- -->

<html lang="en">
<head>
  <title>Users Guide and Command Reference for Xwisp2</title>
  <meta http-equiv="content-type" content="text/html; charset=US-ASCII">
  <meta name="author" content="Rob Hamerling">
  <meta name="description"
              content="Users Guide and Command Reference for Xwisp2">
  <style type="text/css">
  body {
       color:       navy;
       background:  ivory;
       font-size:   100%;
       margin:      1em;
       padding:     0px 3px 0px 0px;
       }
  h1 {
       color:       midnightblue;
       text-align:  center;
       font-size:   200%;
       font-style:  oblique;
     }
  h2 {
       color:       midnightblue;
       text-align:  left;
       text-indent: 2em;
       font-size:   150%;
       font-style:  oblique;
     }
  p  {
       margin:      1em 3px 0 3px;
     }
  br {
       clear:       both;
     }
  a  {
       font-weight: bold;
     }
  ul,ol {
       list-style:   inside bullet;
       margin:       0;
     }
  dt {
       font-weight: bold;
       margin:      1em 0 0 0;
     }
  table {
       border:      solid navy 3px;
       border-collapse: collapse;
       margin:      1em 0 0 1em;
       padding:     2em;
     }
  th {
       padding:     0 1em 0 1em;
       font-weight: bold;
       border:      solid navy 2px;
     }
  td {
       padding:     0 1em 0 1em;
       border:      solid navy 1px;
     }

  pre {
       margin:      1em 0 0 1em;
       font-family: courier, monospace;
       font-weight: bold;
     }
  hr {
       margin:      2em 1em 1em 1em;
     }
  </style>

</head>

<!-- ------------------------------------------------------------ -->

<body>

<h1><a name="xwisp2">Users Guide and Command Reference for Xwisp2</a></h1>
<center>
<h4>&copy; Copyright 2006..2023 by Rob Hamerling</h4>
<h4>Released under the
<a href="http://www.freebsd.org/copyright/freebsd-license.html">FREEBSD license</a></h4>
</center>

<center>
<font face="arial,serif" size="-1">
<p><i>This document is partially based on the Wisp documentation
      by Wouter van Ooijen, used with his permission.</i>
</font>
</center>

<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="contents">Contents</a></h2>
<ul>
<li><a href="#intro"   >Introduction</a>
<li><a href="#targets" >Supported Target PICmicros</a>
<li><a href="#overview">Command Overview</a>
<li><a href="#examples">Commandline Examples</a>
<li><a href="#env"     >Environment Variable XWISP2</a>
<li><a href="#fuses"   >Configuration Words</a>
<li><a href="#errors"  >Problem Determination</a>
<li><a href="#cmdref"  >Command Reference</a>
</ul>

<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="intro">Introduction</a></h2>

<p>XWisp2 is a support program for the
<a href="http://www.voti.nl/wisp648" target="_blank">Wisp648</a>
Picmicro programmer of VOTI (Wouter van Ooijen).

<p>Xwisp2 was originally developed for IBM OS/2 and eComStation
(the reincarnation of IBM OS/2 Warp) as alternative for the original
<a href="http://www.voti.nl/xwisp" target="_blank">Xwisp</a>
tool by Wouter.
Reason of this was that an interface routine for the serial port was missing
in the OS/2 version of Python.
In a later stadium of Xwisp2 it was extended to other computer platforms,
in particular for Linux, FreeBSD, MacOS, Windows (W98 and later).
Thanks to the <b>cross-platform</b>
<a href="http://www.openwatcom.org">Open Watcom C/C++ compiler</a>
the eComStation, Linux and Windows versions of Xwisp2 can all be built under
eComStation.

<p>The major differences between this Xwisp2 and the original Xwisp tool:
<ul>
  <li>Xwisp2 is a single autonomic executable, Xwisp requires Python
  <li>Xwisp2 supports a (large) subset of the functions of XWisp
  <li>Specifications of new PICs can be added by the user
</ul>

<p>The Xwisp2 packages contains executables for several platforms:
<ul>
  <li><b>xwisp2.exe</b>   - eComStation
  <li><b>xwisp2u</b>      - Linux
  <li><b>xwisp2w.exe</b>  - Windows
</ul>

<p>Apart from some minor differences all flavours have the same
functionality and when the name <b>Xwisp2</b> is used in this document it
applies to all of its variants.

<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="targets">Supported Target PICmicros</a></h2>

<p>Currently about 160 PICmicros are supported,
and from time to time new chips are added to this collection.
So you might call it a moving target!
The second screen of the HELP command shows a list of targets currently
supported by Xwisp2.

<p>The file <b>xwisp2.cfg</b> is the main configuration file where the
programming parameters for the different PICmicros are defined.
The actual specifications are in separate files, included by the main file.
There are currently two of these configuration files:
<ul>
<li><b>xwisp2_14.cfg</b> for the 14-bits core (most 12Fxxx and 16Fxxx chips)
<li><b>xwisp2_16.cfg</b> for the 16-bits core (18fxxxx chips)
</ul>
A third configuration file (xwisp2_12.cfg) is also available, but the 12-bit core
chips are not yet supported by Xwisp2.
<p>Included in the XWisp2 packages is a utility <b>xwlist</b> (Linux:
xwlistu, Windows: xwlistw), which will show a list of all parameters of all
PICmicros in the configuration files, including unspecified defaults.

<p>You may browse the configuration files, these are plain ASCII text files.
The main file xwisp2.cfg contains information how the parameters have to be
specified, so theoretically you could change parameters or even add new
PICmicros yourself.
But success requires thorough understanding of the "Programming
Specifications" of the target, the working of the Wisp648 firmware and the
way Xwisp2 and Wisp648 firmware co-operate.
This applies especially to the selection of the Programming Algorithm.

<p>Which PICmicros are actually supported by Xwisp2 depends also on the
firmware level of the Wisp648 programmer.
Not surprisingly the more recent the firmware the more targets are
supported!
Where possible and applicable Xwisp2 adapts its behaviour to the firmware
level of the Wisp648 it has detected.
When the combination of Xwisp2 and Wisp648 firmware level will probably not
work together successfully for the actual target PICmicro, Xwisp2 will issue
a warning message or possibly terminate.


<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="overview">Command Overview</a></h2>

<p>Xwisp2 is a commandline program.
Normally it is started from a commandline (terminal) session, but it might
also be called by another program, script or batch file.
The operations it has to perform are controlled by 'commands' which are
specified as commandline arguments when starting Xwisp2.

<p>Some Xwisp2 commands are stand-alone, others are accompanied by a
keyword or a value.
It is important to understand that:
<ul>
  <li>The commands are executed left to right in the sequence of
      specification on the commandline.
  <li>Commands which are meant to modify behaviour of other commands
      (for example <b>port</b> to select the serial port)
      should be put in front of the commands to be influenced.
  <li>Xwisp2 interrupts execution after the first command which didn't
      terminate successfully, or after an unrecognised command or invalid
      corresponding value or keyword.
      Subsequent commands will be ignored.
</ul>

<p>In this section the most common of the available commands are described
briefly to make you familiar with the work with Xwisp2.
The commands supported by Xwisp2 are described in detail in the
<a href="#cmdref">Command Reference</a> section.

<h3>Programming Operations</h3>
<p>For transfer of data from a file to the target and vice versa a
memory buffer is used.
Primitive operations to transfer data to and from this buffer are:
<ul>
  <li><b>Load</b> from file to buffer.
  <li><b>Save</b> from buffer to file.
  <li><b>Put</b> from buffer to target.
  <li><b>Get</b> from target to buffer.
</ul>
<p>Some other primitive operations are:
<ul>
  <li><b>Erase</b>: bulk erase target device.
  <li><b>Check</b>: compare buffer against target.
  <li><b>Run</b>: exit programming state (reset/restart target device).
</ul>
<p>There are also several compound commands available which combine two or
more primitive operations, such as:
<ul>
  <li><b>Write</b> performs a <b>Load</b> file to buffer followed by a
      <b>Put</b> buffer to target.
  <li><b>Read</b> performs a <b>Get</b> target to buffer followed by a
      <b>Save</b> buffer to file.
  <li><b>Verify</b> performs a <Load> file to buffer followed by a
      <b>Check</b> buffer against target.
  <li><b>Go</b> performs an <b>Erase</b> of the target, a <b>Write</b> of the
      file to the target, a <b>Check</b> of buffer against target and
      finally a <b>Run</b> to restart the target device.
</ul>
<p><b>GO</b> is probably the most widely used for normal programming, since
it combines all the necessary operations to replace the old target contents
with new and to start the execution of the new code.
<p>Note: For all commands which need access to the target device the target
is put in 'programming mode' (the /MCLR will be at approximately 12V).
This mode can only be ended with a <b>Run</b> command or a power recycle.
So in most cases your last command will be Run!

<h3>Some Additional Commands</h3>
<p>Under certain circumstances or for specific purposes you may need
additional commands, for example:
<br>When you want to use another than the default serial port of your
computer, you need the Port command to specify the alternative serial port.
<ul>
  <li><b>Port 2</b> selects COM2 (eComStation and Windows variants of
      Xwisp2), /dev/ttyS2 (Linux) or /dev/ttyd2 (FreeBSD).
</ul>
With a USB serial converter under Linux you may have to specify:
<ul>
  <li><b>Port /dev/ttyUSB0</b> or a similar name.
</ul>
<br>When you experience errors with your USB to serial converter, you might
try a lower baudrate than the default of 115200:
<ul>
  <li><b>Port /dev/ttyUSB0 Baud 57600</b> will use a baudrate of 57.6 Kbps.
</ul>
<p>The speed of the
<a href="http://www.picbasic.nl/galvawisp2_uk.htm" target="_blank">
        Galva-Wisp</a>
programmer it limited to 19200 bps.
Since XWisp2 switches by default to high speed when it detects a Wisp648
programmer with recent firmware version (at least 1.28), it is
needed to specify for this programmer:
<ul>
  <li><b>Baud 19200</b> to set the Baudrate to 19.2 Kbps.
</ul>
<i>
Note: From version 1.9.4 XWisp2 sets by default RTS=ON at startup.
With older versions of XWisp2 it was needed to specify also:
<ul>
  <li><b>RTS on</b> to set RTS high.
</ul>
</i>
<p>For problem determination you might want to use:
<ul>
  <li><b>Verbose</b> to see more progress messages.
  <li><b>Log zzz</b> to obtain a log of the programming activity in file
      zzz.log.
</ul>
<p>Consult the <a href="#cmdref">Command Reference</a> section for details of
these commands and a couple of other yet unmentioned commands.

<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="examples">Commandline Examples</a></h2>

<p>Don't be intimidated by the many commands.
For most users the <b>go</b> command, maybe preceded with <b>port 2</b>,
will be the only command ever needed.

<dl>

  <dt>xwisp2 go blink877</dt>
  <dd>This command line erases the target, loads the file blink877.hex into
      the memory buffer, writes the buffer to the target, checks if the
      programming was successful and starts the target (if all goes well!).
      <br>This command line assumes that the first serial port is used
      (COM1, /dev/ttyS0 or /dev/ttyd0),
      and that the device ID of the target PICmicro can be autodetected
      (which will be the case for all flash PICmicros except the obsolete
      16C84 and 16F84).
      <br>The 'go' command may be omitted when a file specification is the
      only commandline argument and the filespec is not a recognised
      command, so the command <b>xwisp2 blink877</b> will work evenly well.
  </dd>

  <dt>xwisp2 port 2 go blink877</dt>
  <dd>When you don't use the default serial port you must specify the actual
      serial port to be used in front of any other port-specific commands.
      In this case serial port COM2 is selected (with eComStation or
      Windows) or /dev/ttyS2 (with Linux) or /dev/ttyd2 (with FreeBSD).
      <br>You may equally well specify '<b>port Com2</b>' or '<b>port
      /dev/ttyS2</b>' or any other name of a serial port known to your
      system.
      <br>Note: The 'go' command cannot be omitted in this case!
  </dd>

  <dt>xwisp2 baud 19200 rts on go blink877</dt>
  <dd>With the
      <a href="http://www.picbasic.nl/galvawisp2_uk.htm" target="_blank">
      Galva-Wisp</a> programmer the baudrate and RTS settings must be
      specified as indicated, and in front of other commands which require
      access to the programmer like 'go' in this case.
  </dd>

</dl>

<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="env">Environment Variable XWISP2</a></h2>

<p>The environment variable XWISP2 may specify a series of commands like
otherwise specified on the commandline.
The commands in the environment string will be executed ahead of the
commands on the commandline.
This may be useful to specify your preferred default runtime environment
like the serial port, baudrate or other settings,
you won't have to repeat these commands all the time.
<br>Example of an environment variable specification with eComStation or
Windows:
<pre>
  set XWISP2=port 2 baudrate 19200 rts on
</pre>
<p>Do not specify spaces between 'XWISP2' and the '=' character!
<p>Similarly with Linux:
<pre>
  export XWISP2="port 2 baudrate 19200 rts on"
</pre>
<p>Be sure to specify 'XWISP2' in upper case!


<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="fuses">Configuration Words</a></h2>

<p>Microchip recommmends in almost every datasheet that the hex file should
contain the configuration word(s).
Unfortunately not all users, compilers and assemblers follow the rules.
Omitting configuration settings from the hex file may cause trouble
with programming PICmicros because the defaults may not be as you think they
are:
datasheet specifications may differ from reality,
revisions may have different defaults, etc.

<p>Programming devices and supporting software like Xwisp2 have there own
vision about defaults for non-specified configuration words!
Xwisp2 uses as default the value of the FuseFixedZero parameter in
xwisp_14.cfg or xwisp2_16.cfg.

<p><b>Never trust defaults, and make sure your hex files always contain the
complete configuration settings!</b>
Obeying this recommendation may save you quite some valuable time for the
analysis of unexpected behaviour of your target caused by wrong assumptions
about defaults.


<!-- ------------------------------------------------------------ -->
<hr>

<h2><a name="errors">Problem Determination</a></h2>

<p>Xwisp2 produces progress messages and maybe warning or error messages.
These messages should be self explanatory.
The most common returncodes (rc) are:
<table>
<tr><th>rc <th>Description
<tr><td> 8 <td>Not enough memory for buffer(s).
<tr><td>10 <td>Detected target deviceID does not exist
               in configuration files.
<tr><td>20 <td>Target could not be identified.
<tr><td>21 <td>Communications error with programmer.
<tr><td>23 <td>Unable to communicate with programmer.
<tr><td>36 <td>Invalid data value in hex file, or data from target.
</table>

<p>In most cases errors are caused by an improper connection,
either between the computer running Xwisp2 and the Wisp648 programmer,
or between the Wisp648 programmer and the target board.

<p>A <b>timeout</b> or <b>serial line status</b> error indicates that
Xwisp2 did not receive a proper response from the Wisp648 programmer.
The most likely cause is a communication problem:
<ul>
  <li>wrong serial port used
  <li>serial port in use by another (terminal) program
  <li>serial cable not connected
  <li>bad connectors
  <li>cable too long
  <li>no power
</ul>

<p>When no programmer (or other data-echoing device!) is connected to the
used port, or you have selected the wrong port, xwisp2w for Windows seems to
'hang' and may have to be terminated with 'Ctrl-C' or 'Ctrl-Break'.

<p>A <b>read</b>, <b>write</b> or <b>Wbus command error</b>
indicates that the target may have responded but not in a way Xwisp2
expected.
This can be caused for instance by a bad connection between the programmer
and the target circuit.
It may also be a result of a wrong target circuit configuration.
Examples of such faults are:
<ul>
  <li>pullup resistor of /MCLR of the target to Vdd is less than 33K or
      missing
  <li>PGC (ICSPCLK) or PGD (ICSPDAT) pins are too heavily loaded
      (e.g. with a LED) <li>/MCLR,
  PGC or PGD pins are wrongly wired to the Wisp648 programmer
      connector (or not at all)
  <li>target power is insufficient: voltage to low (must be 5V) or has not
      enough capacity (target circuit must power the Wisp648 too!)
</ul>

<p>When experiencing programming problems the first thing to do is adding
the commands <b>verbose</b> and <b>log <em>file</em></b> as the first
arguments to your commandline (after the Xwisp2 command itself of course).
These commands will cause Xwisp2 to produce more detailed progress reports
and a log of activity between Wisp648 and the target.

<p><b>Note: </b> Xwisp2 will issue a warning message when it detects a
target which cannot be handled properly by the actual firmware level of the
Wisp648 programmer.
Nevertheless Xwisp2 proceeds, but will fail most likely to transfer the
program to the target.
This behaviour won't harm your target, except that it may be erased
and/or programmed only partially.


<!-- ------------------------------------------------------------ -->
<hr size="5" noshade>

<h1><a name="cmdref">Command Reference</a></h1>

<p>Xwisp2 supports a large subset of the commands of the original Xwisp and
Wisp.
Some 'old' commands are not supported by Xwisp2, some may have a slightly
different result.
The commands supported by Xwisp2 (and only those) are documented below.

<p>Commands and parameters are case independent, except for file names
when used on a case sensitive file system like Linux.

<p>Command parameters are never optional.
When a command requires a value or keyword, this <b>must</b> be specified
with the command even when the default is desired.
So better omit the command when the default value is desired!
But it maybe useful when calling Xwisp2 from a script or another program!

<p>Notation of command parameters:
<ul>
<li>Keyword parameters are described in uppercase,
but may be specified on the commandline in lower case or mixed case.
<li>A value is described as a word between brackets <b>&lt;</b> and
<b>&gt;</b>, but should be specified on the commandline without the brackets.
<li>A choice of one out of several parameters is indicated with
square brackets: <b>[</b> and <b>]</b>.
</ul>

<dl>

  <dt>BAUD &nbsp; &lt;value&gt;</dt>
  <dd>The Baud command sets the baudrate of the serial port to the
      specified (decimal) value.
      <br>Xwisp2 and Wisp648 always start communications at 19200 bps.
      Baudrate will be increased automatically to 115200 during
      programming.
      In some cases this is not desired and a lower baudrate should be used.
      Some examples:
      <ul>
        <li>When you use a USB-serial converter between PC and Wisp648 and
            you experience communications errors a lower baud may help.
            A value of 57600 will do in most cases.
        <li>When you use the
            <a href="http://www.picbasic.nl" target="_blank">Galva-Wisp</a>
            programmer a value of 19200 is required.
      </ul>
  </dd>

  <dt>BEEP</dt>
  <dd>The Beep command causes Xwisp2 to give an audible indication of
      success or failure - using the PC's speaker - when the program
      terminates.
      A series of beeps with rising frequency means successful completion,
      a series of beeps with lowering frequency means unsuccessful completion.
      By default Xwisp2 executes silently.
      <br><i>Note: The Linux version of Xwisp2 doesn't beep!</i>
  </dd>

  <dt><a name="check">CHECK</a></dt>
  <dd>The Check command compares the contents of the buffer with the
      contents of the target.
      Locations which are supposed to be erased
      (code: 0x03FFF/0xFFFF, data: 0xFF)
      in the buffer are not compared with the contents of the target, unless
      the <a href="#full">Full</a> command precedes the check command.
      In that case all of memory is checked.
      Check terminates at the first non-matching location.
      <br>See also the <a href="#verify">Verify</a> command.
  </dd>

  <dt>DELAY &nbsp; &lt;value&gt;</dt>
  <dd>With the Delay command you can specify the programming delay to be
      used by Wisp648 (the Tprog value in the Microchip Programming
      Specifications).
      The value is expressed in units of 0.1 milliseconds, for example a
      value '100' represents 10 milliseconds delay.
      <br>Normally you won't need to use Delay, the default will be OK.
      The Xwisp2_xx.cfg file may contain an alternative (usually lower)
      value, which might be too aggressive for your specific target.
      In that case you may try to slow down programming with a larger
      Delay value.
  </dd>

  <dt>DTR &nbsp; [ ON | OFF ]</dt>
  <dd>The DTR command sets the DTR signal of the serial port high (ON) or
      low (OFF).
      Normally Xwisp2 starts up with DTR low, flips it briefly to high at
      startup, and keeps it low during the rest of programming.
      With the DTR command you may control the DTR signal as you like.
      The DTR command should be used after port selection.
      <br>DTR will be low after Xwisp2 terminated,
          except when finishing in passthrough mode.
  </dd>

  <dt><a name="dump">DUMP</a></dt>
  <dd>The Dump command shows the content of the image on your screen in
      hexadecimal notation.
      The screen output may be redirected to a file, but in that case the
      <a href="#save">Save</a> command might be more appropriate.
  </dd>

  <dt>DUPLEX &nbsp; [ HALF | FULL ]</dt>
  <dd>The Duplex command selects the mode in which Xwisp2 communicates with
      the Wisp648 programmer.
      With FULL (default) both ends may send at the same time,
      with HALF only one end may send at a time.
      <br>You may need this command only with low quality connections
      (long wires) between PC and Wisp648.
      It would be better to improve the connection!
  </dd>

  <dt>ERASE</dt>
  <dd>The Erase command completely (bulk-)erases the target (code-, data-
      and configuration memory).
      This will also remove the code and data protection.
      After Erase the all of memory will contain the 'erased' value
      (code: 0x03FFF/0xFFFF, data: 0xFF).
  </dd>

  <dt><a name="force">FORCE</a> &nbsp; &lt;device&gt;</dt>
  <dd>The Force command overrides the auto-detected target device with the
      specified target device.
      <br>See also the <a href="#target">Target</a> command.
  </dd>

  <dt><a name="full">FULL<a></dt>
  <dd>When the Full command is specified in combination with a
      <a href="#check">Check</a> command or a compound command containing a
      Check operation will ensure that all of memory is verified,
      including locations which are not in the hex file.
      These locations are assumed to contain the 'erased' value
      (code: 0x03FFF/0xFFFF, data: 0xFF).
  </dd>

  <dt><a name="fuses">FUSES</a> &nbsp; [ FILE | IGNORE | &lt;value&gt; ]</dt>
  <dd>With the Fuses command you can modify the fuses word as follows:
      <ul>
        <li>FILE: use the settings in the hex file
        <li>IGNORE: skip fuses
        <li>Use &lt;value&gt; (4 hexadecimal digits) as fuses contents.
            This variant is applicable only to 14-bits PICmicros and
            only for the first word of configuration memory.
      </ul>
      <b>Note:</b><a href="#protect">Protect</a> can be used to modify the
      protection bits of configuration memory in a target-dependent way.
      Modifications are carried out in the order
      (1) <b>fuses</b>, (2) <b>protection</b>.
  </dd>

  <dt>GET</dt>
  <dd>The Get command reads the content of the target into the buffer.
  </dd>

  <dt>GO &nbsp; &lt;file&gt;</dt>
  <dd>The Go command combines the <b>erase</b>, <b>write</b>, <b>check</b>
      and <b>run</b> commands: the target is erased, the indicated
      &lt;file&gt; is written and verified, and the target is put in run
      mode.
  </dd>

  <dt>HELP</dt>
  <dd>The Help command shows a number of help screens:
      <ol>
        <li>A brief command reference summary
        <li>A list of supported PICmicro targets
        <li>Some commandline examples
      </ol>
      Aliases of the HELP command are '?', '-?' and '/?'.
      The help screens are also shown when Xwisp2 is called without parameters.
  </dd>

  <dt>LOAD &nbsp; &lt;file&gt;</dt>
  <dd>The Load command reads the hex file &lt;file&gt; and puts its
      contents into the buffer.
  </dd>

  <dt>LOG &nbsp; &lt;file&gt;</dt>
  <dd>With the Log command details of the programming activity is written to
      the file &lt;file&gt;.
      This command maybe useful for problem determination.
  </dd>

  <dt>PASS &nbsp; [ B67T | B67I | AUXT | AUXI ]</dt>
  <dd>The Pass command puts the target in run mode and enables serial line
      passthrough.
      This is useful when another terminal program will be used to
      communicate with the target without the need to remove the Wisp648
      programmer.
      <br>The mode argument determines for the Wisp648 programmer how the
      programmer passes the serial line to the target:
      <ul>
        <li><b>B6T</b> target pin PGC (frequently B6) is the targets serial
            input, target pin PGD (frequently B7) its serial output.
            The polarity is the same as on an RS232/V.24 line (as if the
            target uses a non-inverting interface).
        <li><b>B6I</b> PGC (B6) is serial input, PGD (B7) serial output.
            The polarity is the the <b>inverse</b> of a RS232/V.24 line
            (as if the target uses an inverting interface like a MAX232).
        <li><b>AUXT</b> the programmers auxiliary lines are used, using true
            polarity (see note 2).
        <li><b>AUXI</b> the programmers auxiliary lines are used, using
            inverted polarity (see note 2).
      </ul>
      <p><b>Notes:</b>
      <ol>
        <li>The auxiliary lines are wires 7 an 8 of the connection between
            Wisp648 and target (wire 7 for data from Wisp648 to target, wire
            8 for data from target to Wisp648).
            These wires should be connected to the corresponding pins of the
            target (wire 7 to the RX-pin, wire 8 to the TX pin), and of
            course your program in the target should be prepared to
            communicate!
      </ol>
      </dd>

  <dt>PAUSE &nbsp; &lt;message&gt;</dt>
  <dd>The Pause command prints the &lt;message&gt; and waits for the user
      to press the &lt;Enter&gt; or &lt;Return&gt; key.
      The &lt;message&gt; may be a single word (no embedded blanks), or it
      may be multiple words enclosed in double quotes, for example:
      <pre>
        pause "Please wait at least 5 seconds!"
      </pre>
      Your message will automatically be followed on the next line by
      <pre>
        >>>Press 'Enter' to continue:
      </pre>
      <b>Notes:</b>
      <ol>
        <li>Xwisp2 will issue a Pause by itself before terminating when it
            encountered an error condition.
        <li>You may consider to specify a Pause at the end of your
            commandline to see the final results, especially when you call
            Xwisp2 from another program, script or batch file.
      </ol>
  </dd>

  <dt>PORT &nbsp; &lt;port&gt;</dt>
  <dd>With the Port command you can specify a number or name for the serial
      port to be used by subsequent commands.
      <br>When specified as a number the default name will be modified.
      For example: when specified as '2' the port name will become COM2
      with eComStation and Windows, /dev/ttyS2 with Linux, or /dev/ttyd2
      with FreeBSD.
      <br>For xwisp2wf (with FTDI API) the port number represents the index
      number of the USB serial converter to be used, default: 0.
      Xwisp2wf currently supports a range 0..3 for the index value.
      <p>When not specified as a number it will be used as port name,
      replacing completely the default name.
      This is not a valid specification for xwisp2wf.
      <br><b>Note:</b> Xwisp2wf determines the device name by reading the
      USB device description string.
  </dd>

  <dt><a name="protect">PROTECT</a> &nbsp; [ ON | OFF | FILE ]</dt>
  <dd>With the Protect command the code and data protection can be set
      ON (all protection bits reset to 0), OFF (all protection bits set to 1)
      or taken from the hex file (default).
      <br>With ON and OFF <b>all</b> of the Code Protection and Data
      Protection (for the 16-bits family also the Boot Block Protection bits)
      are set or reset.
      The protection bitmask is specified in the Xwisp2 configuration file.
      <br><i>Note: The block write protection bits and table read protection
      bits of the targets of the 16-bits family are not touched!</i>
      <br>See also the <a href="#fuses">Fuses</a> command.
  </dd>

  <dt>PUT</dt>
  <dd>The Put command writes the buffer content to the target.
      Only those locations which do not contain the 'erased' value will be
      written (code: 0x03FFF/0xFFFF, data: 0xFF).
  </dd>

  <dt>QUIET</dt>
  <dd>The Quiet command suppresses all console output (to the screen).
      This includes requests for user-interaction ("Press 'Enter' to continue")
      which means the program proceeds without waiting.
      There is no command to undo this action, once processed the program
      proceeds silently till the end.
  </dd>

  <dt>READ &nbsp; &lt;file&gt;</dt>
  <dd>The Read command reads the contents of target memory into the memory
      buffer and writes it to the &lt;file&gt; in Intel hex format.
      The hex file will contain only entries for those locations which do
      not contain the 'erased' value (code: 0x3FFF/0xFFFF, data: 0xFF).
  </dd>

  <dt>RTS &nbsp; [ ON | OFF ]</dt>
  <dd>The RTS command sets RTS high (ON) or low (OFF).
      Normally Xwisp2 starts with RTS high and keeps it high during
      programming.
      With the RTS command you may control the RTS signal as you like.
      <br>The RTS command should be used after port selection.
      <br>RTS will be low after Xwisp2 has terminated,
          except when finishing in passthrough mode.
  </dd>

  <dt>RUN</dt>
  <dd>The Run command resets the target and puts it in run mode
      (removes the high programming voltage from /MCLR).
  </dd>

  <dt><a name="save">SAVE</a> &nbsp; &lt;file&gt;</dt>
  <dd>The Save command writes the contents of the buffer to &lt;file&gt;.
      The file (in Intel hex notation) will contain only entries for those
      locations which are not in the erased state
      (code: 0x3FFF/0xFFFF, data: 0xFF).
      <br>See also the <a href="#dump">Dump</a> command.
  </dd>

  <dt>SELECT &nbsp; &lt;arg&gt;</dt>
  <dd>The Select command determines which part(s) of the buffer (code, data,
      ID-bytes, fuses) are to be included or excluded.
      <br>The &lt;arg&gt; has the format <b>[+|-][C][D][F][I]</b>
      and is interpreted left to right character-by-character.
      <br>The characters '+' and '-' determine the action (include or
      exclude), the letters determine the memory region to which the action
      applies.
      A '+'-character (default) resets the selection to 'none' and switches
      the action to 'include', a '-'-character resets the selection to 'all'
      and switches the action to 'exclude'.
      When a '+' or '-' appears in the middle of the &lt;arg&gt;string the
      previous selection will be undone.
      <br>One or more of the characters 'C', 'D', 'F' and 'I' determine the
      action to the Code, Data, Fuses or ID-memory.
      <br>Some examples:
      <pre>
        select +cd
      </pre>
      Includes code (program) and data (EEPROM) memory regions, excludes
      ID and Fuses memory regions.
      <pre>
        select -fi
      </pre>
      Excludes fuses and ID memory regions, includes program and data memory
      regions
      (thus this example has the same effect as +cd).
  </dd>

  <dt><a name="target">TARGET</a> &nbsp; [ AUTO | &lt;device&gt; | ? ]</dt>
  <dd>With the Target command you can specify the name or alias to identify
      the target PICmicro.
      <br>Most PICmicro devices with flash memory, except the obsolete 16C84
      and 16F84, have a device ID word which can be read to identify the
      target chip (even when the chip is read protected).
      With the specification of a '?' character you can query the actual
      target without starting any programming operation.
      <br>Before any programming operation Xwisp2 will always try to
      indentify the actual target PICmicro type.
      Depending on the conditions Xwisp2 will act as follows:
      <ul>
      <li>When the Target command is not used or specified with AUTO and
          the target device ID is recognised (is listed in one of the
          xwisp2_xx.cfg files) Xwisp2 will proceed normally and use the
          properties of the detected chip.
      <li>When the Target command is not used or specified with AUTO and
          the target device ID is not recognised Xwisp2 will display an
          error message and terminate.
      <li>When the Target specifies a PICmicro which conflicts with the
          detected target Xwisp2 will display a warning message and will
          proceed with the properties of the detected target (as if AUTO was
          specified).
          <br>Use the <a href="#force">Force</a> command when you insist on
          using the properties of the specified target.
      </ul>
      In normal practice the Target command is only required when
      programming a PIC16C84 or PIC16F84.
  </dd>

  <dt>TIME</dt>
  <dd>The Time command will show the current system time.
  </dd>

  <dt>UNATTENDED</dt>
  <dd>The Unattended command suppresses user-interaction ("Press 'Enter'
      to continue") which means the program proceeds without showing
      this message and without waiting for the user interaction.
  </dd>

  <dt>VERBOSE</dt>
  <dd>The Verbose command causes the program to display more progress
      messages of the operation to the screen.
      This command can be useful when experiencing programming failures.
  </dd>

  <dt><a name="verify">VERIFY</a> &nbsp; &lt;file&gt;</dt>
  <dd>The Verify command compares the contents of the target with the
      contents of the specified &lt;file&gt;.
      Locations which are not specified in the &lt;file&gt; are not
      compared unless the <a href="#full">Full</a> command precedes the
      verify command, in which case all of memory is verified.
      Locations in the target which are not specified in the hex file are
      assumed to contain the 'erased' value
      (code: 0x03FFF/0xFFFF, data: 0xFF).
      Verify terminates at the first non matching location.
      <br>See also the <a href="#check">Check</a> command.
  </dd>

  <dt>WAIT &nbsp; &lt;milliseconds&gt;</dt>
  <dd>The Wait command waits (at least) the specified number of
      milliseconds.
  </dd>

  <dt>WRITE &nbsp; &lt;file&gt;</dt>
  <dd>The Write command transfers the indicated &lt;file&gt; to the target.
      Locations of the target which are not specified in the hex file are
      left unchanged.
  </dd>

</dl>

<!-- -------------------------------------------------------------------- -->
<hr>

</body>
</html>
