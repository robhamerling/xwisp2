
// ============================================================
// xwisp2mis.C - Wisp648 Programmer support.
//
//              Various miscellaneous functions
//                - Command help
//                - Error reporting functions
//                - Logging
//                - User interaction
//                - Time formatting
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ============================================================

// #define __DEBUG__

#include "xwisp2.h"                             // all includes

// local function prototypes

static void  MultiBeep(int, int, int, int);


// -------------------------------------------------------
// Ctrl-Break, Ctrl_C, Interrupt handling
//
// Funtion: - reset signal handling
//          - terminate gracefully
// -------------------------------------------------------
extern void Abort(int IntCode) {

  signal(SIGINT,   SIG_DFL);                            // reset
  signal(SIGTERM,  SIG_DFL);                            //

  ProgrammerDeactivate();                               // break,close
  LogStop();                                            // (when active)
  ReleasePICSpecs();                                    // free PIC properties
  if (!bQuiet)
    printf("%s terminated abnormally, rc %d\n", szProgName, IntCode);
  MessageWithReply(NULL);
  if (!bQuiet)
    printf(szTerminate);                                // Termination!
  exit(IntCode);
  }


// --------------------------------------
//   Signal Success
// --------------------------------------
extern void BeepOK(void) {
  if (bBeepEnable)
    MultiBeep(800, 100, 40, 6);
  }


// --------------------------------------
//   Signal Error
// --------------------------------------
extern void BeepError(void) {
  if (bBeepEnable)
    MultiBeep(600, -100, 125, 6);
  }


// --------------------------------------------------------------------
//
// Command syntax help
//
// Note: Depending on macro __ANSIHELP__ (see xwisp2.h)
//       ANSI screen controls are used to indicate active/inactive commands
//
// --------------------------------------------------------------------
extern  int    Help(void) {

#if defined __ANSIHELP__
  #define ANSIPREFIX 7                          // # bytes of ANSI prefix
  #define ANSINULL   "\33[0m"                   // default
  #define ANSIRED    "\33[0;31m"                //
  #define ANSIGREEN  "\33[0;32m"                //
  #define ANSIBLUE   "\33[0;34m"                //
  #define ANSIPINK   "\33[0;35m"                //
  #define ANSICYAN   "\33[0;36m"                //
#else
  #define ANSIPREFIX 0                          // no ANSI prefix
  #define ANSINULL   ""                         // nothing
  #define ANSIRED    ""                         // nothing
  #define ANSIGREEN  ""                         // nothing
  #define ANSIBLUE   ""                         // nothing
  #define ANSIPINK   "*"                        // asterisk
  #define ANSICYAN   ""                         // nothing
#endif

#define TEXTLENGTH 39                           // max length of text
#define HALFLINE   (ANSIPREFIX + TEXTLENGTH)    // max length incl. prefix

static const  char *szHelp[] = {
//                       ....+....1....+....2....+....3....+....4  (max 39)
               ANSIGREEN"BAUD b    : set baudrate b",
               ANSIGREEN"BEEP      : beep at end of operation",
               ANSIGREEN"CHECK     : buffer against target",
               ANSIGREEN"DELAY d   : programming delay (0.1 ms)",
               ANSIGREEN"DTR x     : x = ON or OFF",
               ANSIGREEN"DUMP      : display hex image",
               ANSIGREEN"DUPLEX m  : m = HALF or FULL (Wisp648)",
               ANSIGREEN"ERASE     : erase target",
               ANSIGREEN"FORCE x   : override target with x",
               ANSIGREEN"FULL      : full memory check/verify",
               ANSIGREEN"FUSES x   : x = FILE, IGNORE or value",
               ANSIGREEN"GET       : target to buffer",
               ANSIGREEN"GO f      : erase, write f, check, run",
               ANSIGREEN"HELP      : display these screens",
               ANSIGREEN"LOAD f    : file f to buffer",
               ANSIGREEN"LOG f     : log to file f",
               ANSIGREEN"PASS m    : enable passthrough",
               ANSIGREEN"            m = B6T, B6I, AUXT, AUXI",
               ANSIGREEN"PAUSE \"m\" : print \"m\", wait for Enter",
               ANSIGREEN"PORT x    : use port x (name or number)",
               ANSIGREEN"PROTECT x : x = ON, OFF or FILE",
               ANSIGREEN"PUT       : buffer to target",
               ANSIGREEN"READ f    : get, save f",
               ANSIGREEN"QUIET     : no more progress reporting",
               ANSIGREEN"RTS x     : x = ON or OFF",
               ANSIGREEN"RUN       : put target in run mode",
               ANSIGREEN"SAVE f    : buffer to file",
               ANSIGREEN"SELECT x  : x = one or more of +-CDFI",
               ANSIGREEN"SPEED b   : same as BAUD",
               ANSIGREEN"TARGET x  : x = ? or chip name or AUTO",
               ANSIGREEN"TIME      : show current time",
               ANSIGREEN"UNATTENDED: skip user interaction",
               ANSIGREEN"VERBOSE   : enable screen logging",
               ANSIGREEN"VERIFY f  : load f, check",
               ANSIGREEN"WAIT n    : wait n milliseconds",
               ANSIGREEN"WRITE f   : load f, put",
               ANSIGREEN"",                    //
               ANSIGREEN"",                    // fillers
               ANSIGREEN"",                    //
                };

  int    i, j;                                  // counter(s)
  ULONG  n;                                     // # PICs in database
  PTARGETINFO pT;                               // ptr to target properties

#ifdef __DEBUG__
  printf("Displaying command help info...\n");
#endif

  printf(" %s\n", szCopyRight);
  printf(" %s\n", szLicense);
  j = sizeof(szHelp) / sizeof(szHelp[0]) / 2;   // 2 commands per line
  for (i = 0; i < j; i++)
    printf("%-*.*s %-*.*s\n",
            HALFLINE, HALFLINE, szHelp[i],
            HALFLINE, HALFLINE, szHelp[i + j]);
  printf(ANSINULL);                             // back to default attrib.

  MessageWithReply(NULL);                       // wait for 'Enter'

  printf("\nSpecs of following targets were collected"
         " from the configuration file(s):\n\n");

#define PICS_PER_LINE   8
#define PIC_NAME_LENGTH (80 / PICS_PER_LINE - 1)

  pT = SearchPICLast(&n);                       // obtain # PIC in database
  n = (n + PICS_PER_LINE - 1) / PICS_PER_LINE;  // number of lines
  printf(ANSIGREEN);
  for (i = 0; i < n; ++i) {                     // all lines
    for (j=0; j < PICS_PER_LINE; j++) {         // # PIC names per line
      pT = SearchPICNumber(i + j * n);          // next PIC this line
      if (pT != NULL) {                         // found
        if (pT->Status == WISP648_TESTED)
          printf(ANSIGREEN"+");
        else if (pT->Status == WISP648_FAMILY)
          printf(ANSICYAN"=");
        else if (pT->Status == WISP648_FAILED)
          printf(ANSIRED"-");
        else
          printf(ANSIGREEN" ");
        if (j < PICS_PER_LINE - 1)              // not last on line
          printf("%-*s", PIC_NAME_LENGTH, pT->Name);  // fixed chars per name
        else
          printf("%-s\n", pT->Name);            // last name on line
        }
      else                                      // no more PICs
        printf("\n");                           // end of line
      }
    }

  printf(ANSINULL);                             // back to default attrib.
  printf("  + tested;  = family of tested PIC;  - failed test;  otherwise:"
         " untested!\n");
                                                // some commandline examples
  MessageWithReply("");

  printf("\nSome examples of command lines:\n\n");
  printf("Flash file 'abc.hex' using a Wisp648 on COM1"
         " or /dev/ttyS0 or /dev/ttyd0:\n\n");
  printf(" >"ANSIGREEN" xwisp2 abc\t\t"ANSINULL
          "{first serial port is default, 'go' is not required}\n\n");
  printf("Flash file 'abc.hex' using a Wisp648 on COM3"
         " or /dev/ttyS3 or /dev/ttyd3:\n\n");
  printf(" >"ANSIGREEN" xwisp2 port 3 go abc\t\t"ANSINULL
          "{'go' must be specified in this case}\n\n");
  printf("The environment variable 'XWISP2' may be set to customize"
         " XWisp2 for daily use:\n\n");
  printf(" >"ANSIGREEN" set XWISP2=baud 19200 port 8 verbose\t\t"ANSINULL
          "{eComStation, OS/2, Windows}\n");
  printf(" >"ANSIGREEN" export XWISP2=\"baud 19200 port 8 verbose\"\t"ANSINULL
          "{Linux, FreeBSD}\n\n");

  return XW_NOERROR;                            // OK
  }


// --------------------------------------
// Format Log Message and write to log
// --------------------------------------
extern  void  LogMsg(char *szStr) {

  ULONG  ulTimeNow;

  if (pLogFile != NULL) {                       // log file open
    ulTimeNow = OS_Time();                      // milliseconds since start
    fprintf(pLogFile, "%+6ld.%03lu | %s\n",
                       ulTimeNow / 1000,        // seconds
                       ulTimeNow % 1000,        // milliseconds
                       szStr);                  // text
    fflush(pLogFile);                           // ensure physical write
    }
  }


// ------------------------------------------------
//
// Open log file and start logging
//
// Input    - filespec for log
//          - when 'NULL' specified, 'XWisp2.log' is used
//
// Output   - zero: OK
//          - not-zero: problem
//
// Remarks: - Extension of log file will be 'log'
//
// ------------------------------------------------
extern  int    LogStart(char *szFileName) {

  char   szBuffer[260];                         // string buffer
  time_t ulTime;                                // current time
  int    i;                                     // counter

  if (pLogFile != NULL)                         // already/still open
    LogStop();                                  // terminate current log

  strcpy(szBuffer, (szFileName != NULL) ? szFileName : szProgName);
  if (strrchr(szBuffer, '.') == NULL)           // no extension
    strcat(szBuffer, ".log");                   // add extension

  if ((pLogFile = fopen(szBuffer, "w")) == NULL) {  // open for write
    if (!bQuiet)
      printf("Could not open log file %s\n", szBuffer);
    return  XW_NOENT;
    }

  time(&ulTime);
  fprintf(pLogFile, "\n %s version %d.%d.%d for %s   /   %s\n",
                    szProgName, VMAJOR, VMINOR, VSUFFIX, runtime_platform,
                    ctime(&ulTime));
  if (szEnv != NULL)
    fprintf(pLogFile, " Environment string XWISP2:\n   %s\n", szEnv);
  fprintf(pLogFile, " Commandline:\n   ");
  for (i = 0; szArgv[i] != NULL; i++)
    fprintf(pLogFile, "%s ", szArgv[i]);
  fprintf(pLogFile, "\n\n");
  fprintf(pLogFile, "     time    event\n");
  fprintf(pLogFile, " ----------+--------------------------------\n");
  return XW_NOERROR;
  }


// ------------------------------------------------
//
// Stop logging and close log file
//
// Input  - nothing
//
// Output - nothing
//
// ------------------------------------------------
extern void LogStop(void) {

  if (pLogFile != NULL) {
    fprintf(pLogFile, "\n\n");                  // empty line
    fclose(pLogFile);
    pLogFile = NULL;                            // log closed
    }
  return;
  }


// --------------------------------------------
//
// Send message to user and wait for Enter-key
//
// Input  - string
//
// Output - nothing
//
// Remarks: - No output and request fot 'Enter'
//            in 'Quiet' or 'Unattended' mode
//
// --------------------------------------------
extern  void  MessageWithReply(char *str) {

  char  ucBuffer[8];

  if (!bQuiet  &&  !bUnattended) {
    if (str != NULL  &&                           // not NULL pointer
        strlen(str) > 0)                          // not empty message
      printf("%s\n", str);
    printf(szPressEnter);                         // to continue
    fflush(stdout);
    fgets(ucBuffer, sizeof(ucBuffer), stdin);     // wait for 'Enter'
    }
  return;
  }



// ------------------------------------------------
//   Audiable Signal
//
//   f = start frequency, d = frequency delta,
//   b = sound duration,  n = number of beeps
// ------------------------------------------------
static void MultiBeep(int  f,                   // initial beep frequency
                      int  d,                   // frequency delta
                      int  b,                   // beep duration
                      int  n) {                 // number of beeps

  ULONG i;                                      // counter(s)

  for (i=0; i<n; i++) {                         // number of beeps
    OS_Beep(f, b);                              // sound
    f += d;                                     // next frequency
    }
  }


// -----------------------------------------------------------------
//
//  Report result of last operation
//
//  input:   - name of operation
//           - resultcode
//           - start time (milliseconds since program start)
//
//  return:  nothing (beep if desired)
//
// -----------------------------------------------------------------
extern  int    ReportResult(const char  *szOperation,
                            int          slResultCode,
                            ULONG        ulStartOperation) {

  ULONG  ulTimeNow = OS_Time();                         // current time

  if (slResultCode == XW_NOERROR) {                     // OK
    if (!bQuiet)
      printf("%s terminated successfully in %lu.%02lu seconds\n",
                     szOperation,
                     (ulTimeNow - ulStartOperation) / 1000,
                    ((ulTimeNow - ulStartOperation + 50) / 10) % 100);
    BeepOK();
    }
  else {                                                // problem
    if (!bQuiet)
      printf("%s failed after %lu.%02lu seconds, rc %d\n",
                     szOperation,
                     (ulTimeNow - ulStartOperation) / 1000,
                    ((ulTimeNow - ulStartOperation + 50) / 10) % 100,
                     slResultCode);
    BeepError();
    }
  return slResultCode;
  }


// ----------------------------------------------------------------
//
//  Time of day
//
//  input:  - none
//
//  output: - time of day string
//
//  returns pointer to formatted time of day
//
// ----------------------------------------------------------------
extern  char  *TimeString(void) {

  static char   szBuffer[16];                           // format buffer
  time_t        timenow;                                // time value
  struct tm    *timeloc;                                // local time struct

  time(&timenow);                                       // current time
  timeloc = localtime(&timenow);                        // obtain local
  sprintf(szBuffer, "%2d:%02d:%02d",
                    timeloc->tm_hour,
                    timeloc->tm_min,
                    timeloc->tm_sec);
  return  szBuffer;
  }
