
// ===================================================================
// XWisp2Dat.C - Wisp648 Programmer support library
//
//             - global Constants and variables
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// Note: This file contains the defining declarations of global
//       constants and variables (for Xwisp2 and Xwlist).
//       The storage class 'extern' is omitted.
//       The file xwisp2.h constants the referencing declarations
//       with the storage class 'extern'.
//       This seems to be the method with which most compilers are happy.
//       (at least Open Watcom C/C++, IBM Visual Age C/C++, GCC).
//
// ===================================================================

#include "xwisp2.h"                             // all includes


// constants

const  char   *szAuthorName     = "Rob Hamerling",
              *szProgName       = "xwisp2",
              *szCopyRight      = "Copyright (c) 2002..2023,"
                                  " R. Hamerling. All rights reserved.",
              *szLicense        = "Released under the FREEBSD license"
                                  " (http://www.freebsd.org/copyright/freebsd-license.html)";

const  char  *szAlgorithmName[] =
                   {"PIC16",  "PIC16A", "PIC16B", "PIC18",
                    "PIC16C", "PIC16D", "PIC16E", "PIC16F",
                    "PIC12",  "PIC16G", "PIC18A", "PIC16H",
                    "PIC16I",
                    "",                                 // last
                    };                                  // must match enum in Xwisp2.h!!

const  char  *szPressEnter = "Press 'Enter' to continue: ";
const  char  *szTerminate = " \b\n";                    // terminate string

const  ULONG  ulWbusBaudrateMinimum = 10;               // default min
const  ULONG  ulWbusBaudrateMaximum = 115200;           // default max
const  ULONG  ulWbusBaudrateDefault = 19200;            // default wisp648

// global variables

char  **szArgv              = NULL;                     // commandline args
char   *szEnv               = NULL;                     // environment string

char   *szWbusDevice        = NULL;                     // Wbus device name
char   *ProgRegion          = NULL;                     // common progmem
char   *DataRegion          = NULL;                     // datamem 18F
char   *IDRegion            = NULL;                     // IDmem 18F
char   *FusesRegion         = NULL;                     // Fusesmem 18F
char   *DataRegionMidrange  = NULL;                     // ) mapped and stored
char   *IDRegionMidrange    = NULL;                     // ) in progmem!
char   *FusesRegionMidrange = NULL;                     // ) for midrange

FILE   *pLogFile            = NULL;

ULONG   ulProgSize          = 0;                        // prog memory
ULONG   ulDataSize          = 0;                        // data memory
ULONG   ulIDSize            = 8;                        // ID memory
ULONG   ulFusesSize         = 14;                       // fuses memory

ULONG   fBurstWrite         = BURSTWRITE_FDX;           // full duplex default
ULONG   fProtection         = PROT_HEX;                 // hex file contents
ULONG   fRegions            = REGION_ALL;               // all regions involved
ULONG   ulWispLevel         = 0;                        // firmware 100*Maj+Min
ULONG   ulLBA               = 0;                        // base addr
ULONG   ulLastAddress       = MINUS1;                   // invalid address (-1)
ULONG   ulWbusBaudrateActual = 19200;                   // initial speed
ULONG   ulWbusBaudrateUser  = 0;                        // user specified speed
ULONG   ulWispState         = WISPSLEEP;                // default state

BOOLEAN bBeepEnable         = FALSE;                    // no beeps
BOOLEAN bDTRstate           = FALSE;                    // state of DTR
BOOLEAN bFullVerify         = FALSE;                    // non blank memory
BOOLEAN bLazyWrite          = FALSE;                    // no lazy writes
BOOLEAN bBurstReading       = FALSE;                    // burst reading
BOOLEAN bRTSstate           = TRUE;                     // state of RTS
BOOLEAN bQuiet              = FALSE;                    // show console output
BOOLEAN bUnattended         = FALSE;                    // with user interaction
BOOLEAN bVerbose            = FALSE;                    // extensive user info

USHORT  usFusesOverride     = 0x0000;                   // no override
USHORT  usDelay             = 0;                        // default pgmg delay

PTARGETINFO pTSpec          = NULL;                     // Specified target
PTARGETINFO pTUsed          = NULL;                     // Used target
