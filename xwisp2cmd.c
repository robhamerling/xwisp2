
// ============================================================
// XWisp2Cmd.c  - Wisp648 programmer support library
//
//              - Wisp648 user Command handling
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ============================================================

#define __DEBUG__

#include "xwisp2.h"                             // all includes

//  Symbolic names of command words
enum _keyword { K_BAUD,
                K_BEEP,
                K_CHECK,
                K_DELAY,
                K_DTR,
                K_DUMP,
                K_DUPLEX,
                K_ERASE,
                K_FORCE,
                K_FULL,
                K_FUSES,
                K_GET,
                K_GO,
                K_HELP,
                K_LOAD,
                K_LOG,
                K_PASS,
                K_PAUSE,
                K_PORT,
                K_PROTECT,
                K_PUT,
                K_READ,
                K_QUIET,
                K_RTS,
                K_RUN,
                K_SAVE,
                K_SELECT,
                K_TARGET,
                K_TIME,
                K_UNATTENDED,
                K_VERBOSE,
                K_VERIFY,
                K_WAIT,
                K_WRITE,
                K_ZZZ};                         // must be last!

// locally use global contants and variables

static const struct _command {                  // Keyword
       char     nbr;                            // parameter identifier
       char     ll;                             // minimum required length
       char    *word;                           // command string
       } cmdtable[] = {                         // table of command words
           {K_BAUD,      3, "baud"},            // alphabetic on cmd word
           {K_BEEP,      3, "beep"},
           {K_CHECK,     5, "check"},
           {K_DELAY,     5, "delay"},
           {K_DTR,       3, "dtr"},
           {K_DUMP,      4, "dump"},
           {K_DUPLEX,    3, "duplex"},
           {K_ERASE,     5, "erase"},
           {K_FORCE,     5, "force"},
           {K_FULL,      4, "full"},
           {K_FUSES,     4, "fuses"},
           {K_GET,       3, "get"},
           {K_GO,        2, "go"},
           {K_HELP,      4, "help"},
           {K_LOAD,      4, "load"},
           {K_LOG,       3, "log"},
           {K_PASS,      4, "pass"},
           {K_PAUSE,     3, "pause"},
           {K_PORT,      4, "port"},
           {K_PROTECT,   4, "protect"},
           {K_PUT,       3, "put"},
           {K_READ,      3, "read"},
           {K_QUIET,     1, "quiet"},
           {K_RTS,       3, "rts"},
           {K_RUN,       3, "run"},
           {K_SAVE,      4, "save"},
           {K_SELECT,    3, "select"},
           {K_BAUD,      5, "speed"},
           {K_TARGET,    4, "target"},
           {K_TIME,      4, "time"},
           {K_DELAY,     4, "tprog"},           // synonym of delay
           {K_UNATTENDED,1, "unattended"},      // no user interaction
           {K_VERBOSE,   4, "verbose"},
           {K_VERIFY,    4, "verify"},
           {K_WAIT,      4, "wait"},
           {K_WRITE,     5, "write"},
           {K_HELP,      1, "?"},
           {K_HELP,      2, "/?"},
           {K_HELP,      2, "-?"},
           {K_ZZZ,       0, ""},                // end of table: kwd not found
             };

static const char   *szNulString    = "";
static const char   *aszModeText[4] = {"B6T", "B6I", "AUXT", "AUXI"};

static       ULONG   ulCurrentParm  = 0;        // argument index
static       char  **szParmArray    = NULL;     // 'argv'
static       char   *szHexFileSpec  = NULL;     // hex file

static       BOOLEAN bWelcomeFlag   = FALSE;    // do not show welcome msg (yet)

static       ULONG   ModeTextIndex  = 0;        // PASS mode (default)


// protoypes of locally used functions

static  ULONG  HandleCommand(ULONG, char *);
static  ULONG  GetModeIndex(char *);
static  char  *NextParameter(BOOLEAN);
static  ULONG  NewHexFileSpec(void);
static  ULONG  ParseCommandWord(char *);
static  ULONG  WrongArgument(ULONG, char *, char *);


// --------------------------------------------------------------------
// Actual command handling
// Input   - list of parameters (commandline: argc and argv)
// Output  - nothing
// Returns - result of last command executed
// Note: welcomeflag initially false: no welcome, no config file reading
// --------------------------------------------------------------------
extern  int    CommandHandling(int    slArguments,      // 'argc'
                               char **szArguments) {    // 'argv'

  int    rc = XW_NOERROR;                               // return code
  ULONG  ulCmdCode;                                     // command code
  char  *s;                                             // pointer to command
  char   ucBuffer[8];                                   // user I/O

  ulCurrentParm = 0;                                    // reset counter
  szParmArray = szArguments;                            // store ptr to argv

  if (slArguments > 1) {
    ulCmdCode = ParseCommandWord(szArguments[1]);       // first argument
    if (ulCmdCode == K_QUIET)                           // 'quiet' command
      bQuiet = TRUE;                                    // no console output
    }

  if (bWelcomeFlag == TRUE) {                           // may welcome user
    if (!bQuiet)
      printf(" %s version %d.%d.%d for %s (%s, %s %d.%d)\n",
             szProgName, VMAJOR, VMINOR, VSUFFIX, runtime_platform,
             __DATE__, compiler_name, compiler_version, compiler_minor);

    rc = ReadPICSpecs(szArguments[0]);                  // read PIC properties
    if (rc) {                                           // problem
      BeepError();
      if (!bQuiet) {
        printf(szPressEnter);
        fflush(stdout);
        fgets(ucBuffer, sizeof(ucBuffer), stdin);       // wait for 'Enter'
        }
      return rc;
      }
    }

  if (slArguments <= 1)                                 // no arguments
    return Help();                                      // show help screen

  if (ulCmdCode >= K_ZZZ  &&                            // command unknown
      slArguments == 2)                                 // only 1 parameter
    return HandleCommand(K_GO, "Go");                   // assume hexfile!

  do {                                                  // all args
    if ((s = NextParameter(CMD_OPT)) == NULL)           // next argument
      break;                                            // all done
    ulCmdCode = ParseCommandWord(s);                    // check if exists
    rc = HandleCommand(ulCmdCode, s);                   // handle command
    } while (rc == XW_NOERROR);                         // still no errors

  return rc;                                            // all done
  }


// --------------------------------------------------------------------
// Environment string handling
// Input   - nothing (enviroment string obtained internally)
// Output  - nothing (global parameters may be changed)
// Returns - nothing
// --------------------------------------------------------------------
extern  void  EnvironmentHandling(void) {

  int     i;                                            // counter
  char   *pszEnvStr, *s;                                // string pointers
  char   *pszArgument[32];                              // array of pointers
  char    szEnvStrTmp[16];                              // work string

  strcpy(szEnvStrTmp, szProgName);                      // name of env var.
  pszEnvStr = getenv(ToUpperCase(szEnvStrTmp));         // get env

  if (pszEnvStr == NULL) {                              // not found
    bWelcomeFlag = TRUE;
    return;                                             // done
    }

  szEnv = strdup(pszEnvStr);                            // copy for log

  pszArgument[0] = (char *)szProgName;                  // program name
  s = strtok(pszEnvStr, " ,");                          // first token
  for (i=1; s != NULL && i < 31; i++) {                 // all (max 31) tokens
    pszArgument[i] = s;                                 // store pointer
    s = strtok(NULL, " ,");                             // next word
    }
  pszArgument[i] = NULL;                                // end of array

  if (i > 1)                                            // any environment cmds
    CommandHandling(i, pszArgument);                    // process these!

  // Note: Reported AFTER handling environment string,
  //       which may contain the 'Verbose' or 'Quiet' command
  if (bVerbose  &&  !bQuiet) {
    printf("Processing Environmentstring:\n %s = ", pszArgument[0]);
    for (i=1; pszArgument[i] != NULL; i++)              // all (max 31) tokens
      printf("%s ", pszArgument[i]);                    // item
    printf("\n");
    }

  bWelcomeFlag = TRUE;                                  // may now welcome user!

  return;                                               // all done
  }


// ---------------------------------------------------
//
// Handle Wisp2 commandline arguments
//
// ---------------------------------------------------
static  ULONG  HandleCommand(ULONG  ulCmdCode,
                             char  *szCommand) {

  ULONG   j;                                            // counter(s)
  char   *s;                                            // ptr to command parm
  char   *sTemp;                                        // ptr to string
  long    v;                                            // numeric parm value
  BOOLEAN bRegionAdd;                                   // select flag
  ULONG   rc, ulTemp;                                   // returncode
  PTARGETINFO pT;                                       // ptr to PIC data

  rc = XW_NOERROR;                                      // default returncode

  switch(ulCmdCode) {

    case K_BAUD:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        v = atol(s);                                    // must be numeric
        ulWbusBaudrateUser = v;                         // user specified
        if (bVerbose  && !bQuiet)
          printf("Requested communications speed: %lu bps\n",
                  ulWbusBaudrateUser);
        ProgrammerDeactivate();                         // must re-init port
        }
      break;

    case K_BEEP:
      bBeepEnable = TRUE;                               // completion signal
      break;

    case K_CHECK:
      rc = VerifyTarget();                              // image <-> Target
      break;

    case K_DELAY:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        v = atol(s);
        if (v < 0  ||  v > 0xFF  ||  s[0] < '0' || s[0] > '9')
          rc = WrongArgument(ulCmdCode, s, ", value must be in range 0..255");
        else
          usDelay = (USHORT)v;
        }
      break;

    case K_DTR:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "OFF"))
          SetModemOutputStatus(FALSE, bRTSstate);       // reset DTR
        else if (!stricmp(s, "ON"))
          SetModemOutputStatus(TRUE, bRTSstate);        // set DTR
        else
          rc = WrongArgument(ulCmdCode, s, ", must be ON or OFF");
        }
      break;

    case K_DUMP:
      rc = DumpImage();                                 // image -> screen
      break;

    case K_DUPLEX:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "FULL"))                        // FDX
          fBurstWrite |= BURSTWRITE_FDX;
        else if (!stricmp(s, "HALF"))                   // HDX
          fBurstWrite &= ~BURSTWRITE_FDX;
        else
          rc = WrongArgument(ulCmdCode, s, ", must be FULL or HALF");
        }
      break;

    case K_ERASE:
      rc = EraseTarget();                               // bulk erase target
      break;

    case K_FORCE:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        pT = SearchPICName(s);                          // search PIC
        if (pT != NULL) {                               // found!
          pTUsed = pT;                                  // used properties
          pTSpec = pT;                                  // force!
          }
        else
          rc = WrongArgument(ulCmdCode, s, " (chip properties unknown)");
        }
      break;

    case K_FULL:
      bFullVerify = TRUE;                               // verify all memory
      break;

    case K_FUSES:                                       // modify fuses
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "IGNORE"))
          fRegions &= ~REGION_FUSES;                    // exclude fuses
        else if (!stricmp(s, "FILE")) {
          fRegions |= REGION_FUSES;                     // include fuses
          usFusesOverride = 0x0000;                     // no override
          }
        else if ((ulTemp = strtoul(s,NULL,16)) <= 0xFFFF) {  // hex
          fRegions |= REGION_FUSES;                     // include fuses
          usFusesOverride = (USHORT)ulTemp;             // overriding word
          if (bVerbose  &&  !bQuiet)
            printf("New fuses %04hX\n", usFusesOverride);
          }
        else
          rc = WrongArgument(ulCmdCode, s,
                             ", must be IGNORE, FILE or 4 hex digits");
        }
      break;

    case K_GET:
      rc = ReadTarget();                                // target -> Image
      break;

    case K_GO:
      if ((rc = NewHexFileSpec()) == XW_NOERROR) {
        if ((rc = ReadHexFile(szHexFileSpec)) == XW_NOERROR) { // -> image
          if ((rc = EraseTarget()) == XW_NOERROR) {
            if ((rc = WriteVerifyTarget()) == XW_NOERROR) {     // -> target
              rc = RunTarget();                         // target in run mode
              }
            }
          }
        }
      break;

    case K_HELP:
      rc = Help();
      break;

    case K_LOAD:
      if ((rc = NewHexFileSpec()) == XW_NOERROR)        // new name
        rc = ReadHexFile(szHexFileSpec);                // file -> image
      break;

    case K_LOG:
      LogStop();                                        // when logging active
      s = NextParameter(CMD_REQ);
      if (s == NULL)
        rc = XW_INVAL;
      else
        rc = LogStart(s);                               // log to file 's'
      break;

    case K_PASS:
      ProgrammerDeactivate();                           // terminate pgmg
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        ModeTextIndex = GetModeIndex(s);                // determine mode
        if ((rc = ProgrammerActivate()) != XW_NOERROR)  // re-init
          rc = XW_NOENT;
        else {
          IdentifyTarget();                             // detect target
          if (pTUsed == NULL)                           // undetermined
            rc = XW_NOENT;
          else {
            if (!bQuiet)
              printf("Entering PassThrough mode: %s\n",
                              aszModeText[ModeTextIndex]);
            rc = WbusPassThrough(ModeTextIndex);
            }
          }
        }
      break;

    case K_PAUSE:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else
        MessageWithReply(s);
      break;

    case K_PORT:
      if ((s = NextParameter(CMD_REQ)) == NULL)         // parm required
        rc = XW_INVAL;
      else {
        ProgrammerDeactivate();                         // must re-init port
        if (strlen(s) > 3)                              // probably port name
          szPortName = strdup(s);                       // new port name
        else {                                          // probably port number
          v = atol(s);                                  // supposedly numeric
          sTemp = malloc(sizeof(szPortName) + 16);      // for new name
          strcpy(sTemp, szPortName);                    // take default name
          sprintf(sTemp + strlen(szPortName) - 1, "%ld", v);  // new suffix
          szPortName = sTemp;                           // ptr to new name
          ulPortIndex = v;                              // port index
          }
        }
      break;

    case K_PROTECT:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "ON")) {
          fRegions |= REGION_FUSES;                     // include fuses
          fProtection = PROT_ALWAYS;
          }
        else if (!stricmp(s, "OFF")) {
          fRegions |= REGION_FUSES;                     // include fuses
          fProtection = PROT_NEVER;
          }
        else if (!stricmp(s, "FILE")) {
          fRegions |= REGION_FUSES;                     // include fuses
          fProtection = PROT_HEX;                       // when fuses in file!
          }
        else
          rc = WrongArgument(ulCmdCode, s, ", should specify ON, OFF or FILE");
        }
      break;

    case K_PUT:
      rc = WriteTarget();                               // image -> target
      break;

    case K_QUIET:
      bQuiet = TRUE;                                    // no console output
      break;

    case K_READ:
      if ((rc = NewHexFileSpec()) == XW_NOERROR) {      // new name
        if ((rc = ReadTarget()) == XW_NOERROR)          // target -> image
          rc = WriteHexFile(szHexFileSpec);             // image -> hex file
        }
      break;

    case K_RTS:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "OFF"))
          SetModemOutputStatus(bDTRstate, FALSE);       // reset RTS
        else if (!stricmp(s, "ON"))
          SetModemOutputStatus(bDTRstate, TRUE);        // set RTS
        else
          rc = WrongArgument(ulCmdCode, s, ", must be ON or OFF");
        }
      break;

    case K_RUN:
      rc = RunTarget();
      break;

    case K_SAVE:
      if ((rc = NewHexFileSpec()) == XW_NOERROR)        // new name
        rc = WriteHexFile(szHexFileSpec);               // image -> file
      break;

    case K_SELECT:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        bRegionAdd = TRUE;                              // default: 'add'
        fRegions = 0;                                   // default: 'none'
        for (j=0; j<strlen(s); j++) {
          switch(toupper(s[j])) {
            case 'C': (bRegionAdd) ? (fRegions |= REGION_PROG) :
                                     (fRegions &= ~REGION_PROG);
                      break;
            case 'D': (bRegionAdd) ? (fRegions |= REGION_DATA) :
                                     (fRegions &= ~REGION_DATA);
                      break;
            case 'F': (bRegionAdd) ? (fRegions |=  REGION_FUSES) :
                                     (fRegions &= ~REGION_FUSES);
                      break;
            case 'I': (bRegionAdd) ? (fRegions |=  REGION_ID) :
                                     (fRegions &= ~REGION_ID);
                      break;
            case '+': bRegionAdd = TRUE;
                      fRegions = 0;                     // select none
                      break;
            case '-': bRegionAdd = FALSE;
                      fRegions = REGION_ALL;            // select all!
                      break;
            default:  rc = WrongArgument(ulCmdCode, s,
                                         ", contains invalid character(s)");
                      break;
            }
          }
        }
      if (bVerbose  &&  !bQuiet)
        printf("Will be handling memory regions:%s%s%s%s\n",
                         (fRegions & REGION_PROG)  ? " Code"  : "",
                         (fRegions & REGION_DATA)  ? " Data"  : "",
                         (fRegions & REGION_ID)    ? " ID"    : "",
                         (fRegions & REGION_FUSES) ? " Fuses" : "");
      break;

    case K_TARGET:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        if (!stricmp(s, "AUTO"))                        // to be detected
          pTUsed = NULL;                                // reset
        else if (s[0] == '?') {                         // query
          ProgrammerDeactivate();                       // reset
          if ((rc = ProgrammerActivate()) != XW_NOERROR)  // detect target type
            return rc;                                  // problem
          IdentifyTarget();                             // detect target
          }
        else {
          pT = SearchPICName(s);                        // search PIC
          if (pT != NULL) {                             // found!
            pTSpec = pT;                                // spec's props
            if (pTUsed != NULL)                         // old target known
              pTUsed = pT;                              // replace by spec'd.
            }
          else
            rc = WrongArgument(ulCmdCode, s,
                               " (chip properties not available)");
          }
        }
      break;

    case K_TIME:
      if (!bQuiet)
        printf("%s\n", TimeString());
      break;

    case K_UNATTENDED:
      bUnattended = TRUE;                               // no user interaction
      break;

    case K_VERBOSE:
      bVerbose = TRUE;                                  // progress report
      break;

    case K_VERIFY:
      if ((rc = NewHexFileSpec()) == XW_NOERROR) {      // filespec OK
        if ((rc = ReadHexFile(szHexFileSpec)) == XW_NOERROR)  // -> image
          rc = VerifyTarget();                          // image <-> target
        }
      break;

    case K_WAIT:
      if ((s = NextParameter(CMD_REQ)) == NULL)
        rc = XW_INVAL;
      else {
        v = atol(s);
        if (v > 0) {                                    // zero, neg, not num
          if (bVerbose  &&  !bQuiet)
            printf("Waiting %lu ms\n", v);
          OS_Sleep(v);                                  // sleep # ms
          }
        else
          rc = WrongArgument(ulCmdCode, s, NULL);
        }
      break;

    case K_WRITE:
      if ((rc = NewHexFileSpec()) == XW_NOERROR) {      // new name
        if ((rc = ReadHexFile(szHexFileSpec)) == XW_NOERROR)  // read file
          rc = WriteTarget();                           // image -> target
        }
      break;

    default:
      if (!bQuiet) {
        printf("Command '%s' unknown or not currently implemented.\n", szCommand);
        if (!strnicmp(szCommand, "COM", 3))
          printf("If you intended to select a serial port,"
                 " use the 'Port x' command.\n");
        }
      rc = XW_INVAL;
      break;

    }

  if (rc)                                               // problem
    ProgrammerDeactivate();                             // deact when active

  return rc;                                            // last result
  }


// -----------------------------------------------------------
//  Convert pass mode to index for passthrough wbus command
//
//  input   - Passthrough mode word (upper case)
//
//  returns - index in range 0..4
//          - MINUS1 if mode not supported
//
// -----------------------------------------------------------
static  ULONG  GetModeIndex(char *szMode) {

  ULONG i;                                              // counter

  for (i=0; i<sizeof(aszModeText)/sizeof(char *); i++) {
    if (!stricmp(szMode, aszModeText[i]))
      return i;
    }
  if (!bQuiet)
    printf("Passthrough mode %s not supported, using %s\n",
            szMode, aszModeText[0]);
  return 0;                                             // use default
  }


// --------------------------------------------------------------------
// Create new hex file specification
// Input   - next parameter
// Output  - szHexFileName pointing to new filespec
//         - when no extension specified '.hex' will be appended
// Returns - zero if OK
//         - non-zero: No new filespec, old filespec (if any) is retained.
// --------------------------------------------------------------------
static  ULONG  NewHexFileSpec(void) {

  char  *f;                                             // file spec

  f = NextParameter(CMD_REQ);                           // required parameter
  if (f == NULL)                                        // no name argument
    return XW_INVAL;                                    // problem

  free(szHexFileSpec);                                  // release previous
  if (strrchr(f, '.') == NULL) {                        // no extension
    szHexFileSpec = malloc(strlen(f) + 5);              // alloc space
    strcpy(szHexFileSpec, f);                           // filespec
    strcat(szHexFileSpec, ".hex");                      // add default ext.
    }
  else
    szHexFileSpec = strdup(f);                          // store

  return XW_NOERROR;                                    // OK
  }


// ------------------------------------------------------
// Verify availability of a next parameter
// Return NULL if no more parameters
// Issue error message when parameter required!
// ------------------------------------------------------
static char *NextParameter(BOOLEAN bOption) {


  char *p;                                              // ptr to argument

  p = szParmArray[ulCurrentParm + 1];                   // next argument
  if (p == NULL) {                                      // no more parms
    if (bOption == CMD_REQ  && !bQuiet)
      printf("Required argument for command %s missing\n",
                       szParmArray[ulCurrentParm]);
    return NULL;                                        // no more arguments
    }
  else {                                                // argument present
    ulCurrentParm += 1;                                 // (argc++)
    return  szParmArray[ulCurrentParm];                 // next argument
    }
  }


// -----------------------------------------------------
// Find command word and determine its code (number)
// Returns the keyword symbolic number, 0 if unrecognised
// -----------------------------------------------------
static  ULONG  ParseCommandWord(char  *szCmdWord) {

  ULONG   i;                                    // counter(s)

  i = 0;                                        // first table entry
  while (cmdtable[i].nbr != K_ZZZ   &&          // search keyword in table
         strnicmp(szCmdWord, cmdtable[i].word,
               max(strlen(szCmdWord), cmdtable[i].ll)) != 0)
    i++;

  return cmdtable[i].nbr;                       // return command number
  }


// --------------------------------------------------------------------
// Generate error message and returncode for invalid command argument
// --------------------------------------------------------------------
static  ULONG  WrongArgument(ULONG  c,                  // command code
                             char  *a,                  // argument or NULL
                             char  *s) {                // extra string or NULL
  int i;

  if (!bQuiet)
    for (i=0; cmdtable[i].nbr < K_ZZZ; i++) {             // search command word
      if (c == cmdtable[i].nbr)
        break;
      }
    printf("'%s' is an invalid argument for '%s'%s\n",
                  (a != NULL) ? a : szNulString,
                  (c <= K_ZZZ) ? cmdtable[i].word : szNulString,
                  (s != NULL) ? s : szNulString);
  return XW_INVAL;
  }
