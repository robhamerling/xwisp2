
// ===================================================================
//
// XWisp2.c  - Wisp648 Programmer support.
//
//             In-Circuit Serial Programming with
//             - WISP648 PIC programmer
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023, R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// All distributed executables eComStation(OS/2), Linux and Windows compiled
// with Open Watcom C/C++ compiler for eComStation.
//
// Alternative compilations tested:
// - with GNU C Compiler (GCC) and EMX package for eComStation executables
// - with GNU C Compiler (GCC) under Linux for Linux executables
// For each of these: see the corresponding make files.
//
// Successful reports received from others for:
// - GNU C Compiler (GCC) under MacOS
// - GNU C Compiler (GCC) under FreeBSD
// - GNU C Compiler (GCC) for 64-bits Linux
// - GNU C Compiler (GCC) for OS X 10.6
//
// Was also compilable, but not tested anymore, with:
// - IBM C/C++ 2.0 for OS/2 Warp + OS/2 Warp toolkit 4.5 under OS/2
// - IBM VisualAge C/C++ 3.0 for OS/2 Warp + OS/2 Warp toolkit 4.5 under OS/2
// - LCC compiler (3.8) under WinXP
// - Borland BCC 5.5 compiler WinXP
//
// ===================================================================
//
//   xwisp2 is composed of the following modules:
//
//   xwisp2.c     - The main module
//   xwisp2bus.c  - WBus command formatting funtions
//   xwisp2cfg.c  - PIC properties interpretor and service functions
//   xwisp2cmd.c  - Commandline argument handling
//   xwisp2com.c  - Communications with the PIC programmer
//   xwisp2dat.c  - Global variables
//   xwisp2hex.c  - Hex file handling functions (read and write)
//   xwisp2mis.c  - Miscellaneous functions
//   xwisp2os.c   - Elementary Operating System specific functions
//   xwisp2tgt.c  - Target handling functions (the actual PIC programming)
//
// ===================================================================

// #define __DEBUG__

#include "xwisp2.h"                             // all other includes


// =========================================
//
//  M A I N L I N E   of   X W I S P 2
//
//  - welcome user
//  - process file with PIC properties
//  - adapt defaults to environment
//  - handle commandline arguments
//  - shutdown
//
// =========================================
extern int  main(int argc, char **argv) {

  int    rc = 0;                                        // returncode
  ULONG  ulProgramStartTime = OS_Time();                // program start
  char   ucBuffer[8];                                   // user I/O

  szArgv = argv;

  signal(SIGINT,   Abort);                              // controlled abort
  signal(SIGTERM,  Abort);

  EnvironmentHandling();                                // process 'XWISP2'

  /* ------ the real work ------------- */

  rc = CommandHandling(argc, argv);

  /* ---------------------------------- */

  ProgrammerDeactivate();                               // stop comm.

  LogStop();                                            // (when active)

  ReleasePICSpecs();                                    // free allocated mem

  signal(SIGINT,   Abort);                              // controlled abort
  signal(SIGTERM,  Abort);
  ReportResult(szProgName, rc, ulProgramStartTime);     // result + exec time
  if (rc != 0  &&  !bQuiet) {                           // not successful
    printf(szPressEnter);
    fflush(stdout);
    fgets(ucBuffer, sizeof(ucBuffer), stdin);           // wait for 'Enter'
    }

#ifdef __DEBUG__
  printf("Leaving XWisp648 in state %u\n", ulWispState);
#endif

  signal(SIGINT, SIG_DFL);                              // reset to default
  signal(SIGTERM, SIG_DFL);

  if (bQuiet == FALSE)
    printf(szTerminate);                                // signal program end
                                                        // to caller (parent
                                                        // program)

  return rc;                                            // result of last
                                                        // command operation
  }
