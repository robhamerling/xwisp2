
        .8086
CSEG    segment para public 'code'
        assume  cs:CSEG,ds:CSEG
entry:  push    cs
        pop     ds                      ; make sure DS = CS
        mov     dx, offset ds:msg       ; pointer to message
        mov     ah, 09h                 ; write to screen function
        int     21h                     ; write it
        mov     ax, 4c01h               ; terminate with errorlvl 1
        int     21h

msg     db      'XWisp2 and XWlist'
        db      ' are eComStation commandline applications'
        db      ' - by Rob Hamerling,'
        db      0dh, 0ah
        db      'which do not run under DOS'
        db      ' or in a Virtual DOS Machine (VDM) of eComStation.'
        db      0dh, 0ah
        db      'For Windows 98 and later'
        db      ' use XWisp2w and XWlistw from a command prompt.'
        db      0dh, 0ah
        db      '$'                     ; string termination

CSEG    ends
        end     entry

