
// ===================================================================
//
// XWisp2Tgt.C - Wisp648 Programmer support
//
//             -  Higher level PIC programming
//             -  Target oriented functions
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// Note: Since XWisp2 1.0.7 all data offsets and addresses expressed in bytes!
//
// Remarks: When OSCCAL has been erased or erased/overwritten accidentally,
//          it is possible to restore it (when you know the original value)
//          by _not_ make it a preserved word in the specifications for
//          the PICmicro in xwisp2_14.cfg and add the contents of the OSCCAL
//          word to the hex file.
//
// ===================================================================

// #define __DEBUG__                            // generate debugging code

#include "xwisp2.h"                             // all includes

// local common defines and variables

#define  PROGRESS_COLUMN  30                    // alignment msgs

static char  szPct[]   = "\b\b\b\b%3d%%";       // formatting string
static char  szPctNL[] = "\b\b\b\b%3d%%\n";     // formatting string


// prototypes of local functions

static  ULONG   AdjustAddress(ULONG);
static  BOOLEAN BlankCheck(char *, ULONG, USHORT);
static  void    DetectTarget(void);
static  void    PatchFuses(USHORT, USHORT);
static  void    PatchPreservedWord(PUSHORT, ULONG);
static  ULONG   ReadTargetBurst(char *);
static  ULONG   ReadTargetRegion(char *, char *, ULONG, ULONG);
static  USHORT  ReadTargetWord(void);
static  ULONG   WriteTargetRegion(char *, char *, ULONG, ULONG, ULONG);
static  ULONG   WriteTargetRegionPart(char *, ULONG, ULONG);
static  ULONG   VerifyTargetRegion(char *, char *, ULONG, ULONG, USHORT);


// --------------------------------------
// Note: Functions ordered alphabetically
// --------------------------------------

// ------------------------------------------------------------------------
//
// Adjust Programming Address
//
// Input  - new programming address
//
// Output - nothing
//
// Remarks: With jumping from one to another region of memory the
//          required (additional) operations are inserted, then
//          the additional address adjustment operation if required.
//
// ------------------------------------------------------------------------
static  ULONG  AdjustAddress(ULONG ulDestAddr) {        // destination address

  ULONG  rc;                                            // return code
  ULONG  ulAlg;                                         // target algorithm
  USHORT usPgmCode;                                     // target algorithm

  rc = XW_NOERROR;                                      // to enter loop
  ulAlg = pTUsed->Algorithm;                            // target algorithm

#if defined(__DEBUG__)
  printf("AdjustAddress() to %08lX (last address %08lX, WispLevel %lu\n",
          ulDestAddr, ulLastAddress, ulWispLevel);
#endif


  if (ulDestAddr >= pTUsed->ProgStart &&                // program..
      ulDestAddr < pTUsed->IDStart +                    // .. memory
                        ((ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A) ? 8 : 0) ) {
                                                        // 18Fxxx: incl ID

    if (ulLastAddress == MINUS1    ||                   // unsure where now
        ulDestAddr < ulLastAddress ||                   // jump back
        ulLastAddress >= pTUsed->IDStart +              // outside range
                       ((ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A) ? 8 : 0) ) {
                                                        // 18Fxxx: past ID
      if (bBurstReading) {                              // burst reading
        if (ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A)
          rc = WbusProgram(0x006c);                     // burst read 18F
        else
          rc = WbusProgram(0x005c);                     // burst read others
        }
      else {
        switch(ulAlg) {
          case ALG_PIC12:  usPgmCode = 0x00ac; break;       // WbusProgram mask
          case ALG_PIC16:  usPgmCode = 0x000c; break;
          case ALG_PIC16A: usPgmCode = 0x001c; break;
          case ALG_PIC16B: usPgmCode = 0x002c; break;
          case ALG_PIC16C: usPgmCode = 0x004c; break;
          case ALG_PIC16D: usPgmCode = 0x007c; break;
          case ALG_PIC16E: usPgmCode = 0x008c; break;
          case ALG_PIC16F: usPgmCode = 0x00bc; break;
          case ALG_PIC16G: usPgmCode = 0x00cc; break;
          case ALG_PIC16H: usPgmCode = 0x00dc; break;
          case ALG_PIC16I: usPgmCode = 0x00cc; break;
          case ALG_PIC18:  usPgmCode = 0x003c; break;
          case ALG_PIC18A: usPgmCode = 0x009c; break;
          }
        rc = WbusProgram(usPgmCode);                    // to prog mem
        }
      if (rc != XW_NOERROR)                             // problem
        return rc;                                      // report error
      }
    }

  else if (ulDestAddr >= pTUsed->DataStart) {           // data memory
    if (ulLastAddress == MINUS1    ||                   // unsure where now
        ulDestAddr < ulLastAddress ||                   // jump back
        ulLastAddress < pTUsed->DataStart) {            // outside range
      switch(ulAlg) {
        case ALG_PIC12:  usPgmCode = 0x00ad; break;       // WbusProgram mask
        case ALG_PIC16:  usPgmCode = 0x000d; break;
        case ALG_PIC16A: usPgmCode = 0x001d; break;
        case ALG_PIC16B: usPgmCode = 0x002d; break;
        case ALG_PIC16C: usPgmCode = 0x004d; break;
        case ALG_PIC16D: usPgmCode = 0x007d; break;
        case ALG_PIC16E: usPgmCode = 0x008d; break;
        case ALG_PIC16F: usPgmCode = 0x00bd; break;
        case ALG_PIC16G: usPgmCode = 0x00cd; break;
        case ALG_PIC16H: usPgmCode = 0x00dd; break;
        case ALG_PIC16I: usPgmCode = 0x00cd; break;
        case ALG_PIC18:  usPgmCode = 0x003d; break;
        case ALG_PIC18A: usPgmCode = 0x009d; break;
        }
      rc = WbusProgram(usPgmCode);                      // to data mem
      if (rc != XW_NOERROR)                             // problem
        return rc;                                      // report error
      }
    }

  else if (ulDestAddr >= pTUsed->IDStart +              // config ..
                        ((ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A) ? 8 : 0)  &&
                                                        // 18Fxxx: past ID
           ulDestAddr < pTUsed->DataStart) {            // .. before data
    if (ulLastAddress == MINUS1    ||                   // unsure where
        ulDestAddr < ulLastAddress ||                   // jump back
        ulLastAddress < pTUsed->IDStart +               // outside range
                        ((ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A) ? 9 : 0) ||
        ulLastAddress >= pTUsed->DataStart) {           // .. range
      switch(ulAlg) {
        case ALG_PIC12:  usPgmCode = 0x00af; break;         // WbusProgram mask
        case ALG_PIC16:  usPgmCode = 0x000f; break;
        case ALG_PIC16A: usPgmCode = 0x001f; break;
        case ALG_PIC16B: usPgmCode = 0x001f; break;         // exception!
        case ALG_PIC16C: usPgmCode = 0x004f; break;
        case ALG_PIC16D: usPgmCode = 0x007f; break;
        case ALG_PIC16E: usPgmCode = 0x008f; break;
        case ALG_PIC16F: usPgmCode = 0x00bf; break;
        case ALG_PIC16G: usPgmCode = 0x00cf; break;
        case ALG_PIC16H: usPgmCode = 0x00df; break;
        case ALG_PIC16I: usPgmCode = 0x00cf; break;
        case ALG_PIC18:  usPgmCode = 0x003f; break;
        case ALG_PIC18A: usPgmCode = 0x009f; break;
        }
      rc = WbusProgram(usPgmCode);                      // to ID/conf
      if (rc != XW_NOERROR)                             // problem
        return rc;                                      // report error
      }
    }

  else {
    if (!bQuiet)
      printf("Internal address calculation error,"
                    "Dest %06lX, Last %06lX, LBA %06lX\n",
                    ulDestAddr, ulLastAddress, ulLBA);
    return XW_INVAL;
    }

  // When arrived here it is certain we are in the proper memory region
  // and current address is equal to or lower than destination address.

  if ((ulAlg == ALG_PIC18  ||  ulAlg == ALG_PIC18A)  &&   // 16-bit core
      ulLastAddress < ulDestAddr)                       // not at destination
    return WbusJump(ulDestAddr);                        // jump to dest.

  if (ulDestAddr - ulLastAddress > 8)                   // jump desired!
    return WbusJump(ulDestAddr);                        // BYTE offset!

  while (ulLastAddress < ulDestAddr  &&                 // not at destination
         rc == XW_NOERROR)                              // no errors
    rc = WbusIncrement();                               // adjust address
  return rc;                                            // result last incr.
  }


// ----------------------------------------------------------
//
//  BlankCheck - determine if part of memory is all blank
//
//  Input   - ptr to start of array
//          - number of bytes in array
//          - bit-pattern for 'blank' (14 or 16 bits mask)
//
//  Output  - nothing
//
//  returns - TRUE if all blank
//          - FALSE if array contains any non-blank word
//
// ----------------------------------------------------------
static  BOOLEAN  BlankCheck(char  *ucBuffer,            // ptr to buffer
                            ULONG  ulBufSize,           // # bytes
                            USHORT usBlankMask) {       // mask

  ULONG   i;                                            // counter(s)
  USHORT  usMemWord;                                    // check per word

  if (ulBufSize == 1) {                                 // single byte
    usMemWord = ucBuffer[0];                            // byte -> word
    if (usMemWord != (usBlankMask & 0x00FF))            // low order byte
      return FALSE;                                     // not blank
    }
  else {                                                // multiple words
    for (i=0; i<ulBufSize; i++,i++) {                   // per 2 bytes
      usMemWord = BYTES2WORD(ucBuffer + i);             // word
      if ((usMemWord & usBlankMask) != usBlankMask)     // check
        return FALSE;                                   // not blank
      }
    }
  return TRUE;                                          // all blank
  }


// --------------------------------------------------
//
//  ControlFuses - Special treatment of Fuses
//
//  Functionality:
//    - effects of PROTECT command
//    - effects of FUSES command
//    - effects of preserved bit patterns (FuseFixedZero, FuseFixedOne)
//
// --------------------------------------------------
extern  void  ControlFuses(void) {

  ULONG   ulFam;                                        // algorithm
  USHORT  usMemWord;                                    // memory word
  ULONG   i;                                            // counter(s)

  ulFam = pTUsed->FamilyIndex;                          // target family

  if (ulFam == CORE_12) {                               // 12-bit core
    /* not supported yet */
    }

  else if (ulFam == CORE_14) {                          // 14-bit core
    if (usFusesOverride != 0x0000)                      // user specified
      PatchFuses(usFusesOverride, pTUsed->BlankMask);   // replace completely
    else if (fProtection != PROT_HEX) {                 // protect command
      if (fProtection == PROT_ALWAYS)                   // set protection
        PatchFuses(0x0000, pTUsed->ProtectMask);
      else if (fProtection == PROT_NEVER)               // reset protection
        PatchFuses(pTUsed->ProtectMask, 0x0000);
      }
    for (i=0; i<pTUsed->FusesSize; i++) {               // all bytes
      FusesRegionMidrange[i] &= pTUsed->FuseFixedZero[i]; // reset fixed 0-bits
      FusesRegionMidrange[i] |= pTUsed->FuseFixedOne[i];  // set fixed 1-bits
      }
    for (i=0; i<pTUsed->FusesSize; i++, i++)            // all words
      PatchPreservedWord((USHORT *)(FusesRegionMidrange+i), pTUsed->FusesStart);
    }

  else if (ulFam == CORE_16) {                          // 16-bit core (18Fxxx)
    usMemWord = BYTES2WORD(FusesRegion + 8);            // only 1 word!!!
    if (fProtection == PROT_ALWAYS)
      usMemWord &= ~pTUsed->ProtectMask;                // protect bits off
    else if (fProtection == PROT_NEVER)
      usMemWord |= pTUsed->ProtectMask;                 // protect bits on
    WORD2BYTES(usMemWord, FusesRegion + 8);
    for (i=0; i<ulFusesSize; i++) {                     // all fuse bytes
      FusesRegion[i] &= pTUsed->FuseFixedZero[i];       // reset fixed 0-bits
      FusesRegion[i] |= pTUsed->FuseFixedOne[i];        // set fixed 1-bits
      }
    }

  else {                                                // other
    /* not supported yet */
    }

  return;
  }


// ----------------------------------------------------------
//
//  DetectTarget - determine target PIC by reading deviceID
//
//  This function tries to read the DeviceID of the target
//  Fortunately reading this word functions with the same
//  basic algorithm: PIC16 for all midrange PICs and
//  PIC18 for the whole 18F range. Both are tried in this
//  sequence. When a non all zero or 'blank' word is returned
//  it is assumed this is a real DeviceID of a member of the
//  corresponding family of PICs.
//
//  Global variable pTUsed will be set (might be NULL!)
//  Returns - NULL when target could not be detected
//          - otherwise pointer to TargetInfo
//
// ----------------------------------------------------------
static  void  DetectTarget(void) {

  static USHORT usFamilyIndex[] = {CORE_16, CORE_14H, CORE_14};

  ULONG   i;                                            // counter(s)
  USHORT  usPICid = 0;                                  // id from config mem
  PTARGETINFO  pT;                                      // ptr to properties
  char    buffer[60];                                   // for log message

  for (i=0; i < sizeof(usFamilyIndex)/sizeof(usFamilyIndex[0]); i++) {
    pT = SearchPICFamily(usFamilyIndex[i]);             // search tryout target
    if (pT == NULL)                                     // none in this family
      continue;                                         // next family
    pTUsed = pT;                                        // try this target
    if (bVerbose && !bQuiet)
      printf("Trying %d bits core family (provisional target %s)\n",
              pTUsed->Core, pTUsed->Name);
    ulLastAddress = MINUS1;                             // reset
    AdjustAddress(pTUsed->DevIDStart);                  // to deviceID
    usPICid = ReadTargetWord();                         // deviceID
    if (bVerbose  &&  !bQuiet)
      printf("Received target deviceID: %04hX\n", usPICid);   // show device ID
    if ( usPICid != 0    &&                             // not all zero
        (usPICid | pTUsed->RevMask) != pTUsed->BlankMask) {  // not blank
      pT = SearchPICDevID(usPICid, pT->FamilyIndex);    // actual PIC
      if (pT != NULL) {                                 // chip in table
        pTUsed = pT;                                    // supported PIC!
        if (!bQuiet)
          printf("Target: %s revision %02hu (ID=%04hX)\n",
                  pTUsed->Name,                         // PIC type
                  usPICid & pTUsed->RevMask,            // revision
                  usPICid);                             // complete deviceID
        if (bVerbose  &&  !bQuiet)
          printf("Datasheet: %s, Programming Specifications: %s\n",
                  pTUsed->DataSheet, pTUsed->PgmSpec);  //
        sprintf(buffer, "(%s rev. %hu, Algorithm %s)",
                         pTUsed->Name,                  // name
                         usPICid & pTUsed->RevMask,     // revision code
                         szAlgorithmName[pTUsed->Algorithm]);   // pgmg. alg.
        LogMsg(buffer);
        return;                                         // search completed
        }
      else if (!bQuiet)                                 // not supported
        printf("Detected target with ID %04X is not supported by %s!\n",
                       usPICid, szProgName);
      }
    else if (bVerbose  &&  !bQuiet)
      printf("Not a valid Device-ID received\n");       // blank or zero
    }

  pTUsed = NULL;                                        // reset!
  return;

  }


// ------------------------------------------------
//
// Erase target
//
// Input  - nothing
//
// Output - nothing
//
// Remarks - Preserved bits are read before and restored
//           immediately after erase
//
// ------------------------------------------------
extern  int    EraseTarget(void) {

  int        rc;                                        // result
  USHORT     usTemp;                                    // work field
  USHORT     usPgmCode;                                 // WbusProgram code
  PPRESERVE  pPres;                                     // preserve ptr
  ULONG      ulAlg;                                     // pgmg algorithm
  char       ucData[2];                                 // output buffer

  if ((rc = ProgrammerActivate()) != XW_NOERROR)        // detect target type
    return rc;                                          // problem

  IdentifyTarget();                                     // detect target
  if (pTUsed == NULL)                                   // undetermined
    return XW_NOENT;                                    // problem

  ulAlg = pTUsed->Algorithm;                            // actual algorithm

  switch(ulAlg) {
    case ALG_PIC12:  usPgmCode = 0x00ae; break;             // WbusProgram mask
    case ALG_PIC16:  usPgmCode = 0x000e; break;
    case ALG_PIC16A: usPgmCode = 0x001e; break;
    case ALG_PIC16B: usPgmCode = 0x002e; break;
    case ALG_PIC16C: usPgmCode = 0x004e; break;
    case ALG_PIC16D: usPgmCode = 0x007e; break;
    case ALG_PIC16E: usPgmCode = 0x008e; break;
    case ALG_PIC16F: usPgmCode = 0x00be; break;
    case ALG_PIC16G: usPgmCode = 0x00ce; break;
    case ALG_PIC16H: usPgmCode = 0x00de; break;
    case ALG_PIC16I: usPgmCode = 0x001e; break;
    case ALG_PIC18:  usPgmCode = 0x003e; break;
    case ALG_PIC18A: usPgmCode = 0x009e; break;
    }
  rc = WbusProgram(usPgmCode);                          // erase

  if (rc == XW_NOERROR  && !bQuiet)
    printf("Target erased\n");

  pPres = pTUsed->pPreserve;                            // first preserve
  while (pPres != NULL) {                               // whole chain
    AdjustAddress(pPres->Address);                      // prepare for write
    usTemp = ReadTargetWord();                          // re-read word
    usTemp &= ~pPres->BitMask;                          // non masked bits
    usTemp |= (pPres->Contents & pPres->BitMask);       // masked bits
    ucData[0] = usTemp / 256;                           // prepare ..
    ucData[1] = usTemp % 256;                           // .. for write
    rc = WbusWrite(ucData, 2);                          // restore word
    if (rc != 0) {                                      // failed to rewrite
      if (!bQuiet)
        printf("Failed to restore preserved location %04hX,"
               " previous contents %04hX\n",
               pPres->Address, usTemp);
      break;                                            // terminate
      }
    else if (bVerbose  &&  !bQuiet)
      printf("Restored contents of address %04hX (mask %04hX) : %04hX\n",
             pPres->Address, pPres->BitMask, usTemp);
    pPres = pPres->next;                                // next in chain
    }

  return rc;
  }


// --------------------------------------------------
//
//  IdentifyTarget - try to read target deviceID
//                 - if (target detected  (implies supported by Xwisp2))
//                      if (target also specified)
//                         check if it matches with detected target
//                 - else (target not detected)
//                      if (target specified)
//                         use specified target
//                      else (target not specified)
//                         error: return NULL
//
//  Returns - Target detected, pTUsed set
//          - NULL: Target unknown (pTUsed set to NULL)
//
//  Remarks: When Bits have to be preserved, these are read and stored
//
// --------------------------------------------------
extern  void  IdentifyTarget(void) {

  PPRESERVE  pPres;                                     // preserve ptr

  if (pTUsed != NULL)                                   // already known
    return;                                             // use current target

  DetectTarget();                                       // read target devID

  if (pTUsed == NULL) {                                 // not detected
    if (pTSpec != NULL) {                               // user specified
      pTUsed = pTSpec;                                  // use spec'd
      if (!bQuiet)
        printf("Target not auto-detected, using specified target: %s\n",
                pTUsed->Name);
      }
    else {
      if (!bQuiet)
        printf("Target not auto-detected, please specify on commandline!\n");
      return;                                           // problem
      }
    }

  if (pTUsed != NULL    &&                              // detected target
      pTSpec != NULL    &&                              // specified target
      pTSpec != pTUsed) {                               // conflict!
    if (!bQuiet)
      printf("WNG: specified target %s, detected target %s\n",
                     pTSpec->Name, pTUsed->Name);
    if (bVerbose  &&  !bQuiet)
      printf("Using detected target %s\n", pTUsed->Name);
    return;                                             // problem
    }

  if (bVerbose  &&  !bQuiet)
    printf("Using programming algorithm %u (%s)\n",
            pTUsed->Algorithm, szAlgorithmName[pTUsed->Algorithm]);

  pPres = pTUsed->pPreserve;                            // first preserve
  while (pPres != NULL) {
    AdjustAddress(pPres->Address);                      // prepare for read
    pPres->Contents = ReadTargetWord();                 // get word
    if (bVerbose  &&  !bQuiet)
      printf("Preserving contents of address %04hX : %04hX\n",
                      pPres->Address, pPres->Contents);
    pPres = pPres->next;                                // next in chain
    }

  return;                                               // Target Used
  }


// --------------------------------------------------
//
//  PatchFuses - patch fuses memory in buffer
//
//             Fuses modified as follows:
//             1. AND-ed with 1's complement of OFF-mask
//             2. OR-ed with ON-mask
//
// --------------------------------------------------
static  void  PatchFuses(USHORT usONmask,
                         USHORT usOFFmask) {

  USHORT  usFuses;                                      // fuses word
  USHORT  ulFam;                                        // PIC family

  ulFam = pTUsed->FamilyIndex;                          // target family

  usFuses =  BYTES2WORD((ulFam == CORE_16) ? FusesRegion
                                           : FusesRegionMidrange);
  if (bVerbose  &&  !bQuiet)
    printf("Patching fuses, old %04hX", usFuses);
  usFuses &= ~usOFFmask;
  usFuses |= usONmask;
  if (bVerbose  &&  !bQuiet)
    printf(" -> (off: %04hX  on: %04hX) -> new %04hX\n",
                    usOFFmask, usONmask, usFuses);
  WORD2BYTES(usFuses, (ulFam == CORE_16) ? FusesRegion
                                         : FusesRegionMidrange);
  }


// --------------------------------------------------
//
//  PatchPreservedWord - patch preserved words in program memory buffer
//
//  Words modified as follows:
//   -  AND-ed with 1's complement of preserve bitmask
//   -  OR-ed with (original ANDed with preserve bitmask)
//
// --------------------------------------------------
static  void   PatchPreservedWord(PUSHORT pusMemWord,
                                  ULONG   ulMemAddress) {

  PPRESERVE  pPres;                                     // pointer
  USHORT     usTemp;

  pPres = pTUsed->pPreserve;                            // first preserve
  while (pPres != NULL) {                               // whole chain
    if (pPres->Address == ulMemAddress) {               // address match
      usTemp = BYTES2WORD((char *)pusMemWord);          // get word contents
      usTemp &= ~pPres->BitMask;                        // selected bits new
      usTemp |= (pPres->Contents & pPres->BitMask);     // selected bits org
#if defined(__DEBUG__)
      printf("Original contents address %04hX : %04hX,"
                      " mask %04hX, returned : %04hX\n",
                      pPres->Address, pPres->Contents,
                      pPres->BitMask, usTemp);
#endif
      WORD2BYTES(usTemp, (char *)pusMemWord);           // store word back
      return;                                           // done
      }
    pPres = pPres->next;                                // next in chain
    }
  return;
  }


// -----------------------------------------------------------------
//
//  ReadTarget - read memory of PIC
//
//  input:   none
//  return:  returncode
//
// ----------------------------------------------------
extern  int    ReadTarget(void) {

  int     rc;                                           // returncode
  ULONG   ulFam;                                        // PIC family
  ULONG   ulStartRead;                                  // read timing

  ulStartRead = OS_Time();                              // start of transfer

  rc = ProgrammerActivate();                            // set 'active'
  if (rc != XW_NOERROR)                                 // problem
    return rc;

  IdentifyTarget();                                     // detect target
  if (pTUsed == NULL)                                   // undetermined
    return XW_NOTEXISTS;                                // unknown

  ulFam = pTUsed->FamilyIndex;                          // target family

  if (!bQuiet)
    printf("Reading target\n");

  BulkErase(ulProgSize,  ProgRegion,  pTUsed->BlankMask);  // ) erase ..
  BulkErase(ulDataSize,  DataRegion,                       // )
            (ulFam != CORE_16) ? 0x00FF : 0xFFFF);         // ) .. memory ..
  BulkErase(ulIDSize,    IDRegion,    pTUsed->BlankMask);  // ) .. buffer ..
  BulkErase(ulFusesSize, FusesRegion, pTUsed->BlankMask);  // ) .. regions

  rc = XW_NOERROR;                                      // default

  if (rc == XW_NOERROR  &&                              // no problem so far
      fRegions & REGION_PROG) {                         // region requested
    rc = ReadTargetRegion("program",                    // read program mem
                        ProgRegion,
                        pTUsed->ProgStart,
                        pTUsed->ProgSize);
    }

  if (rc == XW_NOERROR  &&
      fRegions & REGION_DATA  &&                        // region requested
      pTUsed->DataSize > 0) {                           // target has it
    rc = ReadTargetRegion("data",                       // read data mem
                        (ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
                        pTUsed->DataStart,
                        pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2));
                                                        // 1.8.2-3
    }

  if (rc == XW_NOERROR  &&                              // no problem so far
      fRegions & REGION_ID) {                           // region requested
    ulLastAddress = MINUS1;                             // force WbusProgram
                                                        // burst->byte read
    rc = ReadTargetRegion("ID",
                        (ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
                        pTUsed->IDStart,
                        pTUsed->IDSize);
    }

  if (rc == XW_NOERROR  &&
      fRegions & REGION_FUSES) {                        // region requested
    rc = ReadTargetRegion("fuses",                      // read fuses
                        (ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
                        pTUsed->FusesStart,
                        pTUsed->FusesSize);
    }

  return ReportResult("Read operation", rc, ulStartRead);
  }


// -----------------------------------------------------------------
//
//  Read Target Burst - read 8 bytes target memory
//
//  input:   none
//
//  return:  USHORT
//
//  Remarks: returns 8 consecutive bytes of PIC memory
//
// -----------------------------------------------------------------
static  ULONG  ReadTargetBurst(char *pBuffer) {

  char   szReply[20];                                   // reply buffer
  int    i;                                             // counter
  ULONG  rc;                                            // result code
  ULONG  ulFam;                                         // core index

  ulFam = pTUsed->FamilyIndex;                          // core index

  rc = WbusRead(szReply, 16, sizeof(szReply));          // 8 bytes to read
  if (rc == XW_NOERROR) {                               // read OK
    if (ulFam == CORE_16) {                             // 18Fxxx range
      for (i=0; i<8; i++)                               // 8 pairs hex digits
        Hex2Byte(szReply + 2 * i, &pBuffer[i]);
      }
    else {
      for (i=0; i<8; i++,i++) {                         // 4 words
        Hex2Byte(szReply + 2 * (i + 1), &pBuffer[i + 0]);
        Hex2Byte(szReply + 2 * (i + 0), &pBuffer[i + 1]);
        }
      }
    }
  return rc;                                            // result of read
  }


// -----------------------------------------------------------------
//
//  Read Target Memory Part - read target memory region
//
//  input:   - pointer to memory image (byte array)
//           - maximum number of doublebytes to read
//
//  return:  returncode
//
// -----------------------------------------------------------------
static  ULONG  ReadTargetRegion(char  *szRegionName,
                                char  *Segment,
                                ULONG  ulRegionAddress,
                                ULONG  ulRegionSize) {

  int    i;                                             // counter
  USHORT usMemWord;                                     // for word reads
  ULONG  rc;                                            // result code
  ULONG  ll;                                            // string length
  ULONG  ulMemoryByteCount;                             // bytes to process

  if (ulRegionAddress == pTUsed->ProgStart)             // code
    ulMemoryByteCount = pTUsed->ProgSize;
  else if (ulRegionAddress == pTUsed->DataStart)        // data
    ulMemoryByteCount = pTUsed->DataSize;
  else
    ulMemoryByteCount = 14;                             // ID or fuses

  if (!bQuiet) {
    ll = printf("Reading %s memory", szRegionName);
    for ( ; ll < PROGRESS_COLUMN; ll++)                   // column alignment
      printf(".");                                        // filler
    printf("    ");                                       // space
    fflush(stdout);
    }
  bBurstReading = FALSE;                                // no burst reading
  rc = XW_NOERROR;                                      // default result!
  if (ulRegionAddress == pTUsed->ProgStart) {           // program memory
    bBurstReading = TRUE;                               // burst reading
    for (i = 0; i < ulRegionSize; i += 8) {             // 8 bytes per read
      AdjustAddress(ulRegionAddress + i);
      rc = ReadTargetBurst(Segment + i);
      if (rc) {
        if (!bQuiet)
          printf("Read failed, rc %lu\n", rc);
        break;
        }
      if (!bQuiet) {
        if (bVerbose)
          printf("\n%06lX : %02hX%02hX %02hX%02hX %02hX%02hX %02hX%02hX",
                  ulRegionAddress + i,
                 (USHORT)Segment[i+0], (USHORT)Segment[i+1],
                 (USHORT)Segment[i+2], (USHORT)Segment[i+3],
                 (USHORT)Segment[i+4], (USHORT)Segment[i+5],
                 (USHORT)Segment[i+6], (USHORT)Segment[i+7]);
        else if (i % 256 == 0)
          printf(szPct, 100 * i / ulMemoryByteCount);
        fflush(stdout);
        }
      }
    bBurstReading = FALSE;                              // disable
    }
  else if (pTUsed->Algorithm == ALG_PIC16F  &&          // (16F716)
           ulRegionAddress == pTUsed->IDStart) {        // ID memory
    bBurstReading = TRUE;                               // burst reading
    AdjustAddress(ulRegionAddress);
    rc = ReadTargetBurst(Segment);
    if (!bQuiet) {
      if (rc)
        printf("Read failed, rc %lu\n", rc);
      else if (bVerbose) {
        printf("\n%06lX : %02hX%02hX %02hX%02hX %02hX%02hX %02hX%02hX\n",
                ulRegionAddress,
               (USHORT)Segment[0], (USHORT)Segment[1],
               (USHORT)Segment[2], (USHORT)Segment[3],
               (USHORT)Segment[4], (USHORT)Segment[5],
               (USHORT)Segment[6], (USHORT)Segment[7]);
        fflush(stdout);
        }
      }
    i = ulRegionSize;                                   // progress 100%
    bBurstReading = FALSE;                              // disable
    }
  else {                                                // word-by-word
    for (i = 0; i < ulRegionSize; i++, i++) {
      AdjustAddress(ulRegionAddress + i);
      usMemWord = ReadTargetWord();
      WORD2BYTES(usMemWord, Segment + i);
      if (!bQuiet) {
        if (bVerbose)
          printf("\n%06lX : %02hX%02hX",
                ulRegionAddress + i,
                (USHORT)Segment[i], (USHORT)Segment[i + 1]);
        else if (i % 256 == 0)
          printf(szPct, 100 * i / ulMemoryByteCount);
        fflush(stdout);
        }
      }
    }

  if (rc == XW_NOERROR  &&  !bQuiet) {
    if (bVerbose)
      printf("\n");
    else
      printf(szPctNL, 100);
    }

  return  rc;
  }


// -----------------------------------------------------------------
//
//  Receive Target Word - read memory word of target at current location
//
//  input:   none
//
//  return:  USHORT
//
//  Remarks: returns 2 consecutive bytes in PIC memory
//           as 'unsigned short int' (Intel notation)
//
// -----------------------------------------------------------------
static  USHORT  ReadTargetWord(void) {

  char   szReply[8];                                    // reply buffer
  ULONG  ulFam;                                         // family
  USHORT usValue;                                       // family

  ulFam = pTUsed->FamilyIndex;                          // core index

  if (ulFam == CORE_16) {                               // 18F family
    if (WbusRead(szReply, 2, sizeof(szReply)) == XW_NOERROR) {     // 1st
      if (WbusRead(szReply+2, 2, sizeof(szReply)-2) == XW_NOERROR) { // 2nd
        HEX2WORDINTEL(szReply, &usValue);               // LSB first
        return usValue;
        }
      }
    }                                                   // midrange
  else {                                                // word
    if (WbusRead(szReply, 4, sizeof(szReply)) == XW_NOERROR){    // 4 chars
      HEX2WORD(szReply, &usValue);                      // MSB first
      return  usValue;
      }
    }
  return (USHORT)MINUS1;                                // dummy word
  }


// -----------------------------------------------------------------
//
//  Run       - put target in run mode
//
//  input:   none
//  return:  returncode
//
// ----------------------------------------------------
extern  int    RunTarget(void) {

  int    rc;                                            // returncode

  if ( ulWispState != WISPACTIVE) {                     // not active
    if ((rc = ProgrammerActivate()) != XW_NOERROR)      // set 'active'
      return rc;
    }

  if (!bQuiet)
    printf("Putting target in run mode\n");

  WbusGo();                                             // send Wbus Go cmd
  if (ulWispState != WISPACTIVE)                        // failure
    return XW_IOERROR;

  return XW_NOERROR;
  }


// --------------------------------------------------
//
//  Verify target
//
//  input: - nothing
//
//  output - none
//
//  returns - 0 is hex file and target are identical
//
// --------------------------------------------------
extern  int    VerifyTarget(void) {

  int     rc;                                           // result code
  ULONG   ulStartVerify;                                // start of verify
  ULONG   ulFam;                                        // algorithm

  if ((rc = ProgrammerActivate()) != XW_NOERROR)        // set 'active'
    return rc;

  IdentifyTarget();                                     // detect target
  if (pTUsed == NULL)                                   // undetermined
    return  XW_NOENT;                                   // problem

  ulFam = pTUsed->FamilyIndex;                          // target family

  ulStartVerify = OS_Time();                            // start of verify
  rc = XW_NOERROR;                                      // default return: OK
  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_PROG) {                         // region included
    rc = VerifyTargetRegion("program",
               ProgRegion,
               pTUsed->ProgStart,
               pTUsed->ProgSize - ((pTUsed->Algorithm == ALG_PIC16C) ? 2 : 0),
               pTUsed->BlankMask);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_DATA  &&                        // region included
      pTUsed->DataSize > 0) {                           // target has it
    rc = VerifyTargetRegion("data",
              (ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
               pTUsed->DataStart,
               pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),
                                                        // 1.8.2-3
               (ulFam == CORE_16) ? 0xFFFF : 0x00FF);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_ID) {                           // region included
    ulLastAddress = MINUS1;                             // force WbusProgram
    rc = VerifyTargetRegion("ID",
              (ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
               pTUsed->IDStart,
               pTUsed->IDSize,
               pTUsed->BlankMask);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_FUSES) {                        // region included
    ControlFuses();                                     // handle fixed bits
    rc = VerifyTargetRegion("fuses",
              (ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
               pTUsed->FusesStart,
               pTUsed->FusesSize,
               pTUsed->BlankMask);
    }

  return ReportResult("Verify operation", rc, ulStartVerify);
  }


// --------------------------------------------------
//
//  Verify target memory region
//
//  Input:  - name of region
//          - pointer to memory region
//          - start address
//
//  Output  - none
//
//  Returns 0 if image and target are identical, otherwise I/O error
//
// --------------------------------------------------
static  ULONG  VerifyTargetRegion(char  *szRegionName,
                                  char  *Segment,
                                  ULONG  ulRegionAddress,
                                  ULONG  ulRegionSize,
                                  USHORT usBlankMask) {

  ULONG   rc;                                           // result code
  ULONG   ll;                                           // string length
  USHORT  usMemWord;                                    // 2 bytes of image
  USHORT  usTargetWord;                                 // 2 bytes of target
  char    ucMemBurst[8];                                // 8 bytes of target
  int     i, j;                                         // counter(s)
  ULONG   ulMemoryByteCount;                            // bytes to process

  if (ulRegionAddress == pTUsed->ProgStart)             // code
    ulMemoryByteCount = pTUsed->ProgSize;               // whole region
  else if (ulRegionAddress == pTUsed->DataStart)        // data
    ulMemoryByteCount = pTUsed->DataSize;               // active part
  else
    ulMemoryByteCount = 14;                             // ID or fuses

  if (!bQuiet) {
    ll = printf("Verifying %s memory", szRegionName);
    for ( ; ll < PROGRESS_COLUMN; ll++)                   // column alignment
      printf(".");                                        // filler
    printf("    ");                                       // space
    fflush(stdout);
    }
#if defined(__DEBUG__)
  printf("Firmware %lu, Algorithm %lu, Segment %06lX, IDRegion %06lX\n",
          ulWispLevel, pTUsed->Algorithm, Segment, IDRegion);
#endif
  if (Segment == ProgRegion  ||                         // program memory
      (pTUsed->Algorithm == ALG_PIC16F &&               // 16F716
        ulRegionAddress == pTUsed->IDStart) )           // ID memory
    bBurstReading = TRUE;                               // do BURST reads
#if defined(__DEBUG__)
  printf("Burstreading %s\n", ((bBurstReading) ? "TRUE" : "FALSE"));
#endif
  rc = XW_NOERROR;                                      // default return: OK
  i = 0;                                                // start of region
  while (i < ulRegionSize  &&  rc == XW_NOERROR) {      // whole region
    if (bBurstReading) {                                // burst reading
      if (bFullVerify == TRUE  ||                       // check all
          !BlankCheck(Segment + i, 8, usBlankMask)) {   // not erased
        AdjustAddress(ulRegionAddress + i);             // position
        rc = ReadTargetBurst((char *)ucMemBurst);       // read 8 bytes
        if (rc) {
          if (bQuiet)
            printf("Read failed, rc %lu\n", rc);
          break;                                        // terminate verify
          }
        if (!bQuiet) {
          if (bVerbose)
            printf("\n%06lX : %02hX%02hX %02hX%02hX %02hX%02hX %02hX%02hX",
                   ulRegionAddress + i,
                   (USHORT)ucMemBurst[1], (USHORT)ucMemBurst[0],
                   (USHORT)ucMemBurst[3], (USHORT)ucMemBurst[2],
                   (USHORT)ucMemBurst[5], (USHORT)ucMemBurst[4],
                   (USHORT)ucMemBurst[7], (USHORT)ucMemBurst[6]);
          else if (i % 256 == 0)                          // progress report
            printf(szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        for (j=0; j<8 && i+j<ulRegionSize; j += 2) {    // 4 byte pairs
          usMemWord = BYTES2WORD(Segment + i + j);
          usMemWord &= usBlankMask;
          usTargetWord = BYTES2WORD(ucMemBurst + j);
          if (usMemWord != usTargetWord) {
            if (!bQuiet)
              printf("failed at %06X,"
                            " expected: '%04hX', found: '%04hX'\n",
                            i + j,
                            usMemWord,
                            usTargetWord);
            rc = XW_IOERROR;
            break;                                      // problem
            }
          }
        if (rc)                                         // problem
          break;                                        // exit outer loop

        }
      i += 8;                                           // done 8 bytes
      }
    else {                                              // word-by-word
      if (bFullVerify == TRUE  ||                       // check all
          !BlankCheck(Segment + i, 2, usBlankMask)) {   // not erased
        usMemWord = BYTES2WORD(Segment + i);
        usMemWord &= usBlankMask;
        AdjustAddress(ulRegionAddress + i);             // position
        usTargetWord = ReadTargetWord();                // read word
        if (!bQuiet) {
          if (bVerbose)                                   // progress report
            printf("\n%06lX : %04hX",
                    ulRegionAddress + i,                  // last word
                    usTargetWord);
          else if (i%256 == 0)                            // progress report
            printf( szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        if (usMemWord != usTargetWord) {                // unequal
          if (!bQuiet)
            printf("failed at %06lX, expected: '%04hX', found: '%04hX'\n",
                    ulRegionAddress + i,
                    usMemWord,
                    usTargetWord);
          rc = XW_IOERROR;
          break;                                        // problem
          }
        }

      i += 2;                                           // done 2 bytes
      }
    }
  bBurstReading = FALSE;                                // regardless region

  if (rc == XW_NOERROR  &&  !bQuiet) {
    if (bVerbose)
       printf("\n");
    else if (rc == XW_NOERROR)                          // all done
      printf(szPctNL, 100);
    }

  return rc;
  }


// -------------------------------------
//
// Transfer Image to target
//
// Input   - pointer to image
//
// Output  - 0 for OK
//         - otherwise error
//
// Remarks - takes into account the 'preserve' memory areas
//         - data, ID and Fuses transferred to 18F only when
//           appropriate extended segment record present in hex file
//
// -------------------------------------
extern  int    WriteTarget(void) {

  int        rc;                                        // returncode
  ULONG      ulStartWrite;                              // write timing
  ULONG      ulFam;                                     // core index
  USHORT     usMemWord;                                 // word manipulations
  PPRESERVE  pPres;                                     // ptr to preserve

  if ((rc = ProgrammerActivate()) != XW_NOERROR)        // set 'active'
    return rc;

  IdentifyTarget();                                     // detect target
  if (pTUsed == NULL)                                   // undetermined
    return XW_NOTREAD;                                  // problem

  if (!bQuiet)
    printf("Transferring program to %s via %s (%s)\n",
           pTUsed->Name, szWbusDevice, szAlgorithmName[pTUsed->Algorithm]);

  ulStartWrite = OS_Time();                             // start of transfer

  ulFam = pTUsed->FamilyIndex;                          // target family

  rc = XW_NOERROR;                                      // start value

  if (rc == XW_NOERROR  &&                              // no problem so far
      fRegions & REGION_PROG) {                         // region included
    pPres = pTUsed->pPreserve;                          // first preserve
    while (pPres != NULL) {                             // all preserved words
      if (pPres->Address >= pTUsed->ProgStart &&        // within progmem
          pPres->Address <= pTUsed->ProgStart + pTUsed->ProgSize - 2) {
        usMemWord = BYTES2WORD(ProgRegion + pPres->Address);  // hex contents
        usMemWord &= ~pPres->BitMask;                   // keep non mask bits
        usMemWord |= (pPres->Contents & pPres->BitMask); // add original bits
        usMemWord &= pTUsed->BlankMask;                 // implemented bits
#if defined(__DEBUG__)
        printf("Original contents address %04hX : %04hX,"
                        " mask %04hX, returned : %04hX\n",
                        pPres->Address, pPres->Contents,
                        pPres->BitMask, usMemWord);
#endif
        WORD2BYTES(usMemWord, ProgRegion + pPres->Address);
        }
      pPres = pPres->next;                              // next in chain
      }

    rc = WriteTargetRegion("program",
                           ProgRegion,
                           pTUsed->ProgStart,
                           pTUsed->ProgSize,
                           pTUsed->Algorithm);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_DATA  &&                        // region included
      pTUsed->DataSize > 0) {                           // target has it
    rc = WriteTargetRegion("data",
             (ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
              pTUsed->DataStart,
              pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),   // 1.8.2-3
              pTUsed->Algorithm);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_ID) {                           // region included
    ulLastAddress = MINUS1;                             // force WbusProgram
    rc = WriteTargetRegion("ID",
             (ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
              pTUsed->IDStart,
              pTUsed->IDSize,
              pTUsed->Algorithm);
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_FUSES) {                        // region included
    ControlFuses();                                     // fuses specials
    ulLastAddress = MINUS1;                             // force WBusProgram
    rc = WriteTargetRegion("fuses",
             (ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
              pTUsed->FusesStart,
              pTUsed->FusesSize,
              pTUsed->Algorithm);
    }

  return ReportResult("Write operation", rc, ulStartWrite);
  }


// -------------------------------------
//
// Transmit Region of Image to target
//
// Input   - pointer to region of image
//         - length of region, counted in words
//         - start address
//         - programming algorithm
//
// Output  - progress display
//
// Returns result code of last write operation
//
// -------------------------------------
static  ULONG  WriteTargetRegion(char  *szRegionName,
                                 char  *Segment,
                                 ULONG  ulRegionAddress,
                                 ULONG  ulRegionSize,
                                 ULONG  ulAlg) {

  int    i, j;                                          // counter(s)
  ULONG  rc;                                            // return code
  ULONG  ll;                                            // string length
  char   ucData[132];                                   // write buffer
  ULONG  ulBurst;                                       // length of data
  ULONG  ulMemoryByteCount;                             // bytes to process

  if (ulRegionAddress == pTUsed->ProgStart)             // code
    ulMemoryByteCount = pTUsed->ProgSize;
  else if (ulRegionAddress == pTUsed->DataStart)        // data
    ulMemoryByteCount = pTUsed->DataSize;
  else
    ulMemoryByteCount = 14;                             // ID or fuses

  if (!bQuiet) {
    ll = printf("Transferring %s memory", szRegionName);
    for ( ; ll < PROGRESS_COLUMN; ll++)                   // until column
      printf(".");                                        // filler
    printf("    ");                                       // space
    fflush(stdout);
    }
#if defined(__DEBUG__)
  printf("\nRegion Address %06lX, size %lu, alg %lu\n",
          ulRegionAddress, ulRegionSize, ulAlg);
#endif
  ulBurst = 0;                                          // init (but invalid!)
  rc = XW_NOERROR;                                      // to enter the loop
  for (i=0; i<ulRegionSize && rc==XW_NOERROR; i+=ulBurst) { // region
    if (ulAlg==ALG_PIC18 || ulAlg==ALG_PIC18A) {        // 18Fxxx family
      if (Segment == ProgRegion)                        // program memory
//      ulBurst = pTUsed->WriteBurst;                   // variable
        ulBurst = 8;                                    // still fixed!!
      else if (Segment == IDRegion) {                   // ID memory
        ulBurst = 8;                                    // fized 8 bytes
        }
      else                                              // other memory
        ulBurst = 1;                                    // single byte
      if ( !BlankCheck(Segment + i,                     // Note: word check
                       ulBurst,                         // even when written
                       pTUsed->BlankMask)) {            // 1 byte at a time!
        memcpy(ucData, Segment + i, ulBurst);
        rc = WriteTargetRegionPart(ucData,
                                   ulBurst,
                                   ulRegionAddress + i);
        if (!bQuiet) {
          if (bVerbose) {                                 // progress
            printf("\n%06lX : %02hx",
                   ulRegionAddress + i, (USHORT)ucData[0]);
            if (ulBurst > 1) {
              printf("%02hx",
                     (USHORT)ucData[1]);
              for (j = 2; j < ulBurst; j += 2)
                printf(" %02hx%02hx",
                        (USHORT)ucData[j], (USHORT)ucData[j+1]);
              }
            }
          else if (i % 128 == 0)                          // display progress
            printf( szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        }
      }

    else if (ulAlg == ALG_PIC16D  ||                    // 16F818/9 etc
             ulAlg == ALG_PIC16E                        // 16F91x etc
// ???       ulAlg == ALG_PIC16G                        // 16F88X
// ???       ulAlg == ALG_PIC16H                        //
                                ) {
      if (Segment == ProgRegion)                        // code
        ulBurst = 8;                                    // fixed 4 words
      else                                              // other regions
        ulBurst = 2;                                    // fixed 1 word
      if ( !BlankCheck(Segment + i,
                       ulBurst,
                     (Segment == DataRegion) ? 0x00FF : pTUsed->BlankMask)) {
        for (j=0; j<ulBurst; j += 2) {
          ucData[j] = Segment[i + j + 1];               // copy 1st byte
          ucData[j+1] = Segment[i + j];                 // copy 2nd byte
          }
        rc = WriteTargetRegionPart(ucData,
                                   ulBurst,
                                   ulRegionAddress + i);
        if (!bQuiet) {
          if (bVerbose) {                                 // progress
            printf("\n%06lX : %02hx%02hx",
                   ulRegionAddress + i,
                   (USHORT)ucData[0], (USHORT)ucData[1]);
            for (j = 2; j < ulBurst; j += 2)
              printf(" %02hx%02hx",
                      (USHORT)ucData[j], (USHORT)ucData[j+1]);
            }
          else if (i % 128 == 0)          // display progress
            printf(szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        }
      }

    else if (ulAlg == ALG_PIC16A) {                     // 16F7xx
//    if (Segment == ProgRegion)                        // code
//      ulBurst = 4;                                    // 2 words
//    else                                              // other regions
        ulBurst = 2;                                    // word by word
      if ( !BlankCheck(Segment + i,
                       ulBurst,
                     (Segment == DataRegion) ? 0x00FF : pTUsed->BlankMask)) {
        for (j=0; j<ulBurst; j++,j++) {
          ucData[j] = Segment[i + j + 1];               // copy 1st byte
          ucData[j+1] = Segment[i + j];                 // copy 2nd byte
          }
        rc = WriteTargetRegionPart(ucData,
                                   ulBurst,
                                   ulRegionAddress + i);
        if (!bQuiet) {
          if (bVerbose) {                                 // progress
            printf("\n%06lX : %02hx%02hx",
                   ulRegionAddress + i,
                   (USHORT)ucData[0], (USHORT)ucData[1]);
            for (j = 2; j < ulBurst; j += 2)
              printf(" %02hx%02hx",
                      (USHORT)ucData[j], (USHORT)ucData[j+1]);
            }
          else if (i % 128 == 0)                          // display progress
            printf(szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        }
      }

    else if (ulAlg == ALG_PIC16F) {                     // 16F716
#if defined(__DEBUG__)
      printf("Segment = %06lX, ProgRegion %06lX, IDRegion %06x\n",
             Segment, ProgRegion, IDRegion);
#endif
      if (ulRegionAddress == pTUsed->ProgStart ||       // code memory
          ulRegionAddress == pTUsed->IDStart)           // ID memory
        ulBurst = 8;                                    // 4 words
      else                                              // other regions
        ulBurst = 2;                                    // word by word
      if ( !BlankCheck(Segment + i,
                       ulBurst,
                     (Segment == DataRegion) ? 0x00FF : pTUsed->BlankMask)) {
        for (j=0; j<ulBurst; j++,j++) {
          ucData[j] = Segment[i + j + 1];               // copy 1st byte
          ucData[j+1] = Segment[i + j];                 // copy 2nd byte
          }
        rc = WriteTargetRegionPart(ucData,
                                   ulBurst,
                                   ulRegionAddress + i);
        if (!bQuiet) {
          if (bVerbose) {                                 // progress
            printf("\n%06lX : %02hx%02hx",
                   ulRegionAddress + i,
                   (USHORT)ucData[0], (USHORT)ucData[1]);
            for (j = 2; j < ulBurst; j += 2)
              printf(" %02hx%02hx",
                      (USHORT)ucData[j], (USHORT)ucData[j+1]);
            }
          else if (i % 128 == 0)                          // display progress
            printf(szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        }
      }

    else {                                              // other algorithms
      ulBurst = 2;                                      // fixed 1 word
      if ( !BlankCheck(Segment + i,
                       ulBurst,
                     (Segment == DataRegion) ? 0x00FF : pTUsed->BlankMask)) {
        ucData[0] = Segment[i + 1];                     // copy 1st byte
        ucData[1] = Segment[i];                         // copy 2nd byte
        rc = WriteTargetRegionPart(ucData, ulBurst, ulRegionAddress + i);
        if (!bQuiet) {
          if (bVerbose)                                   // progress
            printf("\n%06lX : %02hx%02hx",
                   ulRegionAddress + i,
                   (USHORT)ucData[0], (USHORT)ucData[1]);
          else if (i % 128 == 0)                          // display progress
            printf(szPct, 100 * i / ulMemoryByteCount);
          fflush(stdout);
          }
        }
      }

    }

  if (rc == XW_NOERROR  &&  !bQuiet) {
    if (bVerbose)
      printf("\n");
    else
      printf(szPctNL, 100);
    }

  return rc;
  }


// -------------------------------------
//
// Transmit Part of a Region of Image to target
//
// Input   - pointer to output buffer
//         - length of buffer data in bytes
//         - target destination address
//
// Output  - display progress
//
// Returns result code of last write operation
//
// -------------------------------------
static  ULONG  WriteTargetRegionPart(char  *ucBuffer,
                                     ULONG  ulDataLength,
                                     ULONG  ulDestinationAddress) {

  ULONG  rc;                                            // returncode

  AdjustAddress(ulDestinationAddress);                  // prepare write
  rc = WbusWrite(ucBuffer, ulDataLength);               // (maybe lazy)

  return rc;
  }


// --------------------------------------------------------
//
// Transfer Image to target and verify result
//
// Input   - pointer to image
//
// Output  - 0 for OK
//         - otherwise error
//
// Remarks: Image is transferred and verified region-by-region
//
// Remarks - takes into account the 'preserve' memory areas
//         - data, ID and Fuses transferred to 18F only when
//           appropriate extended segment record present in hex file
//
// --------------------------------------------------------
extern  int    WriteVerifyTarget(void) {

  int        rc;                                        // returncode
  ULONG      ulStartWriteVerify;                        // file transfer time
  ULONG      ulFam;                                     // code index
  USHORT     usMemWord;                                 // word manipulations
  PPRESERVE  pPres;                                     // ptr to preserve

  if ((rc = ProgrammerActivate()) != XW_NOERROR)        // set 'active'
    return rc;                                          // problem

  IdentifyTarget();                                     // detect target
  if (pTUsed == NULL)                                   // undetermined
    return  XW_NOENT;                                   // problem

  if (!bQuiet)
    printf("Transferring program to %s via %s (%s)\n",
           pTUsed->Name, szWbusDevice, szAlgorithmName[pTUsed->Algorithm]);

  ulStartWriteVerify = OS_Time();                       // start of transfer

  ulFam = pTUsed->FamilyIndex;                          // code index

  rc = XW_NOERROR;                                      // init

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_PROG) {                         // region included
    pPres = pTUsed->pPreserve;                          // first preserve
    while (pPres != NULL) {                             // all preserves
      if (pPres->Address >= pTUsed->ProgStart &&        // within progmem
          pPres->Address <= pTUsed->ProgStart + pTUsed->ProgSize - 2) {
        usMemWord = BYTES2WORD(ProgRegion + pPres->Address);  // hex contents
        usMemWord &= ~pPres->BitMask;                   // keep non mask bits
        usMemWord |= (pPres->Contents & pPres->BitMask); // add original bits
        usMemWord &= pTUsed->BlankMask;                 // implemented bits
#if defined(__DEBUG__)
        printf("Original contents address %04hX : %04hX,"
                        " mask %04hX, returned : %04hX\n",
                        pPres->Address, pPres->Contents,
                        pPres->BitMask, usMemWord);
#endif
        WORD2BYTES(usMemWord, ProgRegion + pPres->Address);
        }
      pPres = pPres->next;                              // next in chain
      }
    rc = WriteTargetRegion("program",
                           ProgRegion,
                           pTUsed->ProgStart,
                           pTUsed->ProgSize,
                           pTUsed->Algorithm);
    if (rc == XW_NOERROR) {
      rc = VerifyTargetRegion("program",
                              ProgRegion,
                              pTUsed->ProgStart,
                              pTUsed->ProgSize,
                              pTUsed->BlankMask);
      }
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_DATA  &&                        // region included
      pTUsed->DataSize > 0) {                           // target has it
    rc = WriteTargetRegion("data",
             (ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
              pTUsed->DataStart,
              pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),   // 1.8.2-3
              pTUsed->Algorithm);
    if (rc == XW_NOERROR) {
      rc = VerifyTargetRegion("data",
              (ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
              pTUsed->DataStart,
              pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),   // 1.8.2-3
              (ulFam == CORE_16) ? 0xFFFF : 0x00FF);
      }
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_ID) {                           // region included
    ulLastAddress = MINUS1;                             // force WbusProgram
    rc = WriteTargetRegion("ID",
             (ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
              pTUsed->IDStart,
              pTUsed->IDSize,
              pTUsed->Algorithm);
    if (rc == XW_NOERROR) {
      rc = VerifyTargetRegion("ID",
                (ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
                 pTUsed->IDStart,
                 pTUsed->IDSize,
                 pTUsed->BlankMask);
      }
    }

  if (rc == XW_NOERROR  &&                              // all OK so far
      fRegions & REGION_FUSES) {                        // region included
    ControlFuses();                                     // fuses specials
    ulLastAddress = MINUS1;                             // force WBusProgram
    rc = WriteTargetRegion("fuses",
             (ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
              pTUsed->FusesStart,
              pTUsed->FusesSize,                        // target dependent
              pTUsed->Algorithm);
    if (rc == XW_NOERROR) {
      ulLastAddress = MINUS1;                           // force WBusProgram
      rc = VerifyTargetRegion("fuses",
              (ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
               pTUsed->FusesStart,
               pTUsed->FusesSize,                       // target dependent
               pTUsed->BlankMask);
      }
    }

  return ReportResult("Write-Verify operation", rc, ulStartWriteVerify);
  }
