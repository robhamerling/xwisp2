
// ===================================================================
//
// Test of XWisp2cfg routines and PIC specifications in XWisp2.cfg
//
// - Read XWisp2.cfg and included files
// - Either write formatted data with properties line-by-line to stdout
//   or write single line with all properties per PIC to stdout
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================

#include "xwisp2.h"                             // all common headers

// ---------------------------------------------
extern int main(int args, char **argv) {


  ULONG       i = 0, j = 0;                     // counter(s)
  int         rc = 0;                           // returncode
  PTARGETINFO pT = NULL;                        // pointer to targetinfo
  char        ListType = 'L';                   // 'L'ong: parm per line
  PPRESERVE   pPres = NULL;                     // ptr to preserve spec
                                                // 'W'ide: line per PIC
  if (args > 1) {
    if (argv[1][0] == '-' || argv[1][0] == '/') {
      if (argv[1][1] == 'W' || argv[1][1] == 'w')
        ListType = 'W';
      else if (argv[1][1] == 'L' || argv[1][1] == 'l')
        ListType = 'L';
      else {
        printf("Invalid option: %s (valid options: -W, -L)\n",
                         argv[1]);
        return 99;
        }
      }
    else {
      printf("Invalid commandline parameter: %s\n"
                      "Valid are only options -W and -L\n",
                      argv[1]);
      return 99;
      }
    }

  rc = ReadPICSpecs(argv[0]);                   // read XWisp2.cfg
  if (rc)                                       // problem
    return rc;

  SearchPICLast(&i);                            // get # of PICs listed

  if (ListType == 'W'  ||  ListType == 'L')
    printf("\n;  XWisp2 configuration files contain"
           " specifications of %lu PICs.\n\n", i);

  if (ListType == 'W') {                        // header for wide list
    printf(" Name     Alias  DataSheet PgmSpec Core  Alg.  DevID  rev-"
           "     ProgMem         IDMem         DevIDmem      ConfigMem"
           "       DataMem     Prot Delay  Write"
           " ~Fixed-0                       Fixed-1"
           "                       Preserved bits\n"
           "                                                      mask"
           "      range          range           range         range  "
           "        range      mask        Burst\n\n");
    }

  for (i=0; ; ++i) {                            // exit by break
    pT = SearchPICNumber(i);                    // next
    if (pT == NULL)                             // end of chain reached
      break;                                    // done
    if (ListType == 'L') {
      printf("\n PIC %s\n", pT->Name);
      printf("\tAlias         = %s\n", pT->Alias);
      printf("\tDatasheet     = %s\n", pT->DataSheet);
      printf("\tPgmSpec       = %s\n", pT->PgmSpec);
      printf("\tFamily        = %hu\n", pT->Core);
      printf("\tAlgorithm     = %s\n",
                                szAlgorithmName[pT->Algorithm]);
      printf("\tDelay         = %u\n", pT->Delay);
      if (pT->FamilyIndex == CORE_16)
        printf("\tWriteBurst    = %hu\n", pT->WriteBurst);
      printf("\tDeviceID      = %04hX\n", pT->DevID);
      printf("\tRevisionMask  = %04hX\n", pT->RevMask);

      if (pT->FamilyIndex != CORE_16)
        printf("\tProgStart     = %04lX\n", pT->ProgStart);
      else
        printf("\tProgStart     = %06lX\n", pT->ProgStart);
      printf("\tProgSize      = %lu%c\n",
              ((pT->ProgSize % 1024) == 0) ? pT->ProgSize/1024 : pT->ProgSize,
              ((pT->ProgSize % 1024) == 0) ? 'K' : ' ');

      if (pT->DataSize > 0) {
        if (pT->FamilyIndex != CORE_16)
          printf("\tDataStart     = %04lX\n", pT->DataStart);
        else
          printf("\tDataStart     = %06lX\n", pT->DataStart);
        }
      printf("\tDataSize      = %lu\n",   pT->DataSize);

      if (pT->FamilyIndex != CORE_16)
        printf("\tIDStart       = %04lX\n", pT->IDStart);
      else
        printf("\tIDStart       = %06lX\n", pT->IDStart);
      printf("\tIDSize        = %lu\n", pT->IDSize);

      if (pT->FamilyIndex != CORE_16) {
        if (pT->DevID != 0x0000) {
          printf("\tDeviceIDStart = %04lX\n", pT->DevIDStart);
          printf("\tDeviceIDSize  = %lu\n", pT->DevIDSize);
          }
        }
      else {
        printf("\tDeviceIDStart = %06lX\n", pT->DevIDStart);
        printf("\tDeviceIDSize  = %lu\n", pT->DevIDSize);
        }

      if (pT->FamilyIndex != CORE_16)
        printf("\tFusesStart    = %04lX\n", pT->FusesStart);
      else
        printf("\tFusesStart    = %06lX\n", pT->FusesStart);
      printf("\tFusesSize     = %lu\n",   pT->FusesSize);

      printf("\tProtectMask   = %04hX\n", pT->ProtectMask);
      printf("###     BlankMask     = %04hX\n", pT->BlankMask);
      printf("\tFuseFixedZero = ");
      for (j=0; j<pT->FusesSize; j += 2) {
        if (pT->FamilyIndex != CORE_16)               // midrange
          printf("%02X%02X", pT->FuseFixedZero[j+1], pT->FuseFixedZero[j]);
        else                                    // 18Fxxx family
          printf("%02X%02X", pT->FuseFixedZero[j], pT->FuseFixedZero[j+1]);
        }
      printf("\n");
      printf("\tFuseFixedOne  = ");
      for (j=0; j<pT->FusesSize; j += 2) {
        if (pT->FamilyIndex != CORE_16)                // midrange
          printf("%02X%02X", pT->FuseFixedOne[j+1], pT->FuseFixedOne[j]);
        else                                     // 18Fxxx family
          printf("%02X%02X", pT->FuseFixedOne[j], pT->FuseFixedOne[j+1]);
        }
      printf("\n");
      pPres = pT->pPreserve;                    // first in chain
      while (pPres != NULL) {
        printf("\tPreserve      = %04hX,%04hX\n",
                pPres->Address, pPres->BitMask);
        pPres = pPres->next;                    // next in chain
        }
      printf("\tStatus        = %s\n",
        (pT->Status == WISP648_TESTED) ? "Tested" :
          ((pT->Status == WISP648_FAMILY) ? "Family" : "Untested"));

      fflush(stdout);
      }

    if (ListType == 'W') {
      printf("%-9s ", pT->Name);
      printf("%-7s ", pT->Alias);
      printf("%-8s ", pT->DataSheet);
      printf("%-8s ", pT->PgmSpec);
      printf("%2hu  ", pT->Core);
      printf("%-6s  ", szAlgorithmName[pT->Algorithm]);
      printf("%04hX  ", pT->DevID);
      printf("%04hX  ", pT->RevMask);
      if (pT->FamilyIndex != CORE_16) {                 /* low- and mid-range */
        printf("  %04lX-%04lX    ",
                  pT->ProgStart, pT->ProgStart + pT->ProgSize - 1);
        printf("  %04lX-%04lX    ",
                  pT->IDStart, pT->IDStart + pT->IDSize - 1);
        if (pT->DevID != 0)
          printf("  %04lX-%04lX    ",
                    pT->DevIDStart, pT->DevIDStart + pT->DevIDSize - 1);
        else
          printf("      -        ");
        printf("  %04lX-%04lX    ",
                  pT->FusesStart, pT->FusesStart + pT->FusesSize - 1);
        if (pT->DataSize > 0)
          printf("  %04lX-%04lX    ",
                    pT->DataStart, pT->DataStart + pT->DataSize - 1);
        else
          printf("      -        ");
        }
      else {                                    // 18Fxxxx
        printf("%06lX-%06lX  ",
                  pT->ProgStart, pT->ProgStart + pT->ProgSize - 1);
        printf("%06lX-%06lX  ",
                  pT->IDStart, pT->IDStart + pT->IDSize - 1);
        printf("%06lX-%06lX  ",
                  pT->DevIDStart, pT->DevIDStart + pT->DevIDSize - 1);
        printf("%06lX-%06lX  ",
                  pT->FusesStart, pT->FusesStart + pT->FusesSize - 1);
        if (pT->DataSize > 0)
          printf("%06lX-%06lX  ",
                     pT->DataStart, pT->DataStart + pT->DataSize - 1);
        else
          printf("      -        ");
        }
      printf("%04hX  ", pT->ProtectMask);
      printf("%3u  ", pT->Delay);
      if (pT->FamilyIndex != CORE_16)                 // low and mid range
        printf("    -   ");
      else                                      // 18Fxxxx
        printf("%5hu   ", pT->WriteBurst);
      for (j=0; j<pT->FusesSize; j += 2) {
        if (pT->FamilyIndex != CORE_16)               // midrange
          printf("%02X%02X", pT->FuseFixedZero[j+1], pT->FuseFixedZero[j]);
        else                                    // 18Fxxx family
          printf("%02X%02X", pT->FuseFixedZero[j], pT->FuseFixedZero[j+1]);
        }
      for (   ; j < sizeof(pT->FuseFixedZero); j += 2)
        printf("    ");
      printf("  ");
      for (j=0; j<pT->FusesSize; j += 2) {
        if (pT->FamilyIndex != CORE_16)         // midrange
          printf("%02X%02X", pT->FuseFixedOne[j+1], pT->FuseFixedOne[j]);
        else                                    // 18Fxxx family
          printf("%02X%02X", pT->FuseFixedOne[j], pT->FuseFixedOne[j+1]);
        }
      for (   ; j < sizeof(pT->FuseFixedZero); j += 2)
        printf("    ");
      printf("  ");
      pPres = pT->pPreserve;                    // first in chain
      while (pPres != NULL) {
        printf("%04hX:%04hX ", pPres->Address, pPres->BitMask);
        pPres = pPres->next;                    // next in chain
        }
      printf("\n");
      }
      fflush(stdout);

    }

  printf("\n");

  ReleasePICSpecs();                            // free allocated memory
  return 0;
  }
