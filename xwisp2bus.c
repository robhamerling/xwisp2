
// =======================================================================
// XWisp2Bus.C - Wisp648 Programmer support.
//
//          - Elementary Wbus protocol functions
//          - Some higher level helper functions (to obtain reply)
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023, R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// Remarks: When appropriate the state of Wisp648 and
//          the last programmed address will be updated.
//
//
// State diagram:
//
//
//   +---------+   'break'     +-----------+   hello  +----------+
//   |         |--------------|           |---------|          |
//   |  SLEEP  |               | ATTENTION |          |  ACTIVE  |
//   |  state  |--------------|   state   |---------|   state  |
//   +---------+  passthrough  +-----------+  'break' +----------+
//                                                        |
//                                   |                     |
//                            'break'|   +-------------+   |passthrough
//                                   |   |             |   |
//                                   '---| PASSTHROUGH |--'
//                                       |    state    |
//                                       +-------------+
//
// =======================================================================

// #define __DEBUG__

#include "xwisp2.h"                             // all includes


static const  char  *szNoReply =  "<no reply>";
static        char   szDevice[12];                      // programmer name

#ifdef __DEBUG__
  char *szWispState[4] = {"SLEEP",
                          "ATTENTION",
                          "ACTIVE",
                          "PASSTHROUGH"};
#endif

// prototypes of locally used functions

static  int    SolicitReply(char *, ULONG, ULONG);


// --------------------------------------
// Solicit reply from Wisp648
//
// input:  - pointer to reply buffer
//         - minumum length of expected reply (0 for _string_)
//         - length of reply buffer
//
// output  - when rc=0 reply buffer will contain reply
//           (as string with terminating zero,
//           leading and trailing blanks stripped for variable reply)
//
// returns - result code of last SendReceive
//
// Remarks: no state change of Wisp648
// --------------------------------------
static  int    SolicitReply(char  *szReply,             // buffer
                            ULONG  ulReplySize,         // expected reply
                            ULONG  ulBufSize) {         // size of buffer

  int    i;                                             // counter(s)
  int    rc;
  char   szBuffer[80];                                  // log buffer

  szReply[0] = '\0';                                    // init to null reply
  szBuffer[0] = 'n';                                    // at least 1 char
  for (i=1; i<ulReplySize  &&  i<sizeof(szBuffer)-1; ++i)  // fixed reply
    szBuffer[i] = 'n';                                  // next byte
  szBuffer[i] = '\0';                                   // end of string
  rc = SendReceive(szBuffer, szReply);
  if (rc) {
    sprintf(szBuffer, "WbusNext failure, rc %d", rc);
    LogMsg(szBuffer);
    return  rc;
    }

  if (szReply[0] == ' ') {                              // _string_ reply
    i = 0;
    if (ulReplySize > 0) {                              // multiple chars read
      for (  ; i<ulReplySize-1; ++i)                    // received part
        szReply[i] = szReply[i+1];                      // shift 1 left
      }
    for (  ; i<ulBufSize-1; ++i) {                      // rest of reply
      rc = SendReceiveSlow("n", szReply + i);           // byte-by-byte
      if (rc != XW_NOERROR) {                           // problem
        sprintf(szBuffer, "WbusNext failure, rc %d", rc);
        LogMsg(szBuffer);
        break;
        }
      if (szReply[i] == ' ')                            // end of reply
        break;
      }
    szReply[i] = '\0';                                  // end of string
    }
#if defined(__DEBUG__)
  printf("Solicited reply: '%s'\n", szReply);
#endif
  return rc;
  }


// --------------------------------------
// WbusGo - release target PIC
//
// Remarks: no state change of Wisp648
// --------------------------------------
extern  int    WbusGo(void) {

  int    rc;
  char   ucReply[8];
  char   szBuffer[80];

  LogMsg("WbusGo()");
  rc = SendReceive("0000g", ucReply);
  if (rc) {
    sprintf(szBuffer, "WbusGo failure, rc %d", rc);
    LogMsg(szBuffer);
    }
  return rc;
  }


// ------------------------------------------
// WbusHello - Activate Wisp648
//
// - send a single byte ('a') to check if programmer echoes
//   - when no echo (timeout) or more than 2 bytes: problem
//   - when single echo: OK
// - send hello command
//
// Remarks: Wisp648 state changes to 'active'
// ------------------------------------------
extern  int    WbusHello(void) {

  int    rc;
  char   szReply[16];
  char   szBuffer[80];
  char   szEcho[] = {'a' ,0x00};                        // determine if echoing


  rc = SendReceiveSlow(szEcho, szReply);                // to check echo
  if (rc != XW_NOERROR) {                               // problem
    sprintf(szBuffer, "Communications error, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
    }

  if (strlen(szReply) != 1  ||  szReply[0] != 'A') {
    sprintf(szBuffer, "WbusHello echotest failure:"
                      " sent 'a' received: '%s', %s",
                      szReply,
                      (ulWbusBaudrateActual != ulWbusBaudrateDefault)
                                       ? "wrong baudrate?"
                                       : "unexpected.");
    LogMsg(szBuffer);
    return XW_IOERROR;
    }

  LogMsg("WbusHello()");
  rc = SendReceiveSlow("0000h", szReply);               // byte-by-byte
  if (rc != XW_NOERROR  ||  strcmp(szReply, "0000H")) {  // problem
    if (rc != XW_NOERROR)
      sprintf(szBuffer, "WbusHello failure, rc %d", rc);
    else
      sprintf(szBuffer, "WbusHello failure, %s (received: %s)",
                        (ulWbusBaudrateActual != ulWbusBaudrateDefault)
                                         ? "wrong baudrate?"
                                         : "unexpected echo",
                         szReply);
    LogMsg(szBuffer);
    return XW_IOERROR;
    }
  ulWispState = WISPACTIVE;                             // now active
  return XW_NOERROR;                                    // OK
  }


// -------------------------------------------
// WbusIncrement - step to next memory address
//
// Remarks: no state change of Wisp648
// -------------------------------------------
extern  int    WbusIncrement(void) {

  int    rc;
  char   szReply[8];
  char   szBuffer[80];

  LogMsg("WbusIncrement()");
  rc = SendReceiveSlow("i", szReply);                   // single byte
  if (rc) {
    sprintf(szBuffer, "WbusIncrement failure, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
    }
  ulLastAddress += 2;                                   // bytes!
  return XW_NOERROR;
  }


// --------------------------------------------------------------
// WbusJump - jump to specified memory address
//
// Remarks: no state change of Wisp648
//
// Note: 16-bit core uses byte addresses,
//       For others PICs the destination address is divided by 2.
// --------------------------------------------------------------
extern  int    WbusJump(ULONG ulLocation) {

  int    rc;
  char   szReply[12];
  char   szBuffer[80];

  sprintf(szBuffer, "WbusJump(%06lx)", ulLocation);
  LogMsg(szBuffer);
  sprintf(szBuffer, "%06lxm",
                    (pTUsed->FamilyIndex == CORE_16) ? ulLocation
                                                     : ulLocation/2);
  rc = SendReceive(szBuffer, szReply);                  // burst preferred
  if (rc) {
    sprintf(szBuffer, "WbusJump failure, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
    }
  ulLastAddress = ulLocation;                           // adjust address
  return XW_NOERROR;
  }


// ------------------------------------------------------
// WbusPassThrough - set passthrough mode
//
// input   - passthrough code 0x00ab
//           - b = 0..3 : passthrough mode ('a' must be 0)
//           - b = 4    : change speed to value indicated with 'a'
//           - a = 0..4 : speed index value (when 'b' = 4)
//                        0 = 9600
//                        1 = 19200
//                        2 = 38400
//                        3 = 57600
//                        4 = 115200
//
// output  -
//
// Remarks: When low nibble not '4'
//            Wisp648 state changes:
//            - from 'active' to 'passthrough'
//            - from 'attention' to 'sleep'
//          Otherwise (speed change): no state change
//
//
// -----------------------------------------------------
extern  int    WbusPassThrough(USHORT usPassCfg) {

  int    rc;
  char   szReply[8];
  char   szBuffer[80];

  sprintf(szBuffer, "WbusPassThrough(%04hx)", usPassCfg);
  LogMsg(szBuffer);
  sprintf(szBuffer, "%04hx", usPassCfg);                // 'p' parameters
  rc = SendReceive(szBuffer, szReply);                  // 1st part
  if (rc == 0)                                          // OK
    rc = SendByte('p');                                 // 2nd part
                                                        // ignore response
  if (rc) {
    sprintf(szBuffer, "PassThrough failure, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
   }

  if ((usPassCfg & 0x000F) != 0x0004) {                 // not speed change
    if (ulWispState == WISPACTIVE)                // currently active
      ulWispState = WISPPASSTHROUGH;
    else if (ulWispState == WISPATTENTION)        // currently attention
      ulWispState = WISPSLEEP;
#ifdef __DEBUG__
    printf("Programmer now in '%s' state\n", szWispState[ulWispState]);
#endif
    }

  return XW_NOERROR;
  }


// ----------------------------------------------------
// WbusProgram - Request programmer to enter programming mode
//
// input:  memory type 0xddpm
//           - 'dd' write delay 00..ff, in units of 0.1 milliseconds
//                  (ignored for other commands than write)
//                  NOTE: This value automatically added here!
//           - 'p'  progamming algorithm code 0..7 (0..9 with 1.10)
//           - 'm'  memory type: c=code, d=data, e=bulk-erase f=config
//
// output: nothing
//
// returns result of SendReceive()
//
// Remarks: - no state change of Wisp648
//          - TProg-delay will be included in command
//          - ulLastAddress global variable will be updated
// ----------------------------------------------------
extern  int    WbusProgram(USHORT usMemType) {

  int    rc;
  char   szReply[8];
  char   szBuffer[80];

  sprintf(szBuffer, "WbusProgram(%02hx%02hx)",
                     (usDelay) ? usDelay : pTUsed->Delay,
                     usMemType);
  LogMsg(szBuffer);
  sprintf(szBuffer, "%02hx%02hxx",
                     (usDelay) ? usDelay                // general override
                               : ((pTUsed->Delay) ? pTUsed->Delay : 0),
                     usMemType);
  rc = SendReceive(szBuffer, szReply);                  // burst preferred
  if (rc) {
    sprintf(szBuffer, "WbusProgram failure, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
    }
  ulLBA = 0;
  if ((usMemType & 0x000F) == 0x000c) {
    ulLBA = ulLastAddress = pTUsed->ProgStart;
    if ((usMemType & 0x00F0) == 0x00a0)                 // 12-bits core
      WbusIncrement();                                  // wrap
    }
  else if ((usMemType & 0x000F) == 0x000d)
    ulLBA = ulLastAddress = pTUsed->DataStart;
  else if ((usMemType & 0x000F) == 0x000f) {
    ulLBA = ulLastAddress = pTUsed->IDStart;
    if ((usMemType & 0x00F0) == 0x00a0)                 // 12-bits core
      ulLastAddress = 0x0FFF;           /* ??? */       // fuses address
    }
  else {                                                // erase (+ unknown)
    ulLBA = 0;                                          // reset
    ulLastAddress = MINUS1;                             // force jump
    }
  return XW_NOERROR;
  }


// --------------------------------------
// WbusRead - read from current address
//
// Remarks: no state change of Wisp648
// --------------------------------------
extern  int    WbusRead(char  *szReplyBuffer,           // buffer
                        ULONG  ulReplySize,             // expected reply
                        ULONG  ulBufSize) {             // buffer size

  int    rc;
  char   szReply[4];
  char   szBuffer[80];

  rc = SendReceiveSlow("r", szReply);                   // single byte!
  if (rc) {
    sprintf(szBuffer, "WbusRead failure, rc %d", rc);
    LogMsg(szBuffer);
    return rc;
    }
  rc = SolicitReply(szReplyBuffer, ulReplySize, ulBufSize);   // obtain reply
  sprintf(szBuffer, "WbusRead(%06lx : %s)",
                     ulLastAddress,
                     (rc == XW_NOERROR) ? szReplyBuffer : szNoReply);
  LogMsg(szBuffer);
  if (bBurstReading)                                    // 8 bytes reads
    ulLastAddress += 8;                                 // auto increment
  else if (pTUsed->Algorithm == ALG_PIC18  ||           // auto increment
           pTUsed->Algorithm == ALG_PIC18A)             // auto increment
    ulLastAddress += 1;                                 // 1 byte
  return XW_NOERROR;
  }


// --------------------------------------
// WbusType - request programmer type
//
// Input  - nothing
//
// Output - nothing
//
// Returns - pointer to string with WBus device name
//         - NULL with failure to obtain device name
//
// Remarks: no state change of Wisp648
// --------------------------------------
extern  char   *WbusType(void) {

  int    rc;
  char   szReply[12];
  char   szBuffer[80];

  rc = SendReceiveSlow("t", szReply);                   // single byte
  if (rc)
    printf("WbusType failure, rc %d\n", rc);
  else {
    rc = SolicitReply(szBuffer, 0, sizeof(szBuffer));   // obtain string
    if (rc)
      printf("Programmer type not received, rc %d\n", rc);
    else {
      if (bVerbose)
        printf("Programmer type string: '%s'\n", szBuffer);
      strncpy(szDevice, szBuffer, sizeof(szDevice)-1);
      szDevice[sizeof(szDevice) - 1] = '\0';            // string termination
      sprintf(szBuffer, "WbusType(%s)", szDevice);
      LogMsg(szBuffer);
      }
    }
  return (char *)szDevice;
  }


// ---------------------------------------------------
// WbusVersion - request programmer firmware version
//
// Input  - nothing
//
// Output - nothing
//
// Returns firmware level value (100 * Major + Minor)
//
// Remarks: no state change of Wisp648
// ---------------------------------------------------
extern  int    WbusVersion(void) {

  int    rc;
  ULONG  ulMajor;                                       // major version
  ULONG  ulMinor;                                       // minor version
  char   szReply[10];
  char   szBuffer[80];
  char  *s;                                             // ptr to string

  ulMajor = ulMinor = 0;                                // for default reply
  rc = SendReceiveSlow("v", szReply);                   // single byte
  if (rc)                                               // problem
    printf("WbusVersion failure, rc %d\n", rc);
  else {
    rc = SolicitReply(szReply, 0, sizeof(szReply));   // obtain reply
    if (rc)
      printf("Firmware version not received, rc %d\n", rc);
    else {
      if (bVerbose)
        printf("Programmer firmware version string: '%s'\n", szReply);
      s = strtok(szReply, ".");                         // probably Wisp648
      if (s != NULL) {
        ulMajor = atol(s);                              // before '.'
        s = strtok(NULL, ".");                          // '.' or end of string
        if (s != NULL)
          ulMinor = atol(s);                            // after '.'
        }
      else {                                            // probably WISP 'nnnn'
        ulMajor = atol(s) / 100;
        ulMinor = atol(s) % 100;
        }
      }
    }
  sprintf(szBuffer, "WbusVersion(%lu.%02lu)", ulMajor, ulMinor);
  LogMsg(szBuffer);
  return  100 * ulMajor + ulMinor;                      // firmware level
  }


// -----------------------------------------------
// WbusWrite - Write to current address
//             (if 'LAZY' specified, the LazyWrite will be used)
//
// Remarks: no state change of Wisp648
// -----------------------------------------------
extern  int    WbusWrite(char  *pucData,
                         ULONG  ulLength) {

  int    rc;
  ULONG  ll;
  char   szReply[24];
  char   szBuffer[192];
  int    i;

  ll = sprintf(szBuffer, "Wbus%sWrite(%06lx : ",
               (bLazyWrite) ? "Lazy" : "",
               ulLastAddress);
  for (i=0; i<ulLength; i++)
    ll += sprintf(szBuffer + ll, "%02hx", (unsigned short)pucData[i]);
  ll += sprintf(szBuffer + ll, ")");
  LogMsg(szBuffer);
  ll = 0;
  for (i=0; i<ulLength; i++)
    ll += sprintf(szBuffer + ll, "%02hx", (unsigned short)pucData[i]);
  strcat(szBuffer, (bLazyWrite) ? "l" : "w");
  rc = SendReceive(szBuffer, szReply);                  // auto burst/byte
  if (rc) {                                             // problem
    sprintf(szBuffer, "Wbus%sWrite failure, rc %d",
                       (bLazyWrite) ? "Lazy" : "",
                       rc);
    LogMsg(szBuffer);
    return rc;
    }
  if ( pTUsed->Algorithm == ALG_PIC18  ||                // all 18Fxxxx
       pTUsed->Algorithm == ALG_PIC18A ||                // all 18Fxxxx
      ((pTUsed->Algorithm == ALG_PIC16D  ||
        pTUsed->Algorithm == ALG_PIC16E  ||
        pTUsed->Algorithm == ALG_PIC16F  ||
        pTUsed->Algorithm == ALG_PIC16G) && (ulLength == 8)) )
    ulLastAddress += ulLength;                          // update
  return XW_NOERROR;
  }
