# XWisp2  -  by Rob Hamerling #

         Support program for Wisp648 PIC programmer for
        eComStation (OS/2), Linux, FreeBSD, MacOS, Windows
    Copyright (c) 2002..2023, R. Hamerling. All rights reserved.

XWisp2 is a support program for the Wisp648 PIC programmer.
Basically it has the same purpose and functionality as the original
program [XWisp of Wouter van Ooijen](http://www.voti.nl/xwisp),
however XWisp2 is a single standalone executable without the need for any
runtime environment (ic Python). Xwisp2 is written in C, sources are available.

_Note: Xwisp2 was originally developed for a 32-bits environment (IBM OS/2).
      The source may not compile without issues for a 64-bits environment._


## Acknowledgements ##

* Wouter van Ooijen for Wisp628, Wisp648, source material and docs.
* Jan-Erik Soderholm for supplying the XWisp2 code for Windows.
* Chen Xiao Fan and Daniel Serpell for help with the Linux version.
* Ezra Buehler for help with the MacOS version.
* Gabe Gindele for support of the FTDI API for the FTDI232 chip.
* Ronald Gilman for the re-establishment of Passthrough AUXI in Wisp628.
* All other people who tested XWisp2 and reported results.


## Contents of the package ##
Xwisp2 is multiplatform: there are separate executables for eComStation
(OS/2), Linux and Windows. The configuration files are common for all
platforms. For every platform there is also a utility to list the PIC
specifications in xwisp2.cfg, which also shows all device parameters,
including defaults.

* README.md     - this document
* changes.txt   - summary of changes, version history
* xwisp2.cfg    - PIC specifications base file, annotated, all platforms
* xwisp2_12.cfg - Specifications for 12-bits PICmicros, all platforms
* xwisp2_14.cfg - Specifications for 14-bits PICmicros, all platforms
* xwisp2_16.cfg - Specifications for 16-bits PICmicros, all platforms
* xwisp2.html   - Commandline reference for xwisp2 in html-format
* xwisp2.exe    - Executable for eComStation, ArcaOS (OS/2)
* xwisp2        - same, Linux executable
* xwisp2w.exe   - same, Windows executable
* xwlist.exe    - PIC specifications display utility for eComStation (OS/2)
* xwlist        - same, Linux executable
* xwlistw.exe   - same, Windows executable

In addition all source files and make files for OS/2, Linux and Windows are provided.
See below for instructions for compiling from source.


## Installation ##
When re-installing over an older version the old files may be replaced.

### eComstation (OS/2) ###
* Create a directory for xwisp2, for example [bootdrive]:\PROGRAMS\XWISP2.
  Use WarpIn to install XWisp2 in your directory of choice.
  There are 2 packages: executables, and source material (optional
  install). For execution you need only xwisp2.exe, xwlist.exe and the
  xwisp2_??.cfg files, you may erase other files.
  Make sure the programs directory is in your PATH environment variable.
* If you prefer you may put the xwisp2_??.cfg files in another directory.
  Preferrably the path of this directory is in your PATH or DPATH
  environment variable, but Xwisp2 searches also for these files in
  [bootdrive]:\PROGRAMS and [bootdrive]:\PROGRAMS\XWISP2
* Start a command-prompt session and go to the directory with your hex file(s).
* Enter 'xwisp2' followed by the required arguments, see users guide.
  <br>When no arguments are specified, some help screens will appear.

### Linux ###
* Extract the ZIP-file in a (temporary) directory of your choice.
  For execution you need only xwisp2, xwlist and the xwisp2_??.cfg
  files, you may erase other files.
  Move xwisp2u and xwlist to /usr/local/bin and set the authorisation
  flags. Move the xwisp2_??.cfg files to /usr/local/bin or /etc or
  /etc/xwisp2. For example as follows:
    * cp xwisp2 /usr/local/bin
    * cp xwlist /usr/local/bin
    * md /etc/xwisp2                { if it does not exist yet }
    * cp xwisp2*.cfg /etc/xwisp2
    * chmod u+s /usr/local/bin/xwisp2
    * chmod u+s /usr/local/bin/xwlist
  <br>These commands may require root or super user privilege.
* You may have to alter the attributes of your serial port,
  for example with:
   * chmod 666 /dev/ttyS0    (may require root privilege).
* Start a terminal session and go to the directory with your hex file(s).
* Enter 'xwisp2' followed by the required arguments, see user guide.
  When no arguments are given, some help screens will appear.
* You may want to change Settings Scheme to 'White on Black' for a
  better view of the help screens.


### MacOS and FreeBSD ###
* No executables for MacOS and FreeBSD are delivered with this package.
  MacOS and FreeBSD users will have to download the source package and
  build xwisp2 for themselves.
  The makefile xwisp2_gu.mak can be used for (GNU-)make.
* Other directives are like those for Linux.

### Windows ###
* Create a directory for xwisp2, for example under "c:\Program Files\".
  Alternatively select an existing programs directory which path is in
  your Path environment variable.
* UnZip the ZIP-file in your directory of choice.
  For execution you need only xwisp2w.exe or xwisp2wf.exe, xwlistw.exe
  and the xwisp2_??.Cfg files, you may erase other files.
* You may prefer to move the xwisp2_??.cfg data files to a separate data
  directory, for example "c:\Program Files\Common Files\xwisp2".
* Open a ('MS-DOS') command-prompt window and go to the directory with
  your hex file(s).
* Enter 'xwisp2w' followed by the required arguments, see users guide.
  When no arguments are given, some help screens will appear.
  When you use a USB serial port converter with FTDI chip and have the
  FTDI API installed, you may prefer to use xwisp2wf.exe instead.

### 64-bits environment ###
* I have received successful results of compilation (with GCC) and
  execution of XWisp2 with a 64-bits Linux Operating System.


## Supported and unsupported commandline arguments ##
Xwisp2 supports a large subset of the commands supported by the original
Xwisp, but it has also some other commands. See the file xwisp2.html for
a Users Guide and Command Reference.


## Compatibility of Wisp628 and Wisp648 firmware ##
XWisp2 was originally designed for Wisp628 and worked with all firmware
versions of it. The Wisp648 firmware introduced some compatibility
issues: newer PICs are handled somewhat differently than with Wisp628
firmware versions above 1.09. From Xwisp2 version 2.0.0 only the
Wisp648 with firmware 1.28 or newer is supported.


## Upgrading firmware of the Wisp648 programmer ##
From time to time a new version of the Wisp648 firmware becomes
available.
There are several ways to upgrade your Wisp648:
* Probably the most easy and reliable way is to use an extra PIC
  16F648A PIC on a test board. Load the new firmware to the extra
  16F648A and replace the old 16F648A with the newly loaded one.
  Keep the old 16F648A in case something went wrong with the new
  firmware or when you blow up the PIC in the Wisp648 programmer.
  Of course any other type of programmer can be used as well!
* When you don't have a spare 16F648A, but happen to have a 16F628[A]
  then approximately the same procedure can be followed, the 16F628[A]
  works fine in the Wisp648!
  * Download an older version of Xwisp2 (1.9.2).
  * Load the 16F628[A] with Wisp628 firmware (Wisp628_112.hex)
  * Replace the 16F648A of the Wisp648 with the newly loaded 16F628A.
  * Load the 16F648A with the new Wisp648 firmware by using
    Xwisp2 version 1.9.2 (and the Wisp648 with 16f628[A]).
  * Replace the 16F628[A] on the Wisp648 by the newly loaded 16F648A.
<br>Note: Older versions of Xwisp2 and Wisp628 firmware can be found on my site.


## Restrictions, limitations, issues, hints and tips ##
* The 12-bit PIC family is not yet supported by Xwisp2
  (all 10Fxxx, some 12Fxxx and 16Fxxx).


## Compiling XWisp2 ##

The source package contains a number of make-files with naming convention:
```
  makefile.ct          change the 'ct' of the extension into:

  'c' compiler         'w' for Watcom Open C/C++
                       'g' for GCC

  't' target platform: '2' for eComStation (OS/2)
                       'u' for Linux and alike (FreeBSD, MacOS)
                       'w' for Windows (W98 and newer)
```
The file 'makefile.wx' is a common part, included by the makefiles above.

You may have to adapt the makefile(s) to your own situation, especially
when using the Watcom compiler: I use no 'include' environment variable but
specify the include path in the makefile. Only the base directory of the
compiler (specified with the variable BASEDIR) may need to be modified
in makefile.wx.

<br>Only the makefiles for OpenWatcom C/C++ version 1.9 are tested with Xwisp2 2.0.5!


## Summary of changes ##
See file 'changes.txt'.


## Comments and Problem reports ##
***XWisp2 is not developed and supported anymore!***

Comments and suggestions for improvements are welcome.
When you have a problem to report, please describe as accurately as
possible the symptoms, configuration of the target device, etc.
Use the Log and Verbose command to collect progress information.


Rob Hamerling
