
// ============================================================
// XWisp2Com.c - Wisp648 Programmer support
//
//             - basic communications I/O functions
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ============================================================

// #define __DEBUG__

#include "xwisp2.h"                             // all common includes

//  locally used constants

static const ULONG  aulWispSpeed[5] = {9600, 19200, 38400, 57600, 115200};
static const char   szProgrammer[]  = "Wisp648";            // supported programmer

//  locally used common variables

static  HSERIALPORT  hSerialPort  = INVALIDSERIALHANDLE;    // port not open
static  ULONG        ulBpsOrg = 0;                          // speed at open

//  local function prototypes

static  int     CheckInputData(char *, char *);
static  int     CheckOutputData(char *);
static  void    PortClose(void);
static  BOOLEAN PortSetup(char *);
static  int     SendBreak(void);
static  int     SendReceiveFast(char *, char *);
static  int     SetBps(ULONG);
static  int     SetMode(void);
static  int     SetReadTimeout(ULONG);


// ------------------------------------------------------
//
// Check received data for valid characters
//
// remarks: - strips hit bit and check for command error
//
// ------------------------------------------------------
static  int    CheckInputData(char *szOutput,          // sent data
                              char * szInput) {        // received data

  int  i;                                               // counter

  for (i=0; i<strlen(szInput); i++) {                   // scan response
    szInput[i] &= ~0x80;                                // strip high bit
    if (szOutput[i] >= 'g' && szOutput[i] <= 'z' &&     // command byte
        szInput[i] == '?') {                            // command failure
      if (!bQuiet)
        printf("Wbus command failure\n");
      return  XW_IOERROR;                               // programmer problem
      }
    }
  return 0;
  }


// ---------------------------------------------------
//
// Check outgoing data for valid characters
//
// ---------------------------------------------------
static  int    CheckOutputData(char *szOutput) {

  int    i;

  for (i=0; i<strlen(szOutput); i++) {
    if ( !( (szOutput[i] >= '0'  &&  szOutput[i] <= '9') ||
            (szOutput[i] >= 'a'  &&  szOutput[i] <= 'z') ) ) {
      if (!bQuiet)
        printf("String '%s' contains invalid characters\n", szOutput);
      return  XW_IOERROR;                               // invalid data
      }
    }
  return XW_NOERROR;                                    // OK
  }


// ---------------------------------------------------
//
// Close COM port
//
// Remarks - state of programmer not changed
//
// ---------------------------------------------------
static void  PortClose(void) {

  if (hSerialPort != INVALIDSERIALHANDLE) {             // currently open
    if (ulWispState != WISPPASSTHROUGH) {               // not in passthrough mode!
//    WbusPassThrough((1<<4) + 4);                      // ) back to ..
//    SetBps(ulWbusBaudrateDefault);                    // ) .. Wbus default
      SetModemOutputStatus(FALSE,FALSE);                // drop DTR and RTS
      }
    OS_SerialClose(hSerialPort);                        // terminate comm.
    hSerialPort = INVALIDSERIALHANDLE;                  // reset handle
    }
  return;
  }


// ---------------------------------------------------
//
// Open and initialise port
//
// Input   - Name of device to open
//
// Output  - nothing
//           (handle of open port stored in local variable)
//
// Returns TRUE when port opened successfully, otherwise FALSE.
//
// Remarks: - if port is open it will be closed first
//          - DTR and RTS will be set false
//
// ---------------------------------------------------
static  BOOLEAN  PortSetup(char *szDeviceName) {

  int  rc;                                              // reeturncode

  if (hSerialPort != INVALIDSERIALHANDLE) {             // currently open
    if (!bQuiet)
      printf("%cWNG: Re-opening an already open port!\n", 7);
    PortClose();                                        // close it
    }

  hSerialPort = OS_SerialOpen(szDeviceName);            // open device
  if (hSerialPort == INVALIDSERIALHANDLE) {             // failure
    if (!bQuiet)
      printf("Failure opening serial port '%s', errno = %d\n",
              szDeviceName, errno);
    return FALSE;
    }

  if (bVerbose  &&  !bQuiet)
    printf("Initializing serial port: %s\n", szPortName);

//if (isatty(hSerialPort) == 0) {                       // not a char device
//  if (!bQuiet)
//    printf("'%s' seems not to be a serial port!\n", szDeviceName);
//  return FALSE;
//  }

                                                        // save original bps
  rc = OS_SerialGetBaudrate(hSerialPort, &ulBpsOrg, NULL, NULL);
  if (rc != XW_NOERROR) {
    if (!bQuiet)
      printf("Error querying baud rate, rc %d, errno %d\n", rc, errno);
    return rc;
    }

  rc = SetBps(ulWbusBaudrateDefault);                   // default for Wbus
  if (rc) {                                             // problem
    if (!bQuiet)
      printf("Setting initial serial port speed failed, rc %d\n", rc);
    return FALSE;
    }

  rc = SetMode();                                       // set 8,n,1, etc
  if (rc) {                                             // problem
    if (!bQuiet)
      printf("Error setting serial port mode, rc %d\n", rc);
    return FALSE;
    }

  rc = SetReadTimeout(300);                             // set timeout
  if (rc) {                                             // problem
    if (!bQuiet)
      printf("Error setting serial port initial read timeout, rc %d\n", rc);
    return FALSE;
    }

  SetModemOutputStatus(TRUE, bRTSstate);                // set DTR, leave RTS asis
  OS_Sleep(90);
  SetModemOutputStatus(FALSE, bRTSstate);               // reset DTR, leave RTS asis
  OS_Sleep(90);

  rc = SetModemOutputStatus(bDTRstate, bRTSstate);      // (re)set DTR and RTS
  if (rc)
    return FALSE;

  return TRUE;                                          // result
  }


// --------------------------------------------------
//
//  ProgrammerActivate - prepare for programming
//
//  Remarks: - only programmer activated
//           - some operational programming variables adapted to
//             properties of programmer type and firmware version
//
// --------------------------------------------------
extern  int    ProgrammerActivate(void) {

  int     i;                                            // counter
  int     rc;
  ULONG   ulPassSpeedCode;
  ULONG   ulVersion;
  ULONG   ulBpsCur, ulBpsMin, ulBpsMax;                 // baudrates

  if (ulWispState != WISPACTIVE) {                      // not already active

    if (hSerialPort == INVALIDSERIALHANDLE) {           // port not open
      if (PortSetup(szPortName) == FALSE)               // open and init port
        return XW_NOENT;                                // open failed
      }
    for (i=0; i < 3; i++) {                             // retries to activate programmer
                                                        // assume state is 'attention'
      rc = WbusHello();                                 // attention -> active
      if (rc == XW_NOERROR  && ulWispState == WISPACTIVE)
        break;                                          // activated successfully

      if (i >= 2)  {                                    // after 3rd try still not active
        if (!bQuiet)
          printf("Failed to activate Programmer.\n"
                 "Check connection to target and serial port (%s).\n", szPortName);
        return XW_NOENT;                                // give up!
        }

      if (bVerbose  &&  !bQuiet)
        printf("Programmer not active, trying to wake up\n");
      SendBreak();                                      // try to reset Wisp648

      }

    szWbusDevice = WbusType();                          // get programmer type
    if (szWbusDevice != NULL) {                         // name obtained
      if (stricmp(szWbusDevice, szProgrammer)) {        // check
        if (!bQuiet)
          printf("Unsupported programmer '%s', %s supports only %s\n",
                  szWbusDevice, szProgName, szProgrammer);
        return XW_NOENT;                                // invalid device
        }
      }
    else                                                // no name obtained
      return XW_NOENT;                                  // invalid device

    ulVersion = WbusVersion();                          // get firmware level
    if (!bQuiet)
      printf("Detected programmer: %s, firmware version %lu.%02lu\n",
                     szWbusDevice,
                     ulVersion / 100, ulVersion % 100);

    ulWispLevel = ulVersion;                            // level dependencies

    if (ulWispLevel < 128) {                            // low level firmware
      OS_Beep(1000, 50);                                // error
      if (!bQuiet) {
        printf("This version of Xwisp2 requires %s with firmware 1.28 or newer\n",
                szProgrammer);
        printf("For a Wisp628 or a Wisp648 with older firmware"
               " use an older version of Xwisp2\n");
        }
      return XW_NOENT;                                  // not supported
      }

    AdjustAlgorithm(ulWispLevel);                       // adjust Pgm.Alg.

    fBurstWrite |= BURSTWRITE_SUPP;                     // supported!
    SetReadTimeout(2000);                               // for 'far' jumps
    ulWbusBaudrateActual = aulWispSpeed[4];             // max speed

    if (ulWbusBaudrateUser != 0)                        // user override
      ulWbusBaudrateActual = ulWbusBaudrateUser;        // use user speed

    rc = OS_SerialGetBaudrate(hSerialPort, &ulBpsCur, &ulBpsMin, &ulBpsMax);
    if (rc != XW_NOERROR) {
      if (!bQuiet)
        printf("Error querying baud rate, rc %d, errno %d\n", rc, errno);
      return rc;
      }
    if (ulWbusBaudrateActual > ulBpsMax) {              // too high for port
      if (bVerbose  &&  !bQuiet)
        printf("WNG: Maximum serial port speed %lu bps\n", ulBpsMax);
      ulWbusBaudrateActual = ulBpsMax;                  // port upper limit
      }

    if (ulWbusBaudrateActual != ulWbusBaudrateDefault) {   // change req'd
      ulPassSpeedCode = SpeedIndex(ulWbusBaudrateActual);
      if (ulPassSpeedCode != MINUS1) {                  // OK
        if (bVerbose  &&  !bQuiet)
          printf("Switching communications speed from %lu to %lu bps\n",
                          ulWbusBaudrateDefault, ulWbusBaudrateActual);
        WbusPassThrough((ulPassSpeedCode<<4) + 4);      // speed change cmd
        OS_Sleep(20);                                   // delay in Wisp648
        OS_SerialFlush(hSerialPort);                    // flush I/O buffers
        rc = SetBps(aulWispSpeed[ulPassSpeedCode]);     // to new speed
        if (rc)
          return rc;
        }
      else if (!bQuiet)
        printf("Speed %lu not supported, using %lu\n",
               ulWbusBaudrateActual, ulWbusBaudrateDefault);
      }
    }

  return  XW_NOERROR;                                   // all OK
  }


// -------------------------------------------------------------
//
//  ProgrammerDeactivate - terminate programming
//
// -------------------------------------------------------------
extern  void  ProgrammerDeactivate(void) {

  if (hSerialPort != INVALIDSERIALHANDLE) {             // currently open
    PortClose();                                        // close port
    }
  return;
  }


// ------------------------------------------------------
//
// Send RS232 break signal of Wisp648 required duration
//
// remarks: port must be opened before.
//
// ------------------------------------------------------
static  int  SendBreak(void) {

  int  rc;                                              // returncode

  rc = OS_SerialBreak(hSerialPort, 200);                // 200 ms RS232 break
  if (rc) {
    if (!bQuiet)
      printf("Failure sending serial 'break' signal, rc=%d\n", rc);
    return rc;
    }
  LogMsg("{break}");

  ulWispState = WISPATTENTION;                          // Wisp648 state

  rc = SetBps(ulWbusBaudrateDefault);                   // back to default

  return rc;
  }


// ---------------------------------------------------
//
// SendByte  - send a single byte, unchecked
//           - discard the echo
//
// Remarks: only used by WbusPassThrough command
//
// ---------------------------------------------------
extern  int    SendByte(char  ucChar) {

  LONG   slBytesWritten;

  slBytesWritten = OS_SerialWrite(hSerialPort, &ucChar, 1);   // write byte
  if (slBytesWritten == -1) {                           // error
    if (!bQuiet)
      printf("SendByte write error, errno = %d\n", errno);
    return errno;                                       // write error
    }
  if (slBytesWritten != 1) {                            // nothing written
    if (!bQuiet)
      printf("SendByte write failed, bytes written: %lu\n", slBytesWritten);
    return XW_IOERROR;
    }
  OS_Sleep(5);                                          // quiesce input
  OS_SerialFlush(hSerialPort);                          // flush input/output
  return XW_NOERROR;
  }


// ---------------------------------------------------
//
// SendReceive - send a string of data
//             - return result code
//
//
// When fBurstWrite supported and not half duplex requested the fast
// write will be used, otherwise slow write (true byte-by-byte Wbus
// protocol) will be used.
//
// ---------------------------------------------------
extern  int    SendReceive(char *szOutput,
                           char *szInput) {

  *szInput = '\0';                                      // nul return string

  if (fBurstWrite == (BURSTWRITE_FDX | BURSTWRITE_SUPP))  // burst writes OK
    return SendReceiveFast(szOutput, szInput);          // burst-write
  else
    return SendReceiveSlow(szOutput, szInput);          // byte-by-byte write
  }


// --------------------------------------------------------------------
//
// SendReceiceFast - send a string of data in one burst
//                   return the echo
//
// --------------------------------------------------------------------
static  int    SendReceiveFast(char *szOutput,
                               char *szInput) {

  int    rc;
  LONG   slBytesReceived, slBytesWritten, slBytesBuffered;
  ULONG  slEchoLength;
  char   ucBuffer[132];                                 // local rcv buffer

  szInput[0] = '\0';                                    // null reply
  rc = CheckOutputData(szOutput);                       // check command data
  if (rc != XW_NOERROR)
    return rc;

  slBytesWritten = OS_SerialWrite(hSerialPort,
                                  szOutput,
                                  strlen(szOutput));
  if (slBytesWritten == -1) {                           // error
    if (!bQuiet)
      printf("SendReceiveFast write error, errno = %d\n", errno);
    return errno;                                       // write error
    }
  if (slBytesWritten < strlen(szOutput)) {              // partial write
    if (!bQuiet)
      printf("SendReceiveFast write error, bytes written: %ld of %u\n",
                    slBytesWritten, strlen(szOutput));
    return XW_IOERROR;                                  // problem
    }

  slBytesBuffered = 0;                                  // nothing received yet
  slEchoLength = strlen(szOutput);
  while (slBytesBuffered < slEchoLength  &&  rc == XW_NOERROR) {  // echo ..
    slBytesReceived = OS_SerialRead(hSerialPort,
                                    ucBuffer + slBytesBuffered,
                                    sizeof(ucBuffer) - slBytesBuffered - 1);
    if (slBytesReceived == -1) {                        // error
      if (!bQuiet)
        printf("SendReceiveFast read error, errno = %d\n", errno);
      rc = errno;                                       // read error
      }
    else if (slBytesReceived == 0) {                    // no more data
      if (!bQuiet)
        printf("SendReceiveFast read timeout, received %ld of %ld bytes\n",
                      slBytesBuffered, slEchoLength);
      rc = XW_IOERROR;                                  // no problem
      }
    else                                                // data received
      slBytesBuffered += slBytesReceived;
    }
  ucBuffer[slBytesBuffered] = '\0';                     // end of string

  if (rc == XW_NOERROR) {                               // something received
    rc = CheckInputData(szOutput, ucBuffer);            // check response data
    if (rc == XW_NOERROR)                               // OK
      strcpy(szInput, ucBuffer);                        // copy to caller
    }

  return rc;                                            // result
  }


// ---------------------------------------------------
//
// SendReceive - send a string of data byte-by-byte
//             - return the echo
//
// ---------------------------------------------------
extern  int    SendReceiveSlow(char *szOutput,
                               char *szInput) {

  int    rc;                                            // result
  LONG   slBytesWritten, slBytesReceived;
  int    i;                                             // counter(s)
  char   ucBuffer[132];                                 // local rcv buffer

  *szInput = '\0';                                      // null reply
  rc = CheckOutputData(szOutput);                       // check data
  if (rc != XW_NOERROR)
    return rc;

  for (i=0; i<strlen(szOutput) && rc==XW_NOERROR; i++) {
    slBytesWritten = OS_SerialWrite(hSerialPort,
                                    szOutput + i,
                                    1);
    if (slBytesWritten == -1) {                         // error
      if (bVerbose  &&  !bQuiet)
        printf("SendReceiveSlow write error, errno = %d\n", errno);
      rc = errno;
      break;
      }
    if (slBytesWritten != 1) {                          // error
      if (bVerbose  &&  !bQuiet)
        printf("SendReceiveSlow write error, bytes written: %ld of 1\n",
                       slBytesWritten);
      rc = XW_IOERROR;
      break;
      }

#ifdef __DEBUG__
    printf("Awaiting reply .... ");
    fflush(stdout);
#endif
    slBytesReceived = OS_SerialRead(hSerialPort,
                                    ucBuffer + i,
                                    sizeof(ucBuffer) - i - 1);
#ifdef __DEBUG__
    printf("received: %d bytes\n", slBytesReceived);
#endif
    if (slBytesReceived == -1) {                        // error
      if (bVerbose  &&  !bQuiet)
        printf("SendReceiveSlow read error, errno = %d\n", errno);
      rc = errno;                                       // read error
      break;
      }
    else if (slBytesReceived == 0) {                    // no data arrived
      if (bVerbose  &&  !bQuiet)
        printf("SendReceiveSlow read timeout, 0 bytes received\n");
      rc = XW_IOERROR;                                  // no problem
      }
    else if (slBytesReceived == 1)                      // 1 byte expected
      rc = XW_NOERROR;                                  // OK
    else {                                              // more than expected
      if (bVerbose  &&  !bQuiet)
        printf("More bytes received (%ld) than expected!\n", slBytesReceived);
      rc = XW_NOERROR;                                  // no problem
      }
    }
  ucBuffer[i] = '\0';                                   // end of string

  if (rc == XW_NOERROR) {                               // something received
    rc = CheckInputData(szOutput, ucBuffer);            // check input
    if (rc == XW_NOERROR)                               // OK
      strcpy(szInput, ucBuffer);                        // return to caller
    }

  return rc;
  }


// ---------------------------------------------------
//
// Set port speed
//
// Input   - New baudrate
//
// Output  - nothing
//
// Returns - zero if OK
//         - not zero: failure, see generated message
//
// ---------------------------------------------------
static  int    SetBps(ULONG ulBpsNew) {

  static  ULONG  ulPortSpeed[] = {1200, 2400, 4800, 9600, 16457, 19200,
                                        28800, 38400, 57600, 115200};

  ULONG   i;
  int     rc;
  ULONG   ulBpsCur, ulBpsMin, ulBpsMax;                 // baudrates

  for (i=0; i<sizeof(ulPortSpeed)/sizeof(ulPortSpeed[0]); i++) {
    if (ulBpsNew == ulPortSpeed[i])
      break;
    }
  if (i >= sizeof(ulPortSpeed)/sizeof(ulPortSpeed[0])) {
    if (!bQuiet)
      printf("Speed %lu not supported\n", ulBpsNew);
    return XW_INVAL;
    }

  rc = OS_SerialGetBaudrate(hSerialPort, &ulBpsCur, &ulBpsMin, &ulBpsMax);
  if (rc != XW_NOERROR) {
    if (!bQuiet)
      printf("Error querying baud rate, rc %d, errno %d\n", rc, errno);
    return rc;
    }

  if (bVerbose  &&  !bQuiet)
    printf("Portspeed min: %lu, max: %lu, cur: %lu\n",
            ulBpsMin, ulBpsMax, ulBpsCur);

  if (ulBpsNew > ulBpsMax) {
    if (!bQuiet)
      printf("Requested speed (%lu) exceeds maximum for port, using %lu bps\n",
              ulBpsNew, ulBpsMax);
    ulBpsNew = ulBpsMax;                                // replace by max
    }

  if (ulBpsNew != ulBpsCur) {                           // change needed
    rc = OS_SerialSetBaudrate(hSerialPort, ulBpsNew);
    if (rc) {
      if (!bQuiet)
        printf("SetBaudrate() error, rc %d\n", rc);
      return rc;
      }

#ifdef __DEBUG__
    OS_SerialGetBaudrate(hSerialPort, &ulBpsCur, NULL, NULL);
    if (ulBpsCur != ulBpsNew)
      printf("Failed to set baudrate to %lu, actual baudrate %lu\n",
              ulBpsNew, ulBpsCur);
#endif

    if (bVerbose  &&  !bQuiet)
      printf("Portspeed new: %lu\n", ulBpsNew);

    }

  return rc;
  }


// ---------------------------------------------------
//
//  SetMode():
//
//  Input   - nothing, communications parameters fixed
//
//  Output  - nothing
//
//  Returns - result of OS_SerialSetMode()
//
//  Remarks: - See function OS_SetMode() in Xwisp2OS.c for details
//           - No action if port not open
//
// ---------------------------------------------------
static  int   SetMode(void) {

  int   rc = 0;                                         // return code

  if (hSerialPort != INVALIDSERIALHANDLE) {             // port is open
    rc = OS_SerialSetMode(hSerialPort);                 // set characteristics
    if (rc != 0  && !bQuiet)
      printf("Error setting communications mode"
             " (Data-, Parity-, Stop-bits), rc %d\n",
              rc);
    }
  return  rc;
  }


// ---------------------------------------------------
//
// Set modem output status - set new DTR and RTS setting
//
// input: bitpattern for 'ON' signal(s):  bit 0 for DTR, bit 1 for RTS
//
// output: result of IOCTL set modem output status.
//
// remarks: - assumes the COMport is open!
//          - when a bit in the bitpattern is not 'ON'
//            corresponding signal will be put OFF!
//          - New DTR and/or RTS only set when port open
//
// ---------------------------------------------------
extern  int   SetModemOutputStatus(BOOLEAN bDTR, BOOLEAN bRTS) {

  int     rc;

  bDTRstate = bDTR;                                     // remember
  bRTSstate = bRTS;

  if (hSerialPort != INVALIDSERIALHANDLE) {             // port is open
    rc = OS_SerialSetModemOutputStatus(hSerialPort, bDTRstate, bRTSstate);
    if (rc != 0  &&  !bQuiet)
      printf("SetModemOutputStatus failed, rc %d\n", rc);

    if (bVerbose  &&  !bQuiet)
      printf("DTR %s, RTS %s\n",
              (bDTRstate) ? "On" : "Off",
              (bRTSstate) ? "On" : "Off");
    }

  return rc;
  }


// ---------------------------------------------------
//
//  SetReadTimeout():
//
//  Input   - desired read timeout value in milliseconds
//
//  Output  - nothing
//
//  Returns - result of OS_SerialSetReadTimeout()
//
//  Remarks: - See function OS_SetReadTimeout() in Xwisp2OS.c for details
//           - No action if port not open
//
// ---------------------------------------------------
static  int   SetReadTimeout(ULONG  ulReadTimeout) {

  int   rc = 0;                                         // return code

  if (hSerialPort != INVALIDSERIALHANDLE) {             // port is open
    rc = OS_SerialSetReadTimeout(hSerialPort, ulReadTimeout);
    if (rc != 0  &&  !bQuiet)
      printf("Error setting read timeout value, rc %d\n", rc);
    }
  return  rc;
  }


// ------------------------------------------------
//  Convert bps value to index for passthrough command
//  (value of 'x' in '00x4p')
//
//  Input   - speed in bps
//
//  Output  - nothing
//
//  Returns - index in table of supported speeds (should be in range 0..4)
//          - MINUS1 if speed not supported
//
// ------------------------------------------------
extern  int  SpeedIndex(ULONG ulBps) {

  int  i;                                               // counter(s)

  for (i=0; i<sizeof(aulWispSpeed)/sizeof(ULONG); i++) {
    if (ulBps == aulWispSpeed[i])
      return i;
    }
  return  MINUS1;
  }
