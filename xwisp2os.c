
// ===================================================================
//
// XWISP2OS.c - Operating System dependent functions, such as:
//              + beep, sleep, time
//              + serial port open, read, write, close and control
//                (control of baudrate, modem control, line control)
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// Functions do not generate error messages (except in __DEBUG__ mode), only
// a returncode (0=OK, otherwise error), or a numeric return value.
//
// Functions will generally contain OS_independent and OS-specific code
// a separate piece of code for each of the supported OS-es. The
// OS-dependent code is automatically selected at precompile time with the
// use of a unique OS-specific precompiler predefined macro.
//
// Currently implemented:
//  __OS2__        eComStation
//  __LINUX__      Generic for Linux, MacOS and FreeBSD
//                 __APPLE__  and __FREEBSD__  may require special treatment
//  __W32__        32-bits Windows (W95, W98, W2K, ME, NT, XP, Vista)
//
// Planned:
//  - nothing
//
// Note 1: The code has been compiled with Open Watcom C compiler
//         for all targets using different target specifications.
//         Also compilable with GCC (tested eComStation and Linux
//         versions 3.3.5)
//
// Note 2: For Windows (maybe later also Linux) some code supports the
//         FTDI API for a Wisp648 equiped with FTDI232 chip (USB serial
//         converter).  The macro FTDIUSB needs to defined to
//         activate this code (done in the special make file).
//         This will include also "ftd2xx.h" and needs ftd2xx.lib in the
//         link step (see file xwisp2_wf.mak).
//         Contribution by Gabe Gindele.
//
//
// =======================================================================
//
// Skeleton of an OS-dependent function:
//  -------------------------------------------
//  extern  int  OS_Function(...) {
//
//  int rc;                                     // returncode
//  other common OS-independent variable declarations
//
//  common OS-independent code
//
//  #if defined(__OSxxxx__)
//    OSxxxx specific declarations and code
//  #elif defined(__OSyyyy__)
//    OSyyyy specific decalarations and code
//  #elif defined(__OSzzzz__)
//    OSzzzz specific decalarations and code
//  #endif
//
//  common OS-independent code
//
//  return rc;
//  }
//
// ===================================================================

// #define __DEBUG__

#include "xwisp2.h"                                     // common includes

#if defined(__EMX__) && !defined(__OS2__)               // EMX/GCC environment
  #define __OS2__                                       // implies OS/2
#endif
#if defined(__OS2__)                                    // OS/2 target
  #define  INCL_BASE
  #define  INCL_DOSDEVIOCTL
  #define  INCL_DOSFILEMGR
  #define  INCL_DOSMISC
  #define  INCL_NOPM
  #include <os2.h>                                      // OS/2 Warp toolkit

#elif defined(__LINUX__)                                // Linux target

//#include <i86.h>
  #include <termios.h>
  #include <unistd.h>
  #include <sys/ioctl.h>
  #include <sys/time.h>

#elif defined(__W32__)                                  // Windows target

  #if defined(FTDIUSB)                                  // (in xwisp2_wf.mak)
    #include "FTD2XX.h"                                 // FTDI API header
  #endif

#endif


// ====================================================================

// Common OS-independent variables, structures, etc.

static const ULONG aulBaudrate[] = {     0,     50,     75,    110,    134,
                                       150,    200,    300,    600,   1200,
                                      1800,   2400,   4800,   9600,  19200,
                                     38400,  57600, 115200, 230400, 460800,
                                    500000, 576000, 921600,1000000,1152000,
                                   1500000,2000000,2500000,3000000,3500000,
                                   4000000 };
extern ULONG  ulPortIndex = 0;                          // serial port index number


// Common OS-dependent variables, structures, etc.

#if defined(__OS2__)                                    // OS/2 target

  char  *szPortName = "COM1";                           // default serial port
  typedef struct _xbps    {ULONG  bps;                  // extended bps
                           char   frac;                 // fraction
                           } XBPS;
  typedef struct _axbps   {XBPS   cur;                  // current bps
                           XBPS   min;                  // port's minimum
                           XBPS   max;                  // port's maximum
                           } AXBPS;

#elif defined(__LINUX__)                                // Linux target

  #if defined(__FREEBSD__)
    char  *szPortName = "/dev/ttyd0";                   // default for FreeBSD
  #else
    char  *szPortName = "/dev/ttyS0";                   // default Linux, MacOS
  #endif

#elif defined(__W32__)                                  // Windows target

  #if defined(FTDIUSB)                                  // (in xwisp2_wf.mak)
    #define FTDIWRAP(FUNCTION) FT_W32_##FUNCTION        // function mapping
    typedef FT_HANDLE  HSERIALPORT;                     // map handle
  #else                                                 // no FTDI
    #define FTDIWRAP(FUNCTION) FUNCTION                 // no mapping!
  #endif

    char  *szPortName = "\\\\.\\COM1";                  // default serial port

#endif


// =====================================================================
//
//  OS_Beep - Give sound of some duration
//
//  Input   - Frequency of beep in Hz
//          - Duration of beep in number of milliseconds
//
//  Output  - Nothing
//
//  Returns returncode of OS function call
//
// =====================================================================
extern  int  OS_Beep(ULONG  ulFrequency,
                     ULONG  ulMilliSeconds) {

  int  rc = MINUS1;                                     // default: problem

#if defined(__OS2__)

  rc = DosBeep(ulFrequency, ulMilliSeconds);            // beep some time

#elif defined(__LINUX__)                                // Linux target

//sound(ulFrequency);                                   // speaker on
  rc = OS_Sleep(ulMilliSeconds);                        // duration
//rc = nosound();                                       // speaker off
                                                        // (missing in Watcom)
#elif defined(__W32__)                                  // Windows target

  if (Beep(ulFrequency, ulMilliSeconds))                // beep some time
    rc = 0;                                             // OK

#endif

  return rc;
  }


// =====================================================================
//
//  OS_FileSearch - Search a (configuration) file
//
//  Input   - filespec
//          - return buffer for pathspec (if file found)
//          - command string from command line
//
//  Output  - complete pathspec of searched file
//
//  Returns -   0 if file found
//          -  -1 if file not found
//
//
// =====================================================================
extern  int  OS_FileSearch(char *szFileSpec,
                           char *szPathSpec,
                           char *szCommandSpec) {

  int         rc;                                       // return code
  int         i;                                        // index
  FILE       *pFile;                                    // file pointer
  char       *pucPos;                                   // string pointer
  char        szTempPath[260];                          // pathspec buffer
#if defined(__OS2__)
  ULONG       ulBootDrive;                              // drive index
#endif

  char       *szXWisp2Path[] = {                        // places to search
                                                        // .cfg files
#if defined(__OS2__)
                           "c:\\PROGRAMS\\",
                           "c:\\PROGRAMS\\XWISP2\\",
#elif defined(__LINUX__)
                           "/etc/",
                           "/etc/xwisp2/",
                           "/usr/local/bin/",
                           "/usr/local/share/xwisp2",
#elif defined(__W32__)
                           "c:\\Program Files\\XWISP2\\",
                           "c:\\Program Files\\Common Files\\XWISP2\\",
#endif
                           NULL                         // end of array
                           };

  rc = MINUS1;                                          // default: problem
  *szPathSpec = '\0';                                   // default: empty path

  pFile = fopen(szFileSpec, "r");                       // try current dir
  if (pFile != NULL) {                                  // found!
    fclose(pFile);                                      // done (empty path)
    return XW_NOERROR;                                  // report OK
    }

#if defined(__OS2__)                                    // eCS
  _searchenv(szFileSpec, "PATH", szPathSpec);           // PATH environment
  if (strlen(szPathSpec) > 0)                           // path found
    return XW_NOERROR;                                  // report OK
  _searchenv(szFileSpec, "DPATH", szPathSpec);          // data path
  if (strlen(szPathSpec) > 0)                           // file found
    return XW_NOERROR;                                  // report OK
  DosQuerySysInfo(QSV_BOOT_DRIVE, QSV_BOOT_DRIVE,
                  &ulBootDrive, sizeof(ulBootDrive));
  for (i=0; szXWisp2Path[i] != NULL; i++)               // listed paths
    szXWisp2Path[i][0] = 'A' + ulBootDrive - 1;         // modify drive letter

#elif defined(__LINUX__)                                // Linux
  _searchenv(szFileSpec, "PATH", szPathSpec);           // PATH environment
  if (strlen(szPathSpec) > 0)                           // path found
    return XW_NOERROR;                                  // report OK

#elif defined(__W32__)                                  // Windows
  _searchenv(szFileSpec, "PATH", szPathSpec);           // PATH environment
  if (strlen(szPathSpec) > 0)                           // found
    return XW_NOERROR;                                  // report OK
  _searchenv(szFileSpec, "ProgramFiles", szPathSpec);   // applications path
  if (strlen(szPathSpec) > 0)                           // found
    return XW_NOERROR;                                  // report OK
  _searchenv(szFileSpec, "CommonProgramFiles", szPathSpec);  // data path
  if (strlen(szPathSpec) > 0)                           // found
    return XW_NOERROR;                                  // report OK

#endif

  for (i=0; szXWisp2Path[i] != NULL; i++) {             // search listed paths
    strcpy(szTempPath, szXWisp2Path[i]);                // next path
    strcat(szTempPath, szFileSpec);                     // concat filespec
    strcpy(szPathSpec, szTempPath);                     // pathspec to return
    pFile = fopen(szTempPath, "r");                     // open pathspec
    if (pFile != NULL) {                                // apparently found!
      fclose(pFile);                                    // done
      return XW_NOERROR;                                // report OK
      }
#if defined(__DEBUG__)
    printf("%s not found!\n", szPathSpec);
#endif
    }

  strcpy(szTempPath, szCommandSpec);                    // take command string
  pucPos = strrchr(szTempPath, ucDIRECTORYSEPARATOR);   // end of path
  if (pucPos != NULL)                                   // found
    *(pucPos+1) = '\0';                                 // mark end of path
  else
    *szTempPath = '\0';                                 // empty path
  strcat(szTempPath, szFileSpec);                       // filespec
  strcpy(szPathSpec, szTempPath);                       // pathspec to return
  pFile = fopen(szTempPath, "r");                       // with pathspec
  if (pFile != NULL) {                                  // found!
    fclose(pFile);                                      // done (with path)
#if defined(__DEBUG__)
    printf("%s found!\n", szPathSpec);
#endif
    return XW_NOERROR;                                  // report OK
    }

  if (!bQuiet)
    printf("ERR: Could not find configuration file '%s', errno=%d\n",
            szFileSpec, errno);                           // file not found(?)

  return  rc;
  }


// =====================================================================
//
//  OS_SerialBreak - send RS232-break signal to serial port
//
//  Input    - handle to opened serial port
//           - duration of break in milliseconds
//
//  Output   - nothing
//
//  Returns returncode of OS API call
//
//  Remarks - input and output queues are flushed
//          - break signal is followed by a short period of silence
//
// ====================================================================
extern  int  OS_SerialBreak(HSERIALPORT  hSerialPort,
                            ULONG        ulDuration) {

  int   rc = MINUS1;                                    // default: problem

#if defined(__OS2__)                                    // OS/2 target

  ULONG    ulDataLength;
  USHORT   usComErr, usComEvent;

  rc = OS_SerialFlush(hSerialPort);                     // flush buffers
  DosDevIOCtl(hSerialPort,
              IOCTL_ASYNC, ASYNC_SETBREAKON,            // break On
              NULL, 0, NULL,
              &usComErr, sizeof(usComErr), &ulDataLength);
  OS_Sleep(ulDuration);                                 // duration break ON
  DosDevIOCtl(hSerialPort,
              IOCTL_ASYNC, ASYNC_SETBREAKOFF,           // break Off
              NULL, 0, NULL,
              &usComErr, sizeof(usComErr), &ulDataLength);
  rc = DosDevIOCtl(hSerialPort,                         // reset status
                   IOCTL_ASYNC, ASYNC_GETCOMMEVENT,
                   NULL, 0, NULL,
                   &usComEvent, sizeof(usComEvent), &ulDataLength);
  OS_Sleep(10);                                         // short pause

#elif defined(__LINUX__)                                // Linux target

  OS_SerialFlush(hSerialPort);                          // flush buffers
  rc = ioctl(hSerialPort, TIOCSBRK);                    // set break
  OS_Sleep(ulDuration);                                 // break time
  rc = ioctl(hSerialPort, TIOCCBRK);                    // clear break
  OS_Sleep(10);                                         // short pause

#elif defined(__W32__)                                  // Windows target

  OS_SerialFlush(hSerialPort);                          // flush buffers
  if (FTDIWRAP(SetCommBreak(hSerialPort))) {            // start break
    OS_Sleep(ulDuration);                               // break period
    if (FTDIWRAP(ClearCommBreak(hSerialPort))) {        // stop break
      OS_Sleep(10);                                     // short pause
      rc = 0;                                           // OK
      }
    }

#endif

  OS_SerialFlush(hSerialPort);                          // flush buffers

  return  rc;
  }


// =====================================================================
//
//  OS_SerialClose - Close the serial port.
//
//  Input    - handle
//
//  Output   - Nothing
//
//  Returns nothing
//
// ====================================================================
extern  void  OS_SerialClose(HSERIALPORT  hSerialPort) {

#if defined(__OS2__)

  close(hSerialPort);

#elif defined(__LINUX__)

  close(hSerialPort);

#elif defined(__W32__)

  FTDIWRAP(CloseHandle(hSerialPort));

#endif

  }


// ====================================================================
//
//  OS_SerialFlush - flush serial port's input and output buffers
//
//  Input    - handle to open serial port
//
//  Output   - nothing
//
//  Returns returncode of OS API call
//
// ====================================================================
extern  int  OS_SerialFlush(HSERIALPORT  hSerialPort) {

  int   rc = MINUS1;                                    // default return code

#if defined(__OS2__)

  DosDevIOCtl(hSerialPort,
              IOCTL_GENERAL, DEV_FLUSHOUTPUT,           // flush output
              NULL, 0, NULL, NULL, 0, NULL);
  rc = DosDevIOCtl(hSerialPort,
              IOCTL_GENERAL, DEV_FLUSHINPUT,            // flush input
              NULL, 0, NULL, NULL, 0, NULL);

#elif defined(__LINUX__)

  rc = tcflush(hSerialPort, TCIOFLUSH);                 // both in and out

#elif defined(__W32__)

  if (FlushFileBuffers(hSerialPort)) {                  // flush I/O buffers
    FTDIWRAP(PurgeComm(hSerialPort, PURGE_TXABORT));    //
    FTDIWRAP(PurgeComm(hSerialPort, PURGE_RXABORT));    //
    FTDIWRAP(PurgeComm(hSerialPort, PURGE_TXCLEAR));    //
    FTDIWRAP(PurgeComm(hSerialPort, PURGE_RXCLEAR));    //
    OS_Sleep(1);
    rc = 0;                                             // OK
    }

#endif

  return  rc;
  }


// ====================================================================
//
//  OS_SerialGetBaudrate - Get current, minimum and maximum baudrate
//
//  Input    - Handle to open serial port
//           - Pointers to current, minimum and maximum baudrate
//             (when specified as NULL, the value will not be returned)
//
//  Output   - current, minimum and maximum baudrate of serial port
//
//  Returns returncode of OS API call
//
// ====================================================================
extern  int  OS_SerialGetBaudrate(HSERIALPORT  hSerialPort,
                                  PULONG       pulBaudrateCur,
                                  PULONG       pulBaudrateMin,
                                  PULONG       pulBaudrateMax) {

  int   rc = MINUS1;                                    // default: problem

#if defined(__OS2__)                                    // OS/2 target
  ULONG   ulDataLength;
  AXBPS   aXbps;
#elif defined(__LINUX__)                                // generic Linux target
  struct  termios  portinfo;
  ULONG   ulBaudIndex;
#elif defined(__W32__)                                  // Windows target
  DCB      dcb;
#endif

  if (pulBaudrateCur != NULL)
    *pulBaudrateCur = aulBaudrate[13];                  // default current
  if (pulBaudrateMin != NULL)
    *pulBaudrateMin = aulBaudrate[1];                   // default minimum
  if (pulBaudrateMax != NULL)
    *pulBaudrateMax = aulBaudrate[17];                  // default maximum

#if defined(__OS2__)                                    // OS/2 target

  rc = DosDevIOCtl(hSerialPort,                         // obtain baud info
                   IOCTL_ASYNC, ASYNC_EXTGETBAUDRATE,
                   NULL, 0, NULL,
                   &aXbps, sizeof(aXbps), &ulDataLength);
  if (rc == 0) {                                        // got it
    if (pulBaudrateCur != NULL)
      *pulBaudrateCur = aXbps.cur.bps;                  // current
    if (pulBaudrateMin != NULL)
      *pulBaudrateMin = aXbps.min.bps;                  // minimum
    if (pulBaudrateMax != NULL)
      *pulBaudrateMax = aXbps.max.bps;                  // maximum
    }

#elif defined(__LINUX__)                                // generic Linux target

  rc = tcgetattr(hSerialPort, &portinfo);               // obtain portinfo
  if (rc == 0) {                                        // got it
 #if defined(__DEBUG__)
    printf("GetBaudrate: c_cflag=%08lX, ispeed=%u, ospeed=%u\n",
            portinfo.c_cflag, portinfo.c_ispeed, portinfo.c_ospeed);
 #endif
    if (pulBaudrateCur != NULL) {                       // not NULL pointer
   #if defined(__APPLE__) || defined(__FREEBSD__)       // BSD-based
      *pulBaudrateCur = portinfo.c_ospeed;              // current speed
   #else                                                // Linux
      ulBaudIndex = (portinfo.c_cflag & CBAUD);         // copy baud flags
      if (ulBaudIndex <= 0x0F)                          // 38400 and lower
        *pulBaudrateCur = aulBaudrate[ulBaudIndex];     // value from table
      else {                                            // 57600 and higher
        ulBaudIndex &= 0x0F;                            // drop high bit
        *pulBaudrateCur = aulBaudrate[ulBaudIndex + 15];  // from 57600
        }
   #endif
      }
    }

#elif defined(__W32__)                                  // Windows target

  if (FTDIWRAP(GetCommState(hSerialPort, &dcb))) {      // port status
    rc = 0;                                             // OK
    if (pulBaudrateCur != NULL)
      *pulBaudrateCur = dcb.BaudRate;                   // current bps
    }

#endif

  return rc;                                            // return to caller
  }


// ====================================================================
//
//  OS_SerialOpen - Open the serial port.
//
//  Input    - name of the serial port
//
//  Output   - Nothing
//
//  Returns  - handle to serial port
//
// ====================================================================
extern  HSERIALPORT  OS_SerialOpen(char *pszSerialPortName) {

  HSERIALPORT  hSerialPort = INVALIDSERIALHANDLE;       // default return

#if defined(__OS2__)                                    // OS/2 target

  hSerialPort = sopen(pszSerialPortName,                // devicename
                      O_RDWR | O_TRUNC | O_BINARY,      // mode
                      SH_DENYRW);                       // no port sharing!

#elif defined(__LINUX__)                                // Linux target

  ULONG   ulFlags = 0;                                  // file control flags

  hSerialPort = sopen(pszSerialPortName,
                      O_RDWR | O_NOCTTY | O_NONBLOCK,   // open non blocking
                      SH_DENYRW);                       // no port sharing
                                                        // (avoid missing DCD)
  if (hSerialPort != INVALIDSERIALHANDLE) {             // opened
    ulFlags = fcntl(hSerialPort, F_GETFL, 0);           // current flags
    fcntl(hSerialPort, F_SETFL, ulFlags & ~O_NONBLOCK);  // set blocking!
    }

#elif defined(__W32__)                                  // Windows target

  #if defined(FTDIUSB)                                  // using FTDI API
    ULONG  rc, ulUSBnum, i;
    char  *pszDevStr[5];                                // pointer array
    char  szDevStr[4][64];                              // description array
    for (i=0; i<4; i++)                                 // init pointer array
      pszDevStr[i] = &szDevStr[i];
    pszDevStr[i] = NULL;                                // last

    if (!bQuiet)
      printf("Using FTDI API!\n");                      // inform user

    rc = FT_ListDevices(pszDevStr,                      // array of pointers
                        &ulUSBnum,                      // number of devices
                        FT_LIST_ALL | FT_OPEN_BY_DESCRIPTION);
    if (rc) {
      if (!bQuiet) {
        printf("ListDevices(all Descriptions) failed, rc %lu\n", rc);
        printf("Probably no FTDI USB serial converters installed\n");
        }
      return hSerialPort;
      }
    else {
      if (bVerbose  &&  !bQuiet) {
        printf("Number of USB serial ports with FTDI chip: %lu\n", ulUSBnum);
        for (i=0; i<ulUSBnum; i++)
          printf("  Portindex %lu : %s\n", i, pszDevStr[i]);
        }
      if (ulPortIndex < ulUSBnum)
        szPortName = strdup(pszDevStr[ulPortIndex]);     // replace port name
      else {
        if (!bQuiet)
          printf("Specified port index (%lu)"
                 " exceeds number of USB serial adapters &lu)!\n",
                  ulPortIndex, ulUSBnum);
        return hSerialPort;
        }
      }

    hSerialPort = FT_W32_CreateFile(szPortName,
                              GENERIC_WRITE | GENERIC_READ,
                              0,
                              NULL,
                              OPEN_EXISTING,
                              FILE_ATTRIBUTE_NORMAL | FT_OPEN_BY_DESCRIPTION,
                              NULL);

  #else                                                 // using Windows API
    hSerialPort = CreateFile(pszSerialPortName,
                             GENERIC_WRITE | GENERIC_READ,
                             0,
                             NULL,
                             OPEN_EXISTING,
                             FILE_FLAG_NO_BUFFERING,
                             NULL);
  #endif

#endif

  return hSerialPort;
  }


// ====================================================================
//
//  OS_SerialRead  - Read from the serial port
//
//  Input    - Handle, length
//
//  Output   - pointer to receive buffer
//
//  Returns  - Number of bytes read
//
//
// ====================================================================
extern  int   OS_SerialRead(HSERIALPORT hSerialPort,
                            char       *pucBuffer,
                            int         iNumChar) {

  long int   iBytesRead = 0;                            // default return

#if defined(__OS2__)                                    // OS/2 target

  iBytesRead = read(hSerialPort,
                    pucBuffer,
                    iNumChar);

#elif defined(__LINUX__)                                // Linux target

  iBytesRead = read(hSerialPort,
                    pucBuffer,
                    iNumChar);

#elif defined(__W32__)                                  // Windows target

  FTDIWRAP(ReadFile(hSerialPort,
                    pucBuffer,
                    iNumChar,
                    (PULONG)&iBytesRead,
                    NULL));

#endif

  return iBytesRead;
  }


// ====================================================================
//
//  OS_SerialSetBaudrate - Set baud rate
//
//  Input    - handle to open serial port
//           - New baudrate
//
//  Output   - Nothing
//
//  Returns returncode of OS function call
//
// ====================================================================
extern  int  OS_SerialSetBaudrate(HSERIALPORT  hSerialPort,
                                  ULONG        ulBaudrateNew) {

  int    rc = 0;                                        // default: problem

#if defined(__OS2__)                                    // OS/2 target

  ULONG   ulParmLength;
  XBPS    Xbps;                                         // extended bps

  Xbps.bps = ulBaudrateNew;
  rc = DosDevIOCtl(hSerialPort,
                   IOCTL_ASYNC, ASYNC_EXTSETBAUDRATE,
                   &Xbps, sizeof(Xbps), &ulParmLength,
                   NULL, 0, NULL);
  OS_Sleep(1);                                          // port settling?
  OS_SerialFlush(hSerialPort);

#elif defined(__LINUX__)                                // Linux target

  struct  termios  portinfo;                            // port info
 #if !(defined(__APPLE__) || defined(__FREEBSD__))      // not BSD-based
  int     i;                                            // counter
 #endif

 #if defined(__DEBUG__)
   printf("SetBaudrate: requested new baudrate %lu\n", ulBaudrateNew);
 #endif

  rc = tcgetattr(hSerialPort, &portinfo);
  if (rc == 0) {
    portinfo.c_ispeed = ulBaudrateNew;                  // set input speed
    portinfo.c_ospeed = ulBaudrateNew;                  //  "  output  "
 #if !(defined(__APPLE__) || defined(__FREEBSD__))      // not BSD-based
    portinfo.c_cflag &= ~CBAUD;                         // clear baud flags
    for (i=0; ulBaudrateNew > aulBaudrate[i]   &&
              i < sizeof(aulBaudrate)/sizeof(aulBaudrate[0]); i++)  // scan
      ;
    if (i < sizeof(aulBaudrate)/sizeof(aulBaudrate[0])) {  // match found
      if (i <= 15)                                      // 38400 or lower
        portinfo.c_cflag |= i;                          // new baud flags
      else                                              // 57600 or higher
        portinfo.c_cflag |= (0x1000 + (i - 15));        // new baud flags
      }
    else                                                // speed not in table
      portinfo.c_cflag |= B19200;                       // use default
 #endif
 #if defined(__DEBUG__)
    printf("Setbaudrate: c_cflag: %08lX, ispeed=%u, ospeed=%u\n",
            portinfo.c_cflag, portinfo.c_ispeed, portinfo.c_ospeed);
 #endif
    rc = tcsetattr(hSerialPort, TCSANOW, &portinfo);

    OS_Sleep(1);                                        // wait a little
    OS_SerialFlush(hSerialPort);                        // flush buffers
    }

#elif defined(__W32__)

  DCB     dcb;                                          // port info

  if (FTDIWRAP(GetCommState(hSerialPort, &dcb))) {
    dcb.BaudRate = ulBaudrateNew;
    if (FTDIWRAP(SetCommState(hSerialPort, &dcb))) {
      OS_Sleep(1);
      OS_SerialFlush(hSerialPort);
      rc = 0;                                           // OK
      }
    }

#endif

  return rc;                                            // return to caller
  }


// ====================================================================
//
// OS_SerialSetMode  - Set line characteristics
//
// Input  - handle to open serial port
//
//  - Set port control parameters
//  - Set line control
//
// Remarks: Wbus is half duplex without flow control ('ping-pong' protocol
//          on byte-by-byte basis.
//          Wisp628 can work full-duplex since firmware version 0.9.
//          The following settings will work optimal:
//          - writes: - CTS output flow control disabled
//                    - UART FiFo buffering load count 16
//          - reads:  - RTS input flow control disabled
//                    - using 'wait for something' mode:
//                      - read returns as soon as there is some data
//                      - times out when no data at all after timeout time
//                        which means: Wisp648 not responding (anymore)
//                    - UART FiFo input trigger: 1 character,
//                      larger values may cause significant slow-down!
//          - Line control: 8 databits, no parity bit, 1 stopbit.
//
// NOTE: 1. Uses default Timeout values, have to be set separately
//          when the defaults are not appropriate (see xwisp2com.c)
//       2. Wisp628 > 0.9 can handle burst of data. This is used
//          by XWisp to speed up communications (Python is slow!).
//          XWisp2 - from version 1.09 - uses this possibility by default,
//          it can be disabled by a 'DUPLEX HALF' command.
//
// ====================================================================
extern  int  OS_SerialSetMode(HSERIALPORT  hSerialPort) {

  int   rc = MINUS1;                                    // default: problem

#if defined(__OS2__)                                    // OS/2 target

  // extended hardware buffering
  #define EHB_DISABLE     0x08
  #define EHB_ENABLE      0x10
  // Receive Buffer Trigger Level
  #define EHB_RBTL_1      0x00
  #define EHB_RBTL_4      0x20
  #define EHB_RBTL_8      0x40
  #define EHB_RBTL_14     0x60
  // Transmit Buffer Load Count
  #define EHB_TBLC_16     0x80
  static  LINECONTROL LineControl = {8,0,0,0};          // fixed!
  DCBINFO  dcb;                                         // dcb parameters
  ULONG    ulParmlength, ulDataLength;                  // IOCTL parameters

  rc = DosDevIOCtl(hSerialPort,
                   IOCTL_ASYNC, ASYNC_GETDCBINFO,       // get current setting
                   NULL, 0, NULL,
                   &dcb, sizeof(DCBINFO), &ulDataLength);
  if (rc == 0) {                                        // OK
//  dcb.usWriteTimeout = 50;                            // .50 seconds
//  dcb.usReadTimeout  = (USHORT)(ulReadTimeout / 10);  // ms -> .01 secs
    dcb.fbCtlHndShake  = 0;                             // handshakes off
    dcb.fbFlowReplace  = 0;                             // no repl, no XON/XOFF
    dcb.fbTimeout     &= ~MODE_NO_WRITE_TIMEOUT;        // normal write timeout
    dcb.fbTimeout     &= ~MODE_NOWAIT_READ_TIMEOUT;     // reset both bits
    dcb.fbTimeout     |=  MODE_WAIT_READ_TIMEOUT;       // wait for something
    if (dcb.fbTimeout & (EHB_ENABLE | EHB_DISABLE)) {   // UART hw has FiFo
      dcb.fbTimeout  &= ~EHB_DISABLE;                   // reset disabled
      dcb.fbTimeout  |=  EHB_ENABLE;                    // set enabled
      dcb.fbTimeout  &= ~EHB_RBTL_14;                   // rcv trigger 1
      dcb.fbTimeout  |=  EHB_TBLC_16;                   // xmt load count 16
      }
    rc = DosDevIOCtl(hSerialPort,
                     IOCTL_ASYNC, ASYNC_SETDCBINFO,     // set new setting
                     &dcb, sizeof(DCBINFO), &ulParmlength,
                     NULL, 0, NULL);
    if (rc == 0) {
      rc = DosDevIOCtl(hSerialPort,                     // set char. format
                       IOCTL_ASYNC, ASYNC_SETLINECTRL,
                       &LineControl, sizeof(LINECONTROL), &ulParmlength,
                       NULL, 0, NULL);
      }
    }

#elif defined(__LINUX__)                                // Linux target

  struct  termios  portinfo;

  memset ((void *)&portinfo, 0, sizeof(struct termios));  // clear all
  portinfo.c_iflag = IGNPAR;                            // no parity
  portinfo.c_oflag = 0;                                 // out flags off
  portinfo.c_cflag = CS8|CREAD|CLOCAL;                  // 8,n,1
  portinfo.c_ispeed = ulWbusBaudrateDefault;            //
  portinfo.c_ospeed = ulWbusBaudrateDefault;            //
#if !(defined(__APPLE__) || defined(__FREEBSD__))       // not BSD-based
  portinfo.c_cflag |= B19200;                           // initial speed
#endif
  portinfo.c_lflag = 0;                                 // all flags off
   portinfo.c_cc[VMIN]  = 0;                            // 1 char or timeout
// portinfo.c_cc[VTIME] = ulReadTimeout/100;            // timeout (0.1 secs)
   portinfo.c_cc[VTIME] = 300/100;                      // timeout (0.1 secs)

  OS_SerialFlush(hSerialPort);                          // flush buffers
  rc = tcsetattr(hSerialPort, TCSANOW, &portinfo);      // immed change

#elif defined(__W32__)                                  // Windows target

  DCB             dcb;

  if (FTDIWRAP(GetCommState(hSerialPort, &dcb))) {
    dcb.fParity = FALSE;
    dcb.fOutxCtsFlow = FALSE;
    dcb.fOutxDsrFlow = FALSE;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;              // DTR control off
    dcb.fDsrSensitivity = FALSE;
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;
    dcb.fNull = FALSE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;              // RTS control off
    dcb.fAbortOnError = FALSE;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;
    dcb.ByteSize = 8;
    if (FTDIWRAP(SetCommState(hSerialPort, &dcb)))
      rc = 0;                                           // OK
    }

#endif

  return  rc;
  }


// ====================================================================
//
// OS_SerialSetModemOutputStatus
//
// Input   - handle to open serial port
//         - desired DTR status (TRUE/FALSE)
//         - desired RTS status (TRUE/FALSE)
//
// Output: - nothing
//
// Returns result of OS API call.
//
// ====================================================================
extern  int  OS_SerialSetModemOutputStatus(HSERIALPORT  hSerialPort,
                                           BOOLEAN      bDTR,
                                           BOOLEAN      bRTS) {

  int    rc = MINUS1;                                   // default: problem

#if defined(__OS2__)                                    // OS/2 target

  ULONG   ulParmLength, ulDataLength;                   // IOCtl parms
  MODEMSTATUS  ModemStatus;                             // modem status
  USHORT  ComErr;                                       // IOCtl return
  char    ucStatusMask;                                 // bit pattern

  ucStatusMask = ((bDTR) ? DTR_ON : 0) | ((bRTS) ? RTS_ON : 0);
  ModemStatus.fbModemOn  = ucStatusMask;
  ModemStatus.fbModemOff = 0xFC | ucStatusMask;         // reset mask
  rc = DosDevIOCtl(hSerialPort,
                   IOCTL_ASYNC, ASYNC_SETMODEMCTRL,
                   &ModemStatus, sizeof(ModemStatus), &ulParmLength,
                   &ComErr, sizeof(ComErr), &ulDataLength);

#elif defined(__LINUX__)                                // Linux target

  int  control;

  control = TIOCM_DTR;
  rc = ioctl(hSerialPort, (bDTR) ? TIOCMBIS : TIOCMBIC, &control);
  if (rc == 0) {
    control = TIOCM_RTS;
    rc = ioctl(hSerialPort, (bRTS) ? TIOCMBIS : TIOCMBIC, &control);
    }

#elif defined (__W32__)                                 // Windows target

  if (FTDIWRAP(EscapeCommFunction(hSerialPort, (bDTR) ? SETDTR : CLRDTR))) {
    if (FTDIWRAP(EscapeCommFunction(hSerialPort, (bRTS) ? SETRTS : CLRRTS)))
      rc = 0;                                           // OK
    }

#endif

  return rc;
  }


// ====================================================================
//
// OS_SerialSetReadTimeout
//
// Input   - handle to open serial port
//         - read timeout value in milliseconds
//
// Output: - nothing
//
// Returns result of OS API call.
//
// ====================================================================
extern  int  OS_SerialSetReadTimeout(HSERIALPORT  hSerialPort,
                                     ULONG        ulReadTimeout) {

  int    rc = MINUS1;                                   // default: problem

#if defined(__OS2__)                                    // OS/2 target

  DCBINFO  dcb;                                         // dcb parameters
  ULONG    ulParmlength, ulDataLength;                  // IOCTL parameters

  rc = DosDevIOCtl(hSerialPort,
                   IOCTL_ASYNC, ASYNC_GETDCBINFO,       // get current setting
                   NULL, 0, NULL,
                   &dcb, sizeof(DCBINFO), &ulDataLength);
  if (rc == 0) {                                        // OK
    dcb.usWriteTimeout = 50;                            // .50 seconds
    dcb.usReadTimeout  = (USHORT)(ulReadTimeout / 10);  // ms -> .01 secs
    rc = DosDevIOCtl(hSerialPort,
                     IOCTL_ASYNC, ASYNC_SETDCBINFO,     // set new setting
                     &dcb, sizeof(DCBINFO), &ulParmlength,
                     NULL, 0, NULL);
    }

#elif defined(__LINUX__)                                // Linux target

  struct  termios  portinfo;

  rc = tcgetattr(hSerialPort, &portinfo);               // get current setting
  if (rc == 0) {
    portinfo.c_cc[VMIN]  = 0;                           // 1 char or timeout
    portinfo.c_cc[VTIME] = ulReadTimeout/100;           // timeout (0.1 secs)
    rc = tcsetattr(hSerialPort, TCSANOW, &portinfo);    // immed change
    }

#elif defined (__W32__)                                 // Windows target

  COMMTIMEOUTS    comtmo;

  if (FTDIWRAP(GetCommTimeouts(hSerialPort, &comtmo))) {  // get current
    comtmo.ReadIntervalTimeout = MAXDWORD;
    comtmo.ReadTotalTimeoutMultiplier = MAXDWORD;
    comtmo.ReadTotalTimeoutConstant = ulReadTimeout;
    comtmo.WriteTotalTimeoutMultiplier = 1;
    comtmo.WriteTotalTimeoutConstant = 500;             // .5 sec
    if (FTDIWRAP(SetCommTimeouts(hSerialPort, &comtmo)))
      rc = 0;                                           // OK
    }

#endif

  return rc;
  }


// ====================================================================
//
//  OS_SerialWrite - Write to the serial port.
//
//  Input    - Handle, buffer, length
//
//  Output   - Nothing.
//
//  Returns  - Number of bytes written
//
// ====================================================================
extern  int  OS_SerialWrite(HSERIALPORT hSerialPort,
                            char       *pucBuffer,
                            int         NumChar) {

  int  iBytesWritten = 0;                               // default return

#if defined(__OS2__)                                    // OS/2 target

  iBytesWritten = write(hSerialPort,
                        pucBuffer,
                        NumChar);

#elif defined(__LINUX__)                                // Linux target

  int  iBlockSizeWritten;                               // intermediate write

  while (iBytesWritten < NumChar) {                     // until all written
    iBlockSizeWritten = write(hSerialPort,
                              pucBuffer + iBytesWritten,
                              NumChar - iBytesWritten);
    if (iBlockSizeWritten < 0) {
      if (errno == EAGAIN)                              // more to write
        break;
      }
    else
      iBytesWritten += iBlockSizeWritten;
    }

#elif defined(__W32__)                                  // Windows target

  FTDIWRAP(WriteFile(hSerialPort,
                     pucBuffer,
                     NumChar,
                     (PULONG)&iBytesWritten,
                     NULL));

#endif

  return iBytesWritten;
  }


// ====================================================================
//
//  OS_Sleep - Put program(-thread) asleep for some time
//
//  Input   - Duration of sleep in milliseconds
//
//  Output  - Nothing
//
//  Returns returncode of OS function call
//
// ====================================================================
extern  int  OS_Sleep(ULONG  ulMilliSeconds) {

  int  rc = MINUS1;                                     // default returncode

#if defined(__OS2__)                                    // OS/2 target

  rc = DosSleep(ulMilliSeconds);

#elif defined(__LINUX__)                                // Linux target

  struct timespec  TimeSpec;

  TimeSpec.tv_sec = ulMilliSeconds / 1000;              // seconds
  TimeSpec.tv_nsec = ulMilliSeconds * 1000000;          // nanoseconds
  rc = nanosleep(&TimeSpec, NULL);                      // sleep

#elif defined(__W32__)                                  // Windows target

  Sleep(ulMilliSeconds);                                // void return
  rc = 0;                                               // OK

#endif

  return rc;
  }


// ====================================================================
//
//   OS_Time  -  Return current time in msecs since first call
//
//   Timer resolution: milliseconds
//
// ====================================================================
extern  ULONG  OS_Time(void) {

  ULONG   ulTimeNow = 0;                                // default returnvalue

#if defined(__OS2__)                                    // OS/2 target

  static  ULONG ulTimeStart = MINUS1;

  if (ulTimeStart == MINUS1)                            // first call
    ulTimeStart = clock() * 1000 / CLOCKS_PER_SEC;
  ulTimeNow = clock() * 1000 / CLOCKS_PER_SEC - ulTimeStart;  // msecs

#elif defined(__LINUX__)                                // Linux target

  static struct timeval TimeStart = {MINUS1,MINUS1};
         struct timeval TimeNow;

  if ((TimeStart.tv_sec & TimeStart.tv_usec) == MINUS1) // first call
    gettimeofday(&TimeStart, NULL);                     // start time
  gettimeofday(&TimeNow, NULL);                         // current time
  ulTimeNow  = TimeNow.tv_sec * 1000 + TimeNow.tv_usec / 1000;
  ulTimeNow -= TimeStart.tv_sec * 1000 + TimeStart.tv_usec / 1000;  // msecs

#elif defined(__W32__)                                  // Windows Target

  static  ULONG ulTimeStart = MINUS1;

  if (ulTimeStart == MINUS1)                            // first call
    ulTimeStart = GetTickCount();                       // store start time
  ulTimeNow = GetTickCount() - ulTimeStart;             // msecs

#endif

  return  ulTimeNow;                                    // relative time

  }
