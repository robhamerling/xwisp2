
// -------------------------------------------------------------
// General purpose C-compiler handling
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// -------------------------------------------------------------
// Purpose:  Handle compiler-specific and related platform specific
//           differences and peculiarities:
//             #define runtime platform:
//              - __OS2__
//              - __LINUX__
//              - __FREEBSD__
//              - __APPLE__
//              - __W32__
//             derive some generic constants and #define:
//              - compiler_name
//              - compiler_major
//              - compiler_minor
//             #define macros for missing compiler functions like:
//              max(), min(), stricmp(), strnicmp()
//
// -------------------------------------------------------------

#if defined(__WATCOMC__)
  #if __WATCOMC__ < 1200
    #define compiler_name     "Watcom C/C++"
    #define compiler_version  (__WATCOMC__ / 100)
    #define compiler_minor    (__WATCOMC__ % 100)
  #elif __WATCOMC__ >= 2000
    #define compiler_name     "Open Watcom C/C++"
    #define compiler_version  (__WATCOMC__ / 1000)
    #define compiler_minor    (__WATCOMC__ % 1000)
  #else
    #define compiler_name     "Open Watcom C/C++"
    #define compiler_version  ((__WATCOMC__ - 1100) / 100)
    #define compiler_minor    ((__WATCOMC__ - 1100) % 100)
  #endif
  #if defined(__NT__)
    #define __W32__
  #elif defined(__UNIX__)
    #define __LINUX__
  #endif                                                // default: runtime OS

#elif defined(__IBMC__)
  #if __IBMC__ < 300
    #define compiler_name     "IBM C/C++"
  #else
    #define compiler_name     "IBM VAC"
  #endif
  #define compiler_version  (__IBMC__ / 100)
  #define compiler_minor    (__IBMC__ % 100)            // implies OS2

#elif defined(__GNUC__) && defined(__OS2__)             // GCC + EMX (OS/2)
  #define compiler_name     "GCC"
  #define compiler_version  (__GNUC__)
  #define compiler_minor    (__GNUC_MINOR__)
  #define max(x,y) (((x) > (y)) ? (x) : (y))            // missing ..
  #define min(x,y) (((x) < (y)) ? (x) : (y))            // .. functions


#elif defined(__gnu_linux__) || defined(__linux) || \
      defined(__APPLE__) || defined(__FreeBSD__)
  #define compiler_name     "GCC"
  #if defined(__GNUC__)
    #define compiler_version  (__GNUC__)
  #elif defined(__VERSION__)
    #define compiler_version  (__VERSION__[0] - '0')    // first char -> num
  #else
    #define compiler_version  (0)                       // unknown
  #endif
  #if defined(__GNUC_MINOR__)
    #define compiler_minor    (__GNUC_MINOR__)
  #elif defined(__VERSION__)
    #define compiler_minor    (__VERSION__[2] - '0')    // third char -> num
  #else
    #define compiler_minor    (0)                       // unknown
  #endif
  #define max(x,y) (((x) > (y)) ? (x) : (y))            // missing ..
  #define min(x,y) (((x) < (y)) ? (x) : (y))            // .. functions ..
  #define strnicmp strncasecmp                          // alias
  #define stricmp  strcasecmp                           // alias
  #if defined(__FreeBSD__)
    #define __FREEBSD__                                 // alias
  #endif
  #define __LINUX__                                     // selected

#elif defined(__GNUC__) && defined(__WIN32__)           // GCC (W32/MinGW)
  #define compiler_name     "GCC"
  #define compiler_version  (__GNUC__)
  #define compiler_minor    (__GNUC_MINOR__)
  #define max(x,y) (((x) > (y)) ? (x) : (y))            // missing ..
  #define min(x,y) (((x) < (y)) ? (x) : (y))            // .. functions
  #define strnicmp strncasecmp                          // alias
  #define stricmp  strcasecmp                           // alias
  #define __W32__                                       // selected

#elif defined(__LCC__)
  #define compiler_name     "LCC"
  #define compiler_version  (4)                         // no version?
  #define compiler_minor    (0)                         // no subversion?
  #define __W32__                                       // implied!

#elif defined(__DMC__)                                  // Digital Mars C
  #define compiler_name     "DigitalMars C"
  #define compiler_version  (8)
  #define compiler_minor    (45)
  #define max(x,y) ((x > y) ? (x) : (y))                // missing ..
  #define min(x,y) ((x < y) ? (x) : (y))                // .. functions
  #define __W32__                                       // implied!

#elif defined(__BORLANDC__)                             // BCC
  #define compiler_name     "Borland C"
  #define compiler_version  (__BORLANDC__ / 256)
  #define compiler_minor   ((__BORLANDC__ % 256) / 16)
  #define __W32__                                       // implied!

#elif defined(_MSC_VER)
  #define compiler_name     "MS-C"
  #define compiler_version  (_MSC_VER / 100)
  #define compiler_minor    (_MSC_VER % 100)
  #define __DOS__                                       // 16-bits DOS

#elif defined(__DJGPP__)
  #define compiler_name     "DJGPP"
  #define compiler_version  (__DJGPP__)
  #define compiler_minor    (__DJGPP_MINOR__)
  #include <dir.h>
  #include <errno.h>                                    // not in <stdlib.h>
  #define __DOS__                                       // implied!

#else                                                   // default
  #define compiler_name     "Unknown compiler"
  #define compiler_version  (0)
  #define compiler_minor    (0)
  #define __W32__                                       // assumed!

#endif
