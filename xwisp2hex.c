
// ===================================================================
//
// XWisp2Hex.C - Wisp648 Programmer support.
//
//             -  Higher level PIC programming
//             -  Hex file-oriented functions
//
// Author:  R. Hamerling
//
// Copyright (c) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// Version: Actual version in xwisp2.ver, reported at startup!
//
// ===================================================================
//
// Note: since version 1.07 all data offsets and addresses are in bytes!
//
//
// 'ProgRegion' is a character array containing the bytes of the
// hexfile in the same sequence and at the offsets as in the hex file.
// For the 18Fxxx range it used only for program memory, but for the
// midrange PICs it is used for all regions.
// For the 18Fxxx range memory for Data, ID, and Fuses is allocated
// separately at the occurance of a extended lineair segment record
// in the hex file.
//
// For the midrange PICs (which are 'word' oriented) even and odd positions
// have to be swapped before transfer to the PIC. For the 18Fxxx range
// this swapping is not needed.
// When the PIC memory is read the same sequence is used. So when writing
// the hex file no byte-swapping is required.
//
// ===================================================================

// #define __DEBUG__                                    // debugging code

#include "xwisp2.h"                                     // all includes


#define   SEGMENTSIZE      0x10000                      // segments are 64KB
#define   WORDSPERLINE     8                            // # of 4 hex digits

// locally defined,  externally referenced variables

        ULONG   ulCodeByteCount = 0;                    // program size
        ULONG   ulDataByteCount = 0;                    // data size

static  BOOLEAN bNewExtLinSegment = FALSE;              // for delayed write
                                                        // set by WriteHexFile
                                                        // reset by
                                                        //   WriteHexDataLine

// prototypes of local functions

static  char   *Byte2Hex(char, char *);
static  char    CheckSumHex(char *, ULONG);
static  int     DumpLine(char *, ULONG, ULONG, ULONG, USHORT);
static  void    DumpSegment(char *, ULONG, ULONG, USHORT);
static  int     ProcessHexLine(PHEXLINE);               // handle contents
static  char   *Word2Hex(USHORT, char *);
static  int     WriteHexLine(char *, ULONG, ULONG, ULONG,
                             USHORT, FILE *);
static  void    WriteHexSegment(char *, ULONG, ULONG, USHORT, FILE *);
static  void    WriteHexExtLinAddrLine(ULONG, FILE *);


//       --------------------------------
// Note: Functions ordered alphabetically
//       --------------------------------


// ----------------------------------------------------------------
//
// Convert byte to array of 2 hex digits
//
// Input  - unsigned character
//        - pointer to output buffer
//
// Output - 2 hex characters in destination buffer (no trailing zero!)
//
// Returns pointer to the formatted buffer
//
// ----------------------------------------------------------------
static  char  *Byte2Hex(char  ucByte,
                        char  *pucHex) {

  static const char  *aucHexDigits = "0123456789ABCDEF";

  pucHex[0] = aucHexDigits[ucByte / 16];                // MSB
  pucHex[1] = aucHexDigits[ucByte & 0x0F];              // LSB
  return  pucHex;                                       // pointer to result
  }


// ----------------------------------------------------------------
//
// Calculate checksum of array of hex digits
//
// Input  - pointer to start of array of hex digits
//        - length of array (number of hex digits)
//
// Output - 1 byte checksum value (binary)
//
// ----------------------------------------------------------------
static  char  CheckSumHex(char  *pucData,
                          ULONG  ulLength) {

  int     i;                                            // counter(s)
  USHORT  usSum;
  char    ucHex;

  usSum = 0;
  for (i=0; i<ulLength; i++,i++) {                      // per 2 hex digits
    Hex2Byte(pucData + i, &ucHex);                      // convert
    usSum += ucHex;                                     // add value
    }
  usSum = (usSum ^ (USHORT)MINUS1) + 1;                 // 2's compl.
  return (char)(usSum & 0x00FF);                        // return LSB
  }


// ---------------------------------------------------
//
// DumpImage - display complete memory image on console
//
// Input   - nothing
//
// Output  - hex lines on screen
//
// Returns - zero returncode if everything OK
//         - otherwise errorcode and error message
//
// Remarks - target must be known
//           - automatically detected via programmer
//           - by commandline specification
//
// ---------------------------------------------------
extern  int    DumpImage(void) {

  int     i;                                            // counter
  int     rc;                                           // returncode
  ULONG   ulFam;                                        // PIC family
  ULONG   ulStartDump;                                  // read timing

  ulStartDump = OS_Time();                              // start of dump

  if (pTUsed == NULL) {                                 // target not detected
    if (pTSpec == NULL) {                               // not spec'd / auto
      rc = ProgrammerActivate();                        // init programmer
      if (rc != XW_NOERROR) {                           // init failed
        if (!bQuiet)
          printf("No programmer connected to %s, target must be specified\n",
                  szPortName);
        return rc;
        }
      IdentifyTarget();                                 // detect target
      if (pTUsed == NULL)                               // undetermined
        return XW_NOENT;                                // cannot proceed
      }
    else                                                // target specified
      pTUsed = pTSpec;                                  // accept it!
    }

  ulFam = pTUsed->FamilyIndex;                          // actual family

  if (fRegions & REGION_PROG) {                         // region selected
    if (bVerbose  &&  !bQuiet)
      printf("Dumping program region\n");
    for (i = 0; i < pTUsed->ProgSize; i += SEGMENTSIZE)  // per segment
      DumpSegment(ProgRegion + i,
                  pTUsed->ProgStart + i,
                  min(pTUsed->ProgSize - i, SEGMENTSIZE),
                  pTUsed->BlankMask);
    }

  if (fRegions & REGION_DATA) {                         // region selected
    if (bVerbose  &&  !bQuiet)
      printf("Dumping data region\n");
    DumpSegment((ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
                pTUsed->DataStart,
                pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),
                (ulFam == CORE_16) ? 0xFFFF : 0x00FF);                                // data always BYTES
    }

  if (fRegions & REGION_ID) {                           // region selected
    if (bVerbose  &&  !bQuiet)
      printf("Dumping ID region\n");
    DumpSegment((ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
                 pTUsed->IDStart,
                 pTUsed->IDSize,
                 pTUsed->BlankMask);
    }

  if (fRegions & REGION_FUSES) {                        // region selected
    if (bVerbose  &&  !bQuiet)
      printf("Dumping fuses region\n");
//  ControlFuses();                                     // special treatment?
    DumpSegment((ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
                 pTUsed->FusesStart,
                 pTUsed->FusesSize,
                 pTUsed->BlankMask);
    }

  return ReportResult("Dump operation", 0, ulStartDump);
  }


// ---------------------------------------
//
// Dump line with memory contents to console
//
// Input    - pointer to start of memoryblock for this segment
//          - extended lineair segment address
//          - offset within segment
//          - maximum offset within segment
//          - bit mask for valid data
//
// Output   - line
//
// Returns  - segment offset for next line
//
// Remarks: - lines are address aligned on 16 byte boundary
//          - large erased memory locations are not written
//
// ---------------------------------------
static  int   DumpLine(char   *pucSegment,
                       ULONG   ulSegmentAddress,
                       ULONG   ulSegmentOffset,
                       ULONG   ulMaxSegmentOffset,
                       USHORT  usBlankMask) {

  int    i;                                             // counter(s)
  ULONG  ulLocalOffset;                                 // moving offset
  ULONG  ll;                                            // line length
  USHORT usMemWord;                                     // data
  char  szLine[128];                                   // output line

  ulLocalOffset = ulSegmentOffset;                      // start data offset
  while (ulLocalOffset < ulMaxSegmentOffset) {          // whole segment
    usMemWord = BYTES2WORD(pucSegment + ulLocalOffset);
    if ((usMemWord & usBlankMask) != usBlankMask)       // not erased location
      break;
    ulLocalOffset += 2;                                 // next 'word'
    }
  if (ulLocalOffset >= ulMaxSegmentOffset)              // out of bounds
    return ulMaxSegmentOffset;                          // all blank

  ll = sprintf(szLine, "%06lX:", ulSegmentAddress + ulLocalOffset);

#ifdef __DEBUG__
  printf("Dump line: LocalOffset %06X.", ulLocalOffset);
#endif

  for (i=0; i < WORDSPERLINE  &&
            ulLocalOffset + (2 * i) < ulMaxSegmentOffset; ++i)
    ll += sprintf(szLine + ll, " %02hX %02hX",
          (USHORT)pucSegment[ulLocalOffset + 2 * i],
          (USHORT)pucSegment[ulLocalOffset + 2 * i + 1]);

  for (  ; i > 0; --i) {                                // backward scan

    usMemWord = BYTES2WORD(pucSegment + ulLocalOffset + 2 * (i-1));  // word
    if ((usMemWord & usBlankMask) != usBlankMask)       // not erased location
      break;                                            // end of block
    }

#ifdef __DEBUG__
  printf(".%06X\n", ulLocalOffset + 2 * i);             // dumped!
#endif

  if (i > 0  &&  !bQuiet)                               // at least 1 word
    printf("%.*s\n", 7 + i * 6, szLine);

  return ulLocalOffset + (2 * WORDSPERLINE);            // actual progress
  }


// ---------------------------------------
//
// Dump Segment of a Region
//
// Input    - pointer to start of memoryblock for this segment
//          - extended lineair segment address
//          - output file pointer
//
// Output   - nothing (calls dumpline)
//
// Returns  - nothing
//
// Remarks: - none
//
// ---------------------------------------
static  void  DumpSegment(char   *pucSegment,
                          ULONG   ulSegmentAddress,
                          ULONG   ulMaxSegmentOffset,
                          USHORT  usBlankMask) {

  ULONG  i;                                             // counter(s)

#ifdef __DEBUG__
  printf("New segment: Address %06lX, Max Offset %06lX, mask %04hX\n",
                   ulSegmentAddress, ulMaxSegmentOffset, usBlankMask);
#endif

  for (i = 0; i < ulMaxSegmentOffset; ) {
    i = DumpLine(pucSegment,                            // ptr to mem data
                 ulSegmentAddress,                      // start addr segment
                 i,                                     // offset in segment
                 ulMaxSegmentOffset,                    // upper bound
                 usBlankMask);
    }

  return;
  }



// ----------------------------------------------------------------
//
// Convert a pair of hex digits to an unsigned character
//
// Input  - pointer to array of 2 hex digits
//        - pointer to character to contain converted hex digits
//
// Return - returncode
//          - zero when valid hex digits found
//          - 1 when one of both bytes is not a valid hex digit
//
// Remarks: Assumes most_significant_nibble_first notation
//
// ----------------------------------------------------------------
extern  int   Hex2Byte(char *pucX,
                       char *pucC) {

  int    i;                                             // counter(s)
  int    rc = 0;                                        // returncode (default)
  char   x, c;                                          // conversion fields

  c = 0;                                                // init converted char
  for (i=0; i<2; ++i) {                                 // 2 hex digits
    c <<= 4;                                            // shift to next digit
    x = pucX[i];
    if (x >= '0' && x <= '9')                           // 0..9
      c += x - '0';
    else if (x >= 'A' && x <= 'F')                      // A..F
      c += x - 'A' + 10;
    else if (x >= 'a' && x <= 'f')                      // a..f
      c += x - 'a' + 10;
    else {                                              // out of range
      c = 0x00;                                         // something
      rc = 1;                                           // invalid hex digit
      break;                                            // done
      }
    }

  if (pucC != NULL)                                     // not a null pointer
    *pucC = c;                                          // return byte

  return rc;                                            // signal result
  }


// ----------------------------------------------------------------
//
// Process line of HEX file
//
// Input   - pointer to start of (parsed) line
//         - pointer to data array
//
// Output  - memory locations array filled with data from hex line
//
// Returns - returncode (MINUS1 = end-of-file)
//
// Remarks - memory for program-segment is preallocated in main()
//         - memory for ID, fuses- or data-memory allocated
//           when a corresponding extended lineair segment record
//           is encountered
//
// ----------------------------------------------------------------
static  int    ProcessHexLine(PHEXLINE pHexLine) {

  int     rc;                                           // return code
  ULONG   ulAddress;                                    // data address
  USHORT  usOffset;                                     // offset in region
  USHORT  usTemp;                                       // intermediate
  ULONG   i, n;                                         // counter(s)
  char    ucType, ucHex;                                // record type

  rc = XW_NOERROR;                                      // default

  Hex2Byte(pHexLine->Length, &ucHex);                   // # data bytes
  n = ucHex;
  HEX2WORD(pHexLine->Offset, &usOffset);                // memory offset
  Hex2Byte(pHexLine->Type, &ucType);                    // record type

  switch(ucType) {                                      // type specific

    case 0x00:                                          // data record
      if (ulLBA < 0x200000) {                           // program memory
        ulAddress = ulLBA + usOffset;                   // start offset
        if (ulAddress > ulProgSize) {                   // beyond array bound
          if (!bQuiet)
            printf("Program memory offset ('%06lX') too high!\n", ulAddress);
          rc = XW_INVAL;                                // problem
          break;
          }
        for (i = 0; i < n; ++i) {                       // all pairs hex digits
          Hex2Byte(pHexLine->Data + i * 2, ProgRegion + ulAddress); // convert
          ulAddress++;                                  // next position
          }
        ulCodeByteCount += n;                           // program size
        }
      else if (ulLBA == 0x200000) {                     // ID memory 18Fxxx
        ulAddress = usOffset;                           // start offset
        if (ulAddress > ulIDSize) {                     // beyond array bound
          if (!bQuiet)
            printf("ID memory offset ('%04lX') too high!\n", ulAddress);
          rc = XW_INVAL;                                // problem
          break;
          }
        for (i = 0; i < n; ++i) {                       // all pairs
          Hex2Byte(pHexLine->Data + i * 2, IDRegion + ulAddress);
          ulAddress++;
          }
        }
      else if (ulLBA == 0x300000) {                     // fuses memory 18Fxxx
        ulAddress = usOffset;                           // start offset
        if (ulAddress > ulFusesSize) {                  // beyond array bound
          if (!bQuiet)
            printf("Fuses memory offset ('%04lX') too high!\n", ulAddress);
          rc = XW_INVAL;                                // problem
          break;
          }
        for (i = 0; i < n; ++i) {                        // all pairs
// was ...
          Hex2Byte(pHexLine->Data + i * 2, FusesRegion + ulAddress);
          ulAddress++;
          }
        }
      else if (ulLBA == 0xF00000) {                     // data memory 18Fxxx
        ulAddress = usOffset;                           // start offset
        if (ulAddress > ulDataSize) {                   // beyond array bound
          if (!bQuiet)
            printf("Data memory offset ('%04lX') too high!\n", ulAddress);
          rc = XW_INVAL;                                // problem
          break;
          }
        for (i = 0; i < n; ++i) {                       // all pairs
//         DataRegion[ulAddress++] = Hex2Byte(pHexLine->Data + i * 2);
           Hex2Byte(pHexLine->Data + i * 2, DataRegion + ulAddress);
           ulAddress++;
           }
        ulDataByteCount += n;                           // data size
        }
      break;

    case 0x01:
      rc = -1;
      break;

    case 0x02:
      break;

    case 0x03:
      break;

    case 0x04:
      HEX2WORD(pHexLine->Data, &usTemp);                // convert
      ulLBA  = (usTemp & 0x00FF) << 16;                 // 24 bits addr
#ifdef __DEBUG__
      printf("New extended lineair segment address: %06X\n", ulLBA);
#endif
      break;

    case 0x05:
      break;

    default:
      if (!bQuiet)
        printf("Unknown hex record type '%02hX\n", (USHORT)ucType);
      rc = XW_INVAL;                                    // error
      break;
    }

  return rc;                                            // done
  }


// --------------------------------------------
//
// Read HEX-file, store contents in memory array(s)
//
// Input    - filename
//
// Output   - 0 for OK, 2 for error
//          - eventually error report on stdout
//
// Rermarks - Previously allocated memory regions must be bulk erased
//            before reading a (new) hex file.
//
// --------------------------------------------
extern  int    ReadHexFile(char  *szFileName) {

  FILE  *pfHF;                                          // HEX file pointer
  int    rc;                                            // returncode
  char   ucCheckSum, ucCheckDigit;                      // checksums
  int    i, n;                                          // counter(s)
  char   szBuffer[sizeof(HEXLINE)];                     // input buffer
  USHORT usBlankMask;                                   // erase pgm word
  USHORT usDataMask;                                    // erase data word
  char   ucDataCount;                                   // # data bytes

  if ((pfHF = fopen(szFileName, "r")) == NULL) {        // open for read
    if (!bQuiet)
      printf("Could not open input file %s\n", szFileName);
    BeepError();
    return  XW_NOENT;
    }

  if (bVerbose  &&  !bQuiet)
    printf("Reading file '%s'\n", szFileName);

  ulCodeByteCount = ulDataByteCount = 0;                // clear counters
  usBlankMask = (USHORT)MINUS1;                         // default
  usDataMask = (USHORT)MINUS1;                          // default
  if (pTUsed != NULL) {                                 // target known
    usBlankMask = pTUsed->BlankMask;                    // replace mask
    if (pTUsed->FamilyIndex != CORE_16)                 // not 18F family
      usDataMask = 0x00FF;                              // replace mask
    }

  ulLBA = 0;                                            // reset to default

  BulkErase(ulProgSize,  ProgRegion,  usBlankMask);     // )
  BulkErase(ulDataSize,  DataRegion,  usDataMask);      // ) erase
  BulkErase(ulIDSize,    IDRegion,    usBlankMask);     // )
  BulkErase(ulFusesSize, FusesRegion, usBlankMask);     // )

  rc = XW_NOERROR;                                      // to enter the loop
  for (i=1; rc == XW_NOERROR; ++i) {                    // all lines in HEXfile

    memset(szBuffer, 0, sizeof(szBuffer));              // reset read buffer

    if (fgets(szBuffer, sizeof(szBuffer), pfHF) == NULL)  // read next line
      break;                                            // EOF

    if (strlen(szBuffer) < 11) {                        // invalid length
      if (!bQuiet)
        printf("HEX file error, line %d too short\n", i);
      rc = XW_INVAL;
      break;
      }

    if (szBuffer[0] != ':') {                           // invalid start char
      if (!bQuiet)
        printf("HEX file error, line %d not starting with ':'\n", i);
      rc = XW_INVAL;
      break;
      }

    Hex2Byte(szBuffer + 1, &ucDataCount);               // # of data bytes
    n = ucDataCount;
    if (strlen(szBuffer) < 11 + 2 * n) {                // invalid length
      if (!bQuiet)
        printf("HEX file error, line %d invalid length\n", i);
      rc = XW_INVAL;
      break;
      }

    ucCheckSum = CheckSumHex(szBuffer + 1, 8 + 2 * n);  // calculated
//  ucCheckDigit = Hex2Byte(szBuffer + 9 + 2 * n);      // presented
    Hex2Byte(szBuffer + 9 + 2 * n, &ucCheckDigit);      // presented
    if (ucCheckDigit != ucCheckSum) {                   // must match!
      if (!bQuiet)
        printf("Hex file error, line %d checksum incorrect,"
                      " calculated: '%02hX', found: '%02hX'\n",
                      i, (USHORT)ucCheckSum,
                         (USHORT)ucCheckDigit);
      rc = XW_INVAL;
      break;
      }

    rc = ProcessHexLine((PHEXLINE)szBuffer);            // data to mem-array
    }

  fclose(pfHF);                                         // done with hex file

  if (rc == XW_NOERROR  ||  rc == MINUS1) {
    if (!bQuiet) {
      printf("File %s loaded and is Intel Hex format conforming\n", szFileName);
      if (bVerbose)
        printf("Total of program and data memory: %lu bytes\n",
                       ulCodeByteCount + ulDataByteCount);
      }
    rc = XW_NOERROR;
    BeepOK();

#ifdef __DEBUG__
//  printf("program memory contents:\n");
//  for (i = 0; i < ulProgSize; i += 32) {
//    printf("%06X  ", i);
//    for (n=0; n<32; ++n)
//      printf("%02hX", (USHORT)ProgRegion[i+n]);
//    printf("\n");
//    }
#endif

    }
  else {
    if (!bQuiet)
      printf("File %s contains errors\n", szFileName);
    BeepError();
    }

  return  rc;                                           // result
  }


// ----------------------------------------------------------------
//
// Convert word to hex array (no trailing zero)
//
// Input  - unsigned short int (word)
//        - pointer to output buffer
//
// Output - 4 hex characters in destination buffer
//
// Returns pointer to the formatted buffer
//
// ----------------------------------------------------------------
static  char  *Word2Hex(USHORT usWord,
                        char  *pucHex) {

  if (pucHex == NULL)                                   // invalid pointer
    return NULL;
  Byte2Hex((char)((usWord / 256) & 0x00FF), pucHex);
  Byte2Hex((char)( usWord        & 0x00FF), pucHex + 2);
  return  pucHex;
  }


// ---------------------------------------
//
// Write Line with Extended Lineair Address info
//
// Input  - Extended Lineair Base address
//        - pointer to output file
//
// Output - line
//
// Returns - nothing
//
// ---------------------------------------
static  void  WriteHexExtLinAddrLine(ULONG  ulSegAddr,
                                     FILE   *pfOut) {

  char  ucCheckSum;

  HEXLINE HexLine = {  ':',
                      {'0','2'},                        // fixed length
                      {'0','0','0','0'},
                      {'0','4'},
                      {'0','0','0','0','0','0','\n','\0'} };

#ifdef __DEBUG__
  printf("Extended Lineair Segment Address: %06X\n", ulSegAddr);
#endif

  Word2Hex((USHORT)(ulSegAddr>>16), (char *)&HexLine.Data);   // 2 MSB's
  ucCheckSum = CheckSumHex((char *)&HexLine.Length, 12);
  Byte2Hex(ucCheckSum, (char *)&HexLine.Data[4]);
  fprintf(pfOut, (char *)(&HexLine));                   // write line
  return;                                               // done
  }


// ---------------------------------------------------
//
// WriteHexFile - create Hex file from current memory region contents
//
// Input   - filespec
//
// Output  - hex file
//
// Returns - zero returncode if everything OK
//         - otherwise errorcode and error message
//
// Remarks - target must be known
//           - automatically detected via programmer
//           - by commandline specification
//
// ---------------------------------------------------
extern  int    WriteHexFile(char  *szFileSpec) {

  ULONG   i;                                            // counter
  int     rc;
  FILE   *pfOutFile;                                    // pointer to hex file
  ULONG   ulFam;                                        // PIC family

  if (pTUsed == NULL) {                                 // target not detected
    if (pTSpec == NULL) {                               // not spec'd / auto
      rc = ProgrammerActivate();                        // init programmer
      if (rc != XW_NOERROR) {                           // init failed
        if (!bQuiet)
          printf("No programmer connected to %s, target must be specified\n",
                  szPortName);
        return rc;
        }
      IdentifyTarget();                                 // detect target
      if (pTUsed == NULL)                               // undetermined
        return XW_NOENT;                                // cannot proceed
      }
    else                                                // target specified
      pTUsed = pTSpec;                                  // accept it!
    }

  pfOutFile = fopen(szFileSpec, "w");
  if (pfOutFile == NULL) {
    if (!bQuiet)
      printf("Failed to open file '%s'\n", szFileSpec);
    BeepError();
    return XW_NOENT;
    }

  if (bVerbose  &&  !bQuiet)
    printf("Writing file '%s'\n", szFileSpec);

  ulFam = pTUsed->FamilyIndex;                          // actual family

  if (fRegions & REGION_PROG) {                         // prog region selected
    if (bVerbose  &&  !bQuiet)
      printf("Writing program region\n");
    for (i = 0; i < pTUsed->ProgSize; i += SEGMENTSIZE) {  // per segment
      bNewExtLinSegment = TRUE;                         // delayed new segment
      WriteHexSegment(ProgRegion + i,
                      pTUsed->ProgStart + i,
                      min(pTUsed->ProgSize - i, SEGMENTSIZE),
                      pTUsed->BlankMask,
                      pfOutFile);
      }
    }

  if (fRegions & REGION_DATA) {                         // data region selected
    if (bVerbose  &&  !bQuiet)
      printf("Writing data region\n");
    if (ulFam == CORE_16)                               // 18Fxxxx range
      bNewExtLinSegment = TRUE;                         // delayed new segment
    WriteHexSegment((ulFam == CORE_16) ? DataRegion : DataRegionMidrange,
                    pTUsed->DataStart,
                    pTUsed->DataSize * ((ulFam == CORE_16) ? 1 : 2),
                    (ulFam == CORE_16) ? 0xFFFF : 0x00FF,   // words/bytes
                    pfOutFile);
    }

  if (fRegions & REGION_ID) {                           // ID region selected
    if (bVerbose  &&  !bQuiet)
      printf("Writing ID region\n");
    if (ulFam == CORE_16)                               // 18Fxxxx range
      bNewExtLinSegment = TRUE;                         // delayed new segment
    WriteHexSegment((ulFam == CORE_16) ? IDRegion : IDRegionMidrange,
                    pTUsed->IDStart,
                    pTUsed->IDSize,
                    pTUsed->BlankMask,
                    pfOutFile);
    }

  if (fRegions & REGION_FUSES) {                       // Fuses Region selected
    if (bVerbose  &&  !bQuiet)
      printf("Writing fuses region\n");
//  ControlFuses();                                     // special treatment?
    if (ulFam == CORE_16)                               // 18Fxxxx range
      bNewExtLinSegment = TRUE;                         // delayed new segment
    WriteHexSegment((ulFam == CORE_16) ? FusesRegion : FusesRegionMidrange,
                    pTUsed->FusesStart,
                    pTUsed->FusesSize,
                    pTUsed->BlankMask,
                    pfOutFile);
    }

  fprintf(pfOutFile, ":00000001FF\n");                  // logical EOF
  fclose(pfOutFile);                                    // done

  if (!bQuiet)
    printf("Memory contents stored in hex file %s\n", szFileSpec);
  BeepOK();

  return XW_NOERROR;                                    // OK
  }


// ---------------------------------------
//
// Write Line with Hex Data
//
// Input    - pointer to start of memoryblock for this segment
//          - extended lineair segment address
//          - offset within segment
//          - maximum offset within segment
//          - bit mask for valid data
//          - output file pointer
//
// Output   - line
//
// Returns  - segment offset for next line
//
// Remarks: - assuming INTHX32 format
//          - lines are address aligned on 16 byte boundary
//          - large erased memory locations are not written
//
// ---------------------------------------
static  int    WriteHexLine(char   *pucSegment,
                            ULONG   ulSegmentAddress,
                            ULONG   ulSegmentOffset,
                            ULONG   ulMaxSegmentOffset,
                            USHORT  usBlankMask,
                            FILE   *pfOut) {

  ULONG  i;                                             // counter(s)
  ULONG  ulLocalOffset;                                 // moving offset
  USHORT usMemWord;                                     // data
  char   ucCheckSum;                                    // checksum value
  static HEXLINE HexLine = {  ':',
                             {'0','0'},
                             {'0','0','0','0'},
                             {'0','0'},
                             {'?','?','\n','\0'} };

  for (ulLocalOffset = ulSegmentOffset;                 // start data offset
           ulLocalOffset < ulMaxSegmentOffset;          // whole segment
               ulLocalOffset += 2) {                    // next 'word'
    usMemWord = BYTES2WORD(pucSegment + ulLocalOffset);
    if ((usMemWord & usBlankMask) != usBlankMask)       // not erased location
      break;
    }
  if (ulLocalOffset >= ulMaxSegmentOffset)              // out of bounds
    return ulMaxSegmentOffset;                          // all blank

//ulLocalOffset &= ~(2 * WORDSPERLINE - 1);             // align
//ulLocalOffset &= ~(WORDSPERLINE - 1);                 // removed 1.9.4

  if (pTUsed->FamilyIndex == CORE_16)                   // 18F family
    Word2Hex(ulLocalOffset, (char *)&HexLine.Offset);
  else                                                  // midrange PIC
    Word2Hex(ulSegmentAddress + ulLocalOffset, (char *)&HexLine.Offset);

#ifdef __DEBUG__
  printf("Hex line: LocalOffset %06X.", ulLocalOffset);
#endif

  for (i=0; i < WORDSPERLINE  &&
            (ulLocalOffset + (2 * i)) < ulMaxSegmentOffset; ++i) {
    Byte2Hex(pucSegment[ulLocalOffset + 2 * i],
                  (char *)&HexLine.Data + 4 * i);
    Byte2Hex(pucSegment[ulLocalOffset + 2 * i + 1],
                  (char *)&HexLine.Data + 4 * i + 2);
    }

  for (  ; i > 0; --i) {                                // backward scan
    usMemWord = BYTES2WORD(pucSegment + ulLocalOffset + 2 * (i-1));  // word
    if ((usMemWord & usBlankMask) != usBlankMask)       // not erased location
      break;                                            // end of block
    }

#ifdef __DEBUG__
  printf(".%06X\n", ulLocalOffset + 2 * i);    // written!
#endif

  if (i > 0) {                                          // at least 1 word
    if (bNewExtLinSegment) {                            // not yet written
      WriteHexExtLinAddrLine(ulSegmentAddress, pfOut);  // always
      bNewExtLinSegment = FALSE;                        // written
      }
    Byte2Hex((char)2*i, (char *)&HexLine.Length);      // length (bytes)
    ucCheckSum = CheckSumHex((char *)&HexLine.Length, 8 + 4 * i);
    Byte2Hex(ucCheckSum, (char *)&HexLine.Data[4*i]);
    HexLine.Data[4*i+2] = '\n';
    HexLine.Data[4*i+3] = '\0';
    fprintf(pfOut, (char *)(&HexLine));
    }

  return ulLocalOffset + (2 * WORDSPERLINE);            // actual progress
  }


// ---------------------------------------
//
// Write Segment of a Region
//
// Input    - pointer to start of memoryblock for this segment
//          - extended lineair segment address
//          - output file pointer
//
// Output   - nothing (calls writehexline)
//
// Returns  - nothing
//
// Remarks: - none
//
// ---------------------------------------
static  void  WriteHexSegment(char   *pucSegment,
                              ULONG   ulSegmentAddress,
                              ULONG   ulMaxSegmentOffset,
                              USHORT  usBlankMask,
                              FILE   *pfOutFile) {

  ULONG  i;                                             // counter(s)

#ifdef __DEBUG__
  printf("New segment: Address %06lX, Max Offset %06lX, mask %04hX\n",
                   ulSegmentAddress, ulMaxSegmentOffset, usBlankMask);
#endif

  for (i = 0; i < ulMaxSegmentOffset; ) {
    i = WriteHexLine(pucSegment,                        // ptr to mem data
                     ulSegmentAddress,                  // start addr segment
                     i,                                 // offset in segment
                     ulMaxSegmentOffset,                // upper bound
                     usBlankMask,
                     pfOutFile);
    }

  return;
  }
