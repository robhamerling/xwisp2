
// ---------------------------------------------------------------------
//
// xwisp2.h - Header file for Wisp648 support program
//
// Note: This header file contains some platform dependent code!
//       A separate header file is included to handle compiler dependencies.
//
// Author:  R. Hamerling
//
// Copyright (s) 2002..2023. R.Hamerling. All rights reserved.
//
// Released under the FREEBSD license (http://www.freebsd.org/copyright/freebsd-license.html)
//
// ---------------------------------------------------------------------

#ifndef __XWISP2_H__                            // prevent duplicate includes
#define __XWISP2_H__

#include "compiler.h"                           // name, version, etc.


// ---------------------------------------------------------------------
// some (OS-dependent!) code-generation parameters

#if defined(__OS2__) || defined(__LINUX__)
  #define __ANSIHELP__                          // W32 doesn't seem to support
                                                // ANSI screen controls
#endif


// ---------------------------------------------------------------------
// type definitions for some basic types

typedef unsigned short int   USHORT, *PUSHORT;  //  'duplicate' compiler
typedef   signed short int   SHORT,  *PSHORT;   //  warnings with xwisp2os.c
typedef unsigned long  int   ULONG,  *PULONG;   //  Just ignore!!
typedef   signed long  int   LONG,   *PLONG;    //


// ---------------------------------------------------------------------
// compiler includes
// (not all needed by all modules, but collected here for simplicity)

#if defined(__OS2__) || defined(__W32__)        // not for Linux
  #include <io.h>                               // gives conflicts
                                                // with Open Watcom C
                                                // ignore missing functions!
#endif

#include <errno.h>
#include <ctype.h>
#include <fcntl.h>
#include <share.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>


// ---------------------------------------------------------------------
// some platform dependent defines

#if defined(__OS2__)                                    // with OS/2
  typedef ULONG  BOOLEAN;
  typedef ULONG  HSERIALPORT;                           // handle for I/O
  #define INVALIDSERIALHANDLE  MINUS1
  #define ucDIRECTORYSEPARATOR   '\\'
  #define szDIRECTORYSEPARATOR   "\\"
  #define runtime_platform "eComStation (OS/2)"

#elif defined(__LINUX__)
  typedef ULONG  BOOLEAN;
  typedef int    HSERIALPORT;                           // handle for I/O
  #define INVALIDSERIALHANDLE  MINUS1
  #define ucDIRECTORYSEPARATOR   '/'
  #define szDIRECTORYSEPARATOR   "/"
  #define _searchenv(a,b,c) (*c=0)                      // nul: not used
  #if defined(__APPLE__)                                // MacOS
    #define runtime_platform "MacOS"
  #elif defined(__FREEBSD__)                            // FreeBSD
    #define runtime_platform "FreeBSD"
  #else                                                 // generic Linux
    #define runtime_platform "Linux"
  #endif

#elif defined(__W32__)                                  // W95 and up
  #include <windows.h>
  typedef HANDLE  HSERIALPORT;                          // handle for I/O
  #define INVALIDSERIALHANDLE  INVALID_HANDLE_VALUE
  #define ucDIRECTORYSEPARATOR   '\\'
  #define szDIRECTORYSEPARATOR   "\\"
  #define runtime_platform "Windows"

#else                                                   // any other
  #error Compile error: runtime platform unknown, or not supported!

#endif


// common defines

#ifndef FALSE
  #define FALSE  0
#endif
#ifndef TRUE
  #define TRUE   1
#endif

#define   MINUS1            0xFFFFFFFF                  // -1 ('unsigned')

#define   XW_NOERROR        0                           // error codes
#define   XW_NOMEM          8
#define   XW_NOTEXISTS      10
#define   XW_NOTREAD        20
#define   XW_IOERROR        21
#define   XW_NOENT          23
#define   XW_INVAL          36

// region flags for variable fRegions
#define   REGION_PROG      0x00000001           // )
#define   REGION_DATA      0x00000002           // ) Regions to be included
#define   REGION_ID        0x00000004           // ) in current command
#define   REGION_FUSES     0x00000008           // )
#define   REGION_ALL   (REGION_PROG | REGION_DATA | REGION_ID | REGION_FUSES)


// burstmode write flags
#define   BURSTWRITE_FDX   0x00000001           // 0=half, 1=full duplex
#define   BURSTWRITE_SUPP  0x00000002           // supp'd by Wisp628 firmware

// Presence of commandline argument
#define   CMD_REQ          TRUE                 // command parameter required
#define   CMD_OPT          FALSE                // command parameter optional


// macro for byte/word conversions
#define   BYTES2WORD(p)    ((USHORT)( (*(p)) + 256 * (*((p)+1)) ))
#define   WORD2BYTES(us,p) {*(p)     = (us) % 256; \
                            *((p)+1) = (us) / 256;}

#define   HEX2WORD(p,q)    { char x[2] = {0x00, 0x00}; \
                             Hex2Byte((char *)(p)+0, &x[0]); \
                             Hex2Byte((char *)(p)+2, &x[1]); \
                             *(USHORT *)q = (USHORT)x[0] * 256 + x[1]; \
                            }
#define   HEX2WORDINTEL(p,q) { char x[2] = {0x00, 0x00}; \
                               Hex2Byte((char *)(p)+2, &x[0]); \
                               Hex2Byte((char *)(p)+0, &x[1]); \
                               *(USHORT *)q = (USHORT)x[0] * 256 + x[1]; \
                              }

// Wisp648 state
enum _wisp_state_   {WISPSLEEP,
                     WISPATTENTION,
                     WISPACTIVE,
                     WISPPASSTHROUGH
                     };

// PIC state
enum _pic_state_    {WISP648_UNTESTED,
                     WISP648_TESTED,
                     WISP648_FAMILY,
                     WISP648_FAILED
                     };

// PIC family identifications (index in table)

enum _familyIndex {CORE_12, CORE_14, CORE_14H, CORE_16};

// PIC programming algorithms

enum  _algorithm {ALG_PIC16,        //  0   )
                  ALG_PIC16A,       //  1   )
                  ALG_PIC16B,       //  2   )
                  ALG_PIC18,        //  3   )
                  ALG_PIC16C,       //  4   ) value to pass to
                  ALG_PIC16D,       //  5   )
                  ALG_PIC16E,       //  6   )
                  ALG_PIC16F,       //  7   )
                  ALG_PIC12,        //  8   )
                  ALG_PIC16G,       //  9   )
                  ALG_PIC18A,       //  10  )
                  ALG_PIC16H,       //  11  )
                  ALG_PIC16I,       //  12  )
                  ALG_ZZZ
                  };


// PIC code/data memory protection
enum _protection_type     {PROT_HEX,
                           PROT_ALWAYS,
                           PROT_NEVER};

// common structures

typedef struct _hexLine {  char      Prefix;            // ':'
                           char      Length[2];         // # bytes
                           char      Offset[4];         // offset first data
                           char      Type[2];           // record type
                           char      Data[132];         // (var) data+XOR+NL
                           } HEXLINE, *PHEXLINE;

typedef struct _preserve { struct _preserve *next;      // chain pointer
                           USHORT    Address;           // address
                           USHORT    BitMask;           // bitmask
                           USHORT    Contents;          // original contents
                           } PRESERVE, *PPRESERVE;

typedef struct _targetInfo {struct _targetInfo *Next;   // ptr to next
                           char     *Name;              // PIC name
                           char     *Alias;             // short PIC name
                           char     *DataSheet;         // PIC datasheet
                           char     *PgmSpec;           // pgm specs
                           USHORT    DevID;             // PIC device ID
                           USHORT    RevMask;           // revision code mask
                           USHORT    FamilyIndex;       // PIC family index
                           USHORT    Core;              // address bits
                           USHORT    BlankMask;         // implemented bits
                           USHORT    Algorithm;         // programming method
                           ULONG     ProgStart;         // start prog mem
                           ULONG     ProgSize;          // program memory
                           ULONG     DataStart;         // start data mem
                           ULONG     DataSize;          // data memory
                           ULONG     IDStart;           // start IDmem
                           ULONG     IDSize;            // ID memory
                           ULONG     DevIDStart;        // start DevID
                           ULONG     DevIDSize;         // DevID mem
                           ULONG     FusesStart;        // start config mem
                           ULONG     FusesSize;         // fuses memory
                           char      FuseFixedZero[14]; // Fuses 0-bit mask
                           char      FuseFixedOne[14];  // Fuses 1-bit mask
                           USHORT    ProtectMask;       // code/data prot.
                           USHORT    WriteBurst;        // for progmem
                           USHORT    Status;            // tested=1
                           char      Delay;             // Tprog delay
                           BOOLEAN   VppFirst;          // Vpp before Vdd
                           PPRESERVE pPreserve;         // preserve chain
                           } TARGETINFO, *PTARGETINFO;


// externally accessible constants

extern  const char *szAuthorName;       // author of this program
extern  const char *szProgName;         // program name
extern  const char *szCopyRight;        // copyright notice
extern  const char *szLicense;          // License titile

extern  const char *szAlgorithmName[];  // programming algorithm name table
extern  const char *szPressEnter;       // global message
extern  const char *szTerminate;        // termination string

extern  const ULONG ulWbusBaudrateMinimum;  // minimum baudrate
extern  const ULONG ulWbusBaudrateMaximum;  // maximum baudrate
extern  const ULONG ulWbusBaudrateDefault;  // default baudrate

// externally accessible variables

extern  char  **szArgv;                 // commandline args
extern  char   *szEnv;                  // environment string

extern  char   *szPortName;             // device (serial port) name
extern  ULONG   ulPortIndex;            // serial port index (0 based number)
extern  char   *szWbusDevice;           // Wbus device name

extern  FILE   *pLogFile;               // log file pointer

                                        // largest memory buffer
                                        // determined from xwisp2.cfg
extern  ULONG   ulProgSize;             // maximum target prog memory
extern  ULONG   ulDataSize;             // maximum target data memory
extern  ULONG   ulIDSize;               // maximum target ID memory
extern  ULONG   ulFusesSize;            // maximum target fuses memory

extern  ULONG   ulCodeByteCount;        // code memory in hex file
extern  ULONG   ulDataByteCount;        // data memory in hex file

extern  ULONG   fBurstWrite;            // use burst writing to Wisp648
extern  ULONG   fProtection;            // protection flags
extern  ULONG   fRegions;               // PIC region indicators
extern  ULONG   ulWispLevel;            // Wisp6x8 firmware 100*Major+Minor
extern  ULONG   ulLBA;                  // lineair base address
extern  ULONG   ulLastAddress;          // Last address programmed
extern  ULONG   ulWbusBaudrateActual;   // actual Wisp648 line speed
extern  ULONG   ulWbusBaudrateUser;     // user specified value
extern  ULONG   ulWispState;            // actual state of Wisp device

extern  BOOLEAN bBeepEnable;            // audable signalling
extern  BOOLEAN bDTRstate;              // status of DTR signal
extern  BOOLEAN bLazyWrite;             // use lazy writing
extern  BOOLEAN bBurstReading;          // burst reading program memory
extern  BOOLEAN bRTSstate;              // status of RTS signal
extern  BOOLEAN bQuiet;                 // no console output if true
extern  BOOLEAN bUnattended;            // no user interaction
extern  BOOLEAN bVerbose;               // verbose log
extern  BOOLEAN bFullVerify;            // verify all of memory (region)

extern  USHORT  usFusesOverride;        // fuses value from commandline
extern  USHORT  usDelay;                // overriding 'TProg' delay

extern  char   *ProgRegion;             // pointer to program memory
extern  char   *DataRegion;             //    "       data   "
extern  char   *IDRegion;               //    "       ID     "
extern  char   *FusesRegion;            //    "       fuses  "
extern  char   *DataRegionMidrange;     // ) mapped and stored
extern  char   *IDRegionMidrange;       // ) in progmem
extern  char   *FusesRegionMidrange;    // ) for midrange

extern  PTARGETINFO  pTUsed;            // ptr to used PIC properties
extern  PTARGETINFO  pTSpec;            // ptr to user specified PIC props.


// prototypes of global common functions

// elementary Wbus functions (in xwisp2bus.c)
extern  int          WbusGo(void);
extern  int          WbusHello(void);
extern  int          WbusIncrement(void);
extern  int          WbusJump(ULONG);
extern  int          WbusPassThrough(USHORT);
extern  int          WbusProgram(USHORT);
extern  int          WbusRead(char *, ULONG, ULONG);
extern  char        *WbusType(void);
extern  int          WbusVersion(void);
extern  int          WbusWrite(char *, ULONG);

// PIC configuration functions (in xwisp2cfg.c)
extern  void         AdjustAlgorithm(ULONG);
extern  void         BulkErase(ULONG, char *, USHORT);
extern  int          ReadPICSpecs(char *);
extern  void         ReleasePICSpecs(void);
extern  PTARGETINFO  SearchPICAlgorithm(ULONG);
extern  PTARGETINFO  SearchPICDevID(USHORT, USHORT);
extern  PTARGETINFO  SearchPICFamily(USHORT);
extern  PTARGETINFO  SearchPICLast(PULONG);
extern  PTARGETINFO  SearchPICName(char *);
extern  PTARGETINFO  SearchPICNumber(ULONG);
extern  char        *ToUpperCase(char *);

// Commandline parameter handling functions (xwisp2cmd.c)
extern  int          CommandHandling(int, char **);
extern  void         EnvironmentHandling(void);

// Elementary communications functions (in xwisp2com.c)
extern  int          ProgrammerActivate(void);
extern  void         ProgrammerDeactivate(void);
extern  int          SendByte(char);
extern  int          SendReceive(char *, char *);
extern  int          SendReceiveSlow(char *, char *);
extern  int          SetModemOutputStatus(BOOLEAN, BOOLEAN);
extern  int          SpeedIndex(ULONG);

// Hex file manipulation functions (in xwisp2hex.c)
extern  int          DumpImage(void);
extern  int          ReadHexFile(char *);
extern  int          WriteHexFile(char *);

// Miscellaneous functions (in xwisp2mis.c)
// (some user API functions defined in xwisp2.h)
extern  void         Abort(int);
extern  void         BeepOK(void);
extern  void         BeepError(void);
extern  int          Help(void);
extern  int          Hex2Byte(char *, char*);
extern  void         LogMsg(char *);
extern  int          LogStart(char *);
extern  void         LogStop(void);
extern  void         MessageWithReply(char *);
extern  int          ReportResult(const char *, int, ULONG);
extern  char        *TimeString(void);

// Operating System specific functions (in xwisp2os.c)
extern  int          OS_Beep(ULONG, ULONG);
extern  int          OS_FileSearch(char *, char *, char *);
extern  int          OS_SerialBreak(HSERIALPORT, ULONG);
extern  void         OS_SerialClose(HSERIALPORT);
extern  int          OS_SerialFlush(HSERIALPORT);
extern  int          OS_SerialGetBaudrate(HSERIALPORT, PULONG,
                                          PULONG, PULONG);
extern  HSERIALPORT  OS_SerialOpen(char *);
extern  int          OS_SerialRead(HSERIALPORT, char *, int);
extern  int          OS_SerialSetBaudrate(HSERIALPORT, ULONG);
extern  int          OS_SerialSetMode(HSERIALPORT);
extern  int          OS_SerialSetModemOutputStatus(HSERIALPORT,
                                                   BOOLEAN, BOOLEAN);
extern  int          OS_SerialSetReadTimeout(HSERIALPORT, ULONG);
extern  int          OS_SerialWrite(HSERIALPORT, char *, int);
extern  int          OS_Sleep(ULONG);
extern  ULONG        OS_Time(void);

// Target handling functions (in xwisp2tgt.c)
extern  void         ControlFuses(void);
extern  int          EraseTarget(void);
extern  void         IdentifyTarget(void);
extern  int          ReadTarget(void);
extern  int          RunTarget(void);
extern  int          VerifyTarget(void);
extern  int          WriteTarget(void);
extern  int          WriteVerifyTarget(void);


#endif  /* __XWISP2_H__ */
